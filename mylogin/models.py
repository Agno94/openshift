from django.db import models
from django.contrib.auth.models import User

from django.utils import timezone

from random import randint

class UserVerificationKey(models.Model):
	"""
	Stores a random generated key that is to be sended to the user's email.
	The key is used to verify the user account.
	 * user -> foreign key to default django User
	 * key -> the string with the key
	 * expire -> the time until the key is valid
	 * ip -> the IP used for the signing up request
	"""
	EXPIRE_TIMEDELTA = timezone.timedelta(days=1)
	def __str__(self):
		return "User Key for %s : %s"%(self.user.__repr__(), self.key)
	def __init__(self, *args, **kwargs) :
		if not ("key" in kwargs):
			kwargs["key"] = "%.32x"%randint(1,2**128-1)
		if not ("expire" in kwargs):
			kwargs["expire"] = timezone.now()+UserVerificationKey.EXPIRE_TIMEDELTA
		super(UserVerificationKey, self).__init__(*args, **kwargs)
	user = models.OneToOneField(User, on_delete=models.CASCADE)
	key = models.CharField("Key",max_length=32)
	expire = models.DateTimeField("Expire Time")
	ip = models.GenericIPAddressField("Ip adress")
