# -*- encoding: utf8 -*-
from django.shortcuts import render, get_object_or_404
from django.http import HttpResponse, HttpResponseRedirect, Http404
from django.urls import reverse
from django.utils.http import urlencode

from django.contrib import messages
from django.contrib.auth import login as django_login, logout as django_logout
from django.contrib.auth.models import User

from django.contrib.auth.forms import *
#from django import forms

from django.utils import timezone

from .models import UserVerificationKey

from django.core.mail import EmailMessage, send_mail

MAX_UNCHECKED_USER_X_IP = 5
MIN_PASSWORD_LENGTH = 4
CLEAN_OLD_KEYS = False

ALLOW_WITHOUT_EMAIL = False
EMAIL_SENDER_ADDRESS = "lupusrealtime@gmx.com"
EMAIL_PREFIX = "[LRT] "

class MyUserCreationForm(UserCreationForm):
	class Meta(UserCreationForm.Meta):
		fields = UserCreationForm.Meta.fields + ("first_name","last_name", "email")
	def clean(self):
		cleaned_data = super(MyUserCreationForm, self).clean()
		first_name = cleaned_data.get("first_name")
		last_name = cleaned_data.get("last_name")
		if (not first_name) and (not last_name):
			raise forms.ValidationError("E' riesco almeno un campo fra nome e cognome")
		password = cleaned_data.get("password1")
		if (password) :
			if (password.isalnum() or password.isalpha() or password.isdigit()) :
				del cleaned_data["password1"]
				raise forms.ValidationError("Scegli un password con lettere, numeri e caratteri speciali")
			if (len(password) < MIN_PASSWORD_LENGTH) :
				del cleaned_data["password1"]
				raise forms.ValidationError("Scegli un password di almeno %(min_length)d caratteri", params={"min_length":MIN_PASSWORD_LENGTH})
		email = cleaned_data.get("email")
		if (not email):
			raise forms.ValidationError("E' richiesto un indirizzo email valido")
		else :
			if User.objects.filter(email=email).exists() :
				raise forms.ValidationError("Indirizzo email non valido")
		return cleaned_data

PASSWORD_RESET_EMAIL_FORMAT = """
<h3>Richiesta reset password per <b>%(user)s</b></h3>
<p>Questa email &egrave; stata fornita per richiedere il reset della password del prorpio account sul sito <i>%(site_name)s</i>.</p>
<p>Apri il link seguente per scegliere una nuova password: <br>
<a href="%(url)s"> [<small> %(url)s </small>] </a></p><br>
<p>Se non sei tu ad aver richiesto la reimpostazione della password cancella pure questa email.</p><br>
<p><small>Email generata automaticamente da Django. Non rispondere.</small></p>
"""

class MyPasswordResetForm(PasswordResetForm):
	def send_mail(self, subject_template_name, email_template_name, context, from_email, to_email, html_email_template_name=None):
		"""
		Invia la mai per il reset della password
		"""
		subject = EMAIL_PREFIX + "Richiesta reset password"
		url = "https://" + context["domain"] + reverse("login:password_reset_confirm", kwargs={"uidb64": context["uid"], "token": context["token"]})
		context["url"] = url
		content = PASSWORD_RESET_EMAIL_FORMAT%context
		msg = EmailMessage( subject, content, EMAIL_SENDER_ADDRESS, to=[to_email], bcc=[EMAIL_SENDER_ADDRESS])
		msg.content_subtype = "html"
		msg.send()

def index(request):
	return render(request,"mylogin/index.html",{})

VERIFICATION_EMAIL_FORMAT = """
<h3>Verifica dell'account <b>%(username)s</b></h3>
<p>Questa email &egrave; stata fornita per registrare un account sul sito <i>%(host)s</i>.</p>
<p>Il codice di verifica dell'account e' <b>%(key)s</b><br>Apri il link seguente per attivare il tuo utente automaticamente : <br>
<a href="%(url)s?key=%(key)s"> [<small> %(url)s?key=%(key)s </small>] </a></p><br>
<p>Se non sei tu ad aver richiesto la registrazione cancella pure questa email. Se lo desideri puoi eliminare l'account creato aprendo questo link : <br>
<a href="%(url)s?delete=%(key)s"> [<small> %(url)s?delete=%(key)s </small>] </a></p><br>
<p><small>Email generata automaticamente da Django. Non rispondere.</small></p>
"""

def signup(request):
	if request.user.is_active :
		return HttpResponseRedirect(reverse("login:index"))
	if request.user.is_authenticated() :
		return HttpResponseRedirect(reverse("login:signup_check"))
	if request.method == "GET" :
		return render(request,"mylogin/signup_form.html",{"reg_form":MyUserCreationForm()})
	# Inizializzo il Form e controlla i dati
	form = MyUserCreationForm(request.POST)
	if not form.is_valid() :
		return render(request,"mylogin/signup_form.html",{"reg_form":form})
	# Se l'IP ha gia' troppi utenti non verificati esco
	ip = request.META["REMOTE_ADDR"]
	unchecked_x_ip = UserVerificationKey.objects.filter(expire__gt=timezone.now(), ip = ip).count()
	if (unchecked_x_ip >= MAX_UNCHECKED_USER_X_IP):
		return HttpResponseRedirect(reverse("login:signup_check"))
	# Ottengo il modello dell'utente, lo disattivo, genero la chiave e salvo
	u = form.save(commit = False)
	u.is_active = False
	u.save()
	ukey = UserVerificationKey(user=u, ip=ip)
	ukey.save()
	# Invio la mail
	url = "https://" + request.META["HTTP_HOST"] + reverse("login:signup_check")
	address = u.email
	content = VERIFICATION_EMAIL_FORMAT%{"username":u.username, "key":ukey.key, "url":url, "host":request.META["HTTP_HOST"]}
	msg = EmailMessage( EMAIL_PREFIX + "Chiave verifica account", content, EMAIL_SENDER_ADDRESS, to=[address], bcc=[EMAIL_SENDER_ADDRESS])
	msg.content_subtype = "html"
	msg.send()
	if (ALLOW_WITHOUT_EMAIL):
		request.session["show_registration_key"]=ukey.key
	return HttpResponseRedirect(reverse("login:signup_check"))

def signup_check(request):
	if request.user.is_active :
		return HttpResponseRedirect(reverse("login:index"))
	check_key = request.GET.get("key","")
	to_delete = (not check_key)
	if to_delete :
		check_key = request.GET.get("delete","")
		to_delete = True
	context={"check_key":check_key, "to_delete":to_delete}
	if not check_key :
		if (ALLOW_WITHOUT_EMAIL):
			context["show_check_key"] = request.session.get("show_registration_key","")
		context["host"] = "http://" + request.META["HTTP_HOST"]
	else :
		try:
			ukey = UserVerificationKey.objects.select_related().get(expire__gt=timezone.now(), key=check_key)
		except (UserVerificationKey.DoesNotExist):
			context["is_key_valid"] = False
			context["error"] = "Codice chiave errata o scaduta"
		except (UserVerificationKey.MultipleObjectsReturned):
			context["is_key_valid"] = False
			context["error"] = "Codice chiave non univoca"
		else :
			context["is_key_valid"] = True
			user = ukey.user
			context["username"] = user.username
			if to_delete :
				user.delete()
			else :
				user.is_active=True
				user.save()
			ukey.delete()#Forse con OneToOneField potrebbe non essere necessario ?
	return render(request,"mylogin/signup_check.html",context)

def login_form(request):
	next_url = request.GET.get("next","")
	if next_url :
		request.session["next"] = next_url
	if request.user.is_authenticated() :
		return HttpResponseRedirect(next_url if next_url else reverse("login:index"))
	context = {}
	if ("error" in request.GET) :
		context["errors"] = request.session.pop("login_form_errors",{})
	return render(request,"mylogin/login_form.html", context)

def login(request):
	next_url=request.GET.get("next", request.session.pop("next", None))
	if request.user.is_authenticated() :
		return HttpResponseRedirect(next_url if next_url else reverse("login:index"))
	request.session["next"] = next_url
	request.session.pop("login_form_errors","")
	if not request.method == "POST" :
		return HttpResponseRedirect(reverse("login:dologin"))
	form = AuthenticationForm(request, request.POST)
	if not form.is_valid() :
		request.session["login_form_errors"] = {"non_field":form.non_field_errors(),"fields":[(x.name,x.errors) for x in form]}
		return HttpResponseRedirect(reverse("login:dologin")+"?error")
	u = form.get_user()
	messages.success(request, "Login affenuto con successo")
	u.backend = 'django.contrib.auth.backends.ModelBackend'
	django_login(request, u)
	if not u.is_active :
		HttpResponseRedirect(reverse("login:signup_check"))
	return HttpResponseRedirect(next_url if next_url else reverse("login:index"))

def logout(request):
	django_logout(request)
	return HttpResponseRedirect(reverse("login:index"))
