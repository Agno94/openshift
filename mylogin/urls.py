from django.conf.urls import url
from django.contrib.auth import views as auth_views

from mylogin import views

app_name = "login"

urlpatterns = [
	url(r"^$", views.index, name ="index"),
	url(r"^login/$", views.login, name ="login"),
	url(r"^logout/$", views.logout, name ="logout"),
	url(r"^dologin/$", views.login_form, name ="dologin"),
	url(r"^signup/$", views.signup, name ="signup"),
	url(r"^check/$", views.signup_check, name ="signup_check"),
	url(r"^password/reset$", auth_views.password_reset, {"template_name": "mylogin/password_reset.html", "password_reset_form": views.MyPasswordResetForm, "post_reset_redirect": "login:password_reset_done"}, name="password_reset"),
	url(r"^password/reset/sent$", auth_views.password_reset_done, {"template_name": "mylogin/password_reset_done.html"}, name="password_reset_done"),
	url(r"^password/reset/confirm/uid=(?P<uidb64>[0-9A-Za-z_\-]+)&token=(?P<token>[0-9A-Za-z]{1,13}-[0-9A-Za-z]{1,20})$", auth_views.password_reset_confirm, {"template_name": "mylogin/password_reset_confirm.html", "post_reset_redirect": "login:password_reset_complete"}, name="password_reset_confirm"),
	url(r"^password/reset/done$", auth_views.password_reset_complete, {"template_name": "mylogin/password_reset_complete.html"}, name="password_reset_complete"),
	url(r"^password/change$", auth_views.password_change, {"template_name": "mylogin/password_change.html", "post_change_redirect": "login:password_change_done"}, name="password_change"),
	url(r"password/change/done$", auth_views.password_change_done, {"template_name": "mylogin/password_change_done.html"}, name="password_change_done"),
]
