from setuptools import setup

setup(name='LupusRT',
      version='1.0',
      description='OpenShift App',
      author='AA & DT',
      author_email='example@example.com',
      url='http://www.python.org/sigs/distutils-sig/',
      install_requires=['Django>=1.11','psycopg2>=2.7'],
     )
