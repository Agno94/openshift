function loadScript(url, callback) 
{
	var script = document.createElement("script")
	script.type = "text/javascript";
	if (script.readyState) { 
		//IE
		script.onreadystatechange = function () 
		{
			if (script.readyState == "loaded" || script.readyState == "complete") 
			{
				script.onreadystatechange = null;
				callback();
			}
		};
	} else { 
		//Others
		script.onload = function () 
		{
			callback();
		};
	}
	script.src = url;
	document.getElementsByTagName("head")[0].appendChild(script);
}

function loadCSS(url) 
{
	var link = document.createElement("link")
	link.type = "text/css";
	link.rel = "styleSheet"
	link.href = url;
	document.getElementsByTagName("head")[0].appendChild(link);
}