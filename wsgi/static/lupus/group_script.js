function lpc_new_msg(val, after){ //Funzione che crea un nuovo messaggio e lo aggiunge dove deve andare (in cima o in fondo)
	var s = '<div class="lpc-msg" id="msg'+val.id+'"';
	var date = new Date(val.time);
	s+='><div>'+val.utente+'<br>'+date.toLocaleTimeString();
	s+='</div><div class="lpc-msgtxt">'+val.text+'</div></div>';
	if (after) {
		$(".lpc-messages").children().first().next().after(s);
	} else {
		$(".lpc-messages").append(s);
	}
}

function lpc_new_date(date, after) {
	var s = '<div class="lpc-msg" ';
	s+='><div>'+date.getDate() + " " + MESI[date.getMonth()] + " " + date.getFullYear();
	s+='</div></div>';
	if (after) {
		$(".lpc-messages").children().first().after(s);
	} else {
	$(".lpc-messages").append(s);
	}
}

function lpc_get_msg(first, last) {
	str=url_getmsg_standard.replace("0to",first.toString()+"to").replace("to0","to"+last.toString());
	$.get(str, function(data, textStatus, jqXHR){
	if (data.length > 0){
		var obj = $(".lpc-messages")[0];
		var atBottom = $(obj).scrollTop() + $(obj).innerHeight() >= obj.scrollHeight;
		var after;
		var m = data[0];
		if (first_id < m.id) {
			after = false;
			if (!last_id && !first_id){ //Se entrambi a zero allora è la prima chiamata, quindi li devo modificare entrambi
				last_id=data[data.length-1].id;
				var date = new Date(data[data.length-1].time);
				last_date = date.toLocaleDateString();
			}
			first_id=m.id;
			data = data.reverse(); //Reverse dell'array im modo da inserire dal messaggio meno recente
		} else if (last_id > m.id) {
			after = true;
			last_id=data[data.length-1].id;
		}
		$.each(data, function(index, m){
			var date = new Date(m.time);
			if (!$("#msg"+m.id).length){
				if (!(after) && first_date != date.toLocaleDateString()){
					lpc_new_date(date, after);
					first_date = date.toLocaleDateString();
				}
				else if (after && last_date != date.toLocaleDateString()){
					lpc_new_date(date, after);
					last_date = date.toLocaleDateString();
				}
					lpc_new_msg(m, after);
			}
			});
		if (atBottom){ //Se era già in fondo
			$(obj).stop().animate({
				scrollTop: obj.scrollHeight
			}, 800);
		}
		}
		if ((last || (!(first) && !(last))) && data.length < msg_per_req_context) { //Se chiedo i messaggi vecchi (last) o è la prima chiamata (0, 0)
			$("#lpc-load").remove();
		}
	}); 
}

function lpc_send_msg() {
	valore = $(".lpc-newmsg").val();
	$(".lpc-newmsg").val("");
	if (valore=="") { return; }
	$.post(url_postmsg,
		{msg:valore, gruppo:gruppo_id_context, csrfmiddlewaretoken:csrf_token_context},
		function(data,status){});
	clearInterval(timer);
	setTimeout(function() {lpc_get_msg(first_id, 0);}, 200);
	timer = startTimer();
}

function startTimer() {
	return setInterval(function() {lpc_get_msg(first_id, 0);}, req_interval_context);
}
 
function lpc_characters_count() {
	var count = $(this).val().length;
	$("#lpc-len").html(count);
	if (count > 190) {
		$("#lpc-car").css("color", "#a11");
		$("#lpc-send").prop("disabled", true);
	} else {
		$("#lpc-car").css("color", "");
		$("#lpc-send").prop("disabled", false);
	}
}

//var agent_btn_id = "#power_agent_";
//var target_btn_id = "#target_agent_";
//var selectform = document.power_action_form;

function selectform_agent_click(uID) {
	agente = parseInt(selectform.agente.value);
	bersaglio = parseInt(selectform.bersaglio.value);
	if (agente==uID) {
		selectform.agente.value = 0;
	} else {
		if (agente) {
			$(agent_btn_id+agente).toggleClass("btn-chosen");
			$(target_btn_id+agente).toggleClass("btn-disabled");
		}
		if (uID == bersaglio) {
			$(agent_btn_id+bersaglio).toggleClass("btn-chosen");			
			selectform.bersaglio.value = 0;
		}
		selectform.agente.value = uID;
	}
	$(agent_btn_id+uID).toggleClass("btn-chosen");
	$(target_btn_id+uID).toggleClass("btn-disabled");
}

function selectform_target_click(uID) {
	agente = parseInt(selectform.agente.value);
	bersaglio = parseInt(selectform.bersaglio.value);
	if (uID == bersaglio) {
		selectform.bersaglio.value = 0;
	} else {
		if (uID == agente) {
			alert("Non puoi scegliere questo utente");
			return;
		}
		if (bersaglio) {
			$(target_btn_id+bersaglio).toggleClass("btn-chosen");
		}
		selectform.bersaglio.value = uID;
	}
	$(target_btn_id+uID).toggleClass("btn-chosen");
}
