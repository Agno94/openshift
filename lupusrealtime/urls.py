from django.conf.urls import include, url

from django.contrib import admin
admin.autodiscover()

urlpatterns = [
    # Examples:
    # url(r'^$', 'lupusrealtime.views.home', name='home'),
    # url(r'^blog/', include('blog.urls')),
    url(r'^', include('lupus.urls', namespace="lupus")),
    url(r'^auth/', include("mylogin.urls", namespace="login")),
    url(r'^admin/', include(admin.site.urls)),
    url(r'^admin/doc/', include('django.contrib.admindocs.urls')),
]
