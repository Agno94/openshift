# -*- encoding: utf8 -*-

#from django.conf import settings
#from lupus.setting import EVENTI_FLAG, EVENTI
from lupus.models import Eventi, Utente, Nota, Votazione

from random import randint, choice

class EventJournalObject(object):
	def __init__(self, titolo, date, nice=99, imm="", text="", template="", context=dict()):
		self.titolo = titolo
		self.imm = "lupus-images/events-images/"+imm if imm else ""
		self.date = date
		self.text = text
		self.template = "lupus/events/%s"%template if template else ""
		self.context = context
		self.nice = nice
	def __le__(self, y):
		return (self.date < y.date) or (self.date == y.date and self.nice >= y.nice)
	def __lt__(self, y):
		return (self.date < y.date) or (self.date == y.date and self.nice > y.nice)

class EventParser(object):
	flags = []
	def __init__(self, compatto=False) :
		self.results = []
		self.events = []
		self.compatto = compatto
	def __str__(self):
		return "{%s}"%(str(self.flags)[1:-1]);
	def __repr__(self):
		return "<Event Parser : %s >"%(self.__str__())
	def __call__(self):
		self.parser()
		return self.results
	def parser(self):
		return
	def append(self, event) :
		return self.events.append(event)

class EventParserDefaultMulti(EventParser):
	scene = [{"imm":"", "testo":""}]
	scena_compatto = {"imm":"", "testo":""}
	titolo = ""
	nice = 99
	def parser(self):
		for evento in self.events :
			if self.compatto:
				scena = self.scena_compatto
			else:
				if evento.scena and (evento.scena <= len(self.scene)):
					scena = self.scene[evento.scena-1]
				else :
					scena = choice(self.scene)
			testo = scena["testo"]%(evento.utente.nome)
			titolo = scena.get("titolo",self.titolo)
			self.results.append(EventJournalObject(titolo, evento.date, nice=self.nice, imm=scena["imm"], text=testo))
		return


class EventParserMorte(EventParserDefaultMulti):
	flags = [1]
	scene = [
		{"imm":"immagine","testo":"""
<p>Diamo la triste notizia che nella mattinata di stamane il lattaio ha trovato il corpo esanime
 del nostro concittadino <span style="color:#f44; font-weight:600;">%s</span> accasciato sul pianerottolo della propria abitazione.<br>
 Il corpo, contuso e dilaniato, suggerisce una morte violenta.</p>
		"""},
		{"imm":"immagine","testo":"""
<p>Sulla piazza della chiesa, ai piedi del campanile &egrave; stato trovato all'alba
 il corpo insanguinato di <span style="color:#f44; font-weight:600;">%s</span><br>
 Le cause della morte non sono chiare ma si sospetta un'aggressione.<br>
 Il villaggio si raccoglie nel lutto per un altro condittadino che trapassa.</p>
		"""},
		{"imm":"immagine","testo":"""
<p>Rimane ben poco delle membra di <span style="color:#f44; font-weight:600;">%s</span>.<br>
Le sue viscere, sparse per il chiostro del museo, sono state ritrovate in mattinata dal custode. Lo scheletro &egrave; stato rinvenuto
 scuoiato sotto i portici del vicolo adiacente. <br>
Il reparto investigativo dichiara di non avere la bench&eacute; minima idea circa quale
 forza umana o paranormale possa aver ridotto in siffatto stato il cadavere del nostro concittadino.
		"""},
	]
	scena_compatto = {"imm":"","testo":"%s &egrave; morto"}
	titolo = "Morte violenta"

class EventParserSuicidioNotte(EventParserMorte):
	flags = [3]
	scena_compatto = {"imm":"","testo":"%s si &egrave; suicidato"}
	pass

class EventParserSuicidioGiorno(EventParserDefaultMulti):
	flags = [5]
	scene = [
		{"imm":"immagine","testo":"""
<p>Il villaggio &egrave; sconvolto da quanto accaduto questo pomeriggio. Il nostro concittadino <span style="color:#f44; font-weight:600;">%s</span>
 si &egrave; lanciato dalla cima della Torre cittadina, rimanendo ucciso nell'impatto con il suolo. Nulla hanno potuto fare i soccorsi.<br>
 I testimoni della scena hanno affermato che &egrave; stato un atto libero, quindi un suicidio. Non sono chiare le cause.<br>
		"""},
	]
	scena_compatto = {"imm":"","testo":"%s si &egrave; suicidato"}
	nice=1
	titolo = "Suicidio"

class EventParserSam(EventParser):
	flags = [4,14]
	testo = """
<p><i>Finalmente %(sam)s  si &egrave; rivelato come Sam lanciando la sua maledizione su quell'antipatico di %(altro)s</i><br>
 Giusto qualche ora fa il nostro concittadino <b>%(sam)s</b>, uscito di casa, si dirige verso via degli Indesiderati dove si trova
 la casa di <b>%(altro)s</b> e, raggiuntala, suona il campanello. Appena l'inquilino esce il nostro eroe gli punta addosso il proprio indice
 e, con un ghigno, afferma <b>&laquo;Muori, Stronzo !!&raquo;</b>.<br>
Il seccatore fugge spaventato, ma viene presto colto, forse per lo spavento, da un infarto. <br>Nessuno ha creduto di dover imputare al Sam
 la morte del nostro concittadino, di cui, peraltro, non sentiremo molto la mancanza.</p>
	"""
	testo_compatto = "%(sam)s si &egrave; rivelato come Sam e ha causato la morte di %(altro)s"
	def parser(self):
		sam_index = 0 if (self.events[0].flags == 14) else 1
		sam = self.events[sam_index].utente.nome
		altro = self.events[1-sam_index].utente.nome
		if self.compatto:
			testo = self.testo_compatto%{"sam":sam, "altro":altro}
		else:
			testo = self.testo%{"sam":sam, "altro":altro}
		self.results = [EventJournalObject("Il Sam si è rivelato", self.events[0].date, nice=10, imm="immaginesam.png", text=testo)]
		return

class EventParserLinciaggio(EventParserDefaultMulti):
	flags = [2]
	nice = 0
	scene = [
		{"titolo":"Al rogo !!","imm":"immagine","testo":"""
<p>Mossa dalla rabbia furiosa per quello che sta avvenendo nel villaggio, dalla sete di vendetta e dal desiderio di sicurezza
 l'assemblea cittadina si &egrave; oggi scagliata contro <span style="color:#fd6; font-weight:600;">%s</span>.<br>
 Dopo averlo rincorso e catturato il popolo ha stabilito che venisse bruciato sul rogo.<br>
 Il rosso del fuoco che consumava il suo corpo ha colorato un flebile tramonto.</p>
		"""},
		{"titolo":"Appeso alla forca !!","imm":"immagine","testo":"""
<p>L'assemblea del popolo, per risolvere il problema che vive il villaggio,
 ha stabilito oggi la condanna a morte per <span style="color:#fd6; font-weight:600;">%s</span>.<br>
 Il concittadino &egrave; stato portato alla forca ed impiccato nel tardo pomeriggio</p>
		"""},
		{"titolo":"Traditore decapitato !! Sfregiato palazzo rinascimentale","imm":"immagine","testo":"""
<p>Suonano le Alte Trombe per commemorare il decesso di <span style="color:#fd6; font-weight:600;">%s</span>.<br>
Un portentoso getto di sangue si &egrave; riversato sulla maestosa facciata di Palazzo Contarini appena ristrutturata,
 dopo che il traditore &egrave; stato giustiziato dal nostro Boia alla presenza della Nobile Magnolia. Nessuno si sarebbe
 mai aspettato che un fluido come il sangue subisse una tale deviazione dalla legge di Bernoulli.<br>
La compagnia delle Belle Arti ha gi&aacute; un progetto per rimediare all'accaduto.</p>
		"""},
		{"titolo":"Un papiro di Sangue lungo il Piovego","imm":"immagine","testo":"""
Una solenne celebrazione si &egrave; tenuta lungo le rive del Canal Piovego.<br>
L'impiccagione di <span style="color:#fd6; font-weight:600;">%s</span> &egrave; stata realizzata sui robusti rami
 del pi&uacute; alto platano dell'argine sinistro. Un coro di voci colme di furore si &egrave; levato orgoglioso alla lettura
 del manoscritto che trasmette il disprezzo per colui che ha appena dato l'ultimo esame della propria vita: La Morte.<br>
Un rigolo color carminio dipinge le rigogliose acque verde oliva-andata-a-male.
 Ad assistere allo spettacolo un sostanzioso gruppo di pantegane a dir poco infastidite per la
 macabra esecuzione. Speriamo che la morte del reietto serva a liberarci finalmente da questo flagello indomabile.
		"""},
	]
	scena_compatto = {"imm":"","testo":"%s &egrave; stato ucciso"}

class EventParserElezione(EventParser):
	flags = [11]
	testo = """
<p>Siamo lieti di annunciare che &egrave; stato eletto un nuovo sindaco.<br>
 Il fortunato &egrave; <b>%(nome)s</b> che ha vinto le elezioni tenutesi oggi con la bellezza di %(voti)d voti<br>
 Mentre le malelingue segnalano brogli, noi facciamo i nostri auguri di buon lavoro al neo-eletto</p>
		"""
	testo_compatto = "%(nome)s &egrave; stato eletto sindaco con %(voti)d voti"
	def parser(self):
		for evento in self.events :
			try :
				voti = Votazione.objects.get(utente__id=evento.utente.id, flags=2, date=evento.date)
			except (Votazione.DoesNotExist) :
				print("ERRROR Sindaco eletto senza votazione ?!?!?!?!?!")
				# Istruzioni per debug
				voti = Votazione(count=0)
			if self.compatto:
				testo = self.testo_compatto%{"nome":evento.utente.nome, "voti":voti.count}
			else:
				testo = self.testo%{"nome":evento.utente.nome, "voti":voti.count}
			self.results.append(EventJournalObject("Eletto il sindaco", evento.date, nice=-1, imm="immaginesindaco.png", text=testo))
		return

class EventParserPaparazzi(EventParser):
	flags = [21]
	lista_template = ["PAP1.html"]
	testo_compatto = "Il paparazzi ha spiato %(spiato)s"
	testo_uscito = "&egrave; uscito di casa ed "
	def parser(self):
		paparazzati = set()
		for evento in self.events :
			if not self.compatto:
				if evento.scena and (evento.scena <= len(self.lista_template)):
					tmpl = self.lista_template[evento.scena-1]
				else :
					tmpl = choice(self.lista_template)
			movimenti = Nota.objects.select_related("utente2").filter(utente1_id=evento.utente.id, time=evento.date, flags="EPAP") #opt=evento.id,
			if not self.compatto:
				context = {"spiato":evento.utente}
				context["entrati"] = [mov.utente2.nome for mov in movimenti]
				context["uscito"] = Nota.objects.filter(utente1_id=evento.utente.id, time=evento.date, flags="UPAP").exists()#.exclude(opt=0).exists()
				self.results.append(EventJournalObject("Indagine a domicilio", evento.date, nice=50, imm="immaginesam.png", template=tmpl, context=context))
			else:
				context = {"spiato":evento.utente.nome}
				testo = self.testo_compatto%context
				self.results.append(EventJournalObject("Indagine a domicilio", evento.date, nice=50, imm="immaginesam.png", text=testo))
		return

class EventParserResurezzione(EventParserDefaultMulti):
	nice = 45
	flags = [31]
	scene = [
		{"imm":"immagine","testo":"""
<p>Un fatto incredibile &egrave; avvenuto stamani. Cosa considerata impossibile: sembra che
 <span style="color:#4f4; font-weight:600;">%s</span> sia risorto. <br>Verso mezzogiorno si sarebbero dovuti tenere i funerali del nostro concittadino.
 Gli ufficiali preposti si sono quindi recati in mattinata a controllarne la salma, ma si sono allarmati non trovandola.<br>
 In un un primo momento si era pensato ad un furto legato ad un qualche traffico di organi, o ad una connessione con le attivit&agrave; dei mannari, tuttavia
 compaesani sentendo movimento nella casa del supposto morto si sono raccomandati di controllare, hanno cos&igrave; trovato il nostro concittadino vivo e vegeto
 intento a prepararsi la colazione, un po' malconcio ma convito di aver fatto una lunga dormita.
 </p>
		"""},
	]
	scena_compatto = {"imm":"","testo":"%s &egrave; risorto"}
	titolo = "E' risolto!"

class EventParserLinciaggioFallito(EventParserDefaultMulti):
	nice = -1
	flags = [32]
	scene = [
		{"imm":"immagine","testo":"""
		"""},
	]
	scena_compatto = {"imm":"","testo":"%s non &egrave; stato linciato"}
	titolo = "E' risolto!"

class EventParserFirstBlood(EventParser):
	flags = [100]
	testo = """
<p>A seguito delle indagini seguite riguardo la morte misteriosa avvenuta questa notte, gli investigatori sono giunti a sospettare il coinvolgimento di un lupo mannaro.<br>
Vari segni della creatura sono stati trovati sul corpo della vittima e nei d'intorni.<br>
Le autorità regionali hanno disposto la quarantena del villaggio interessato per salvaguardare i borghi vicini. Ora gli abitanti dovranno cavarsela da soli.</p>
	"""
	def append(self, event) :
		self.event = event
	def __call__(self):
		return [EventJournalObject("Lupi Mannari nel villaggio !!", self.event.date, text=self.testo, nice=20)]

EVENTI_PARSER = {
	1  : EventParserMorte,
	3  : EventParserSuicidioNotte,
	5  : EventParserSuicidioGiorno,
	4  : EventParserSam,
	14 : EventParserSam,
	2  : EventParserLinciaggio,
	11 : EventParserElezione,
	21 : EventParserPaparazzi,
	31 : EventParserResurezzione,
	32 : EventParserLinciaggioFallito,
	100: EventParserFirstBlood,
}

MSG_VITTORIA = {
	" ": {"titolo":"Tutti morti !","imm"   :"",},
	"P": {"titolo":"Il villaggio è stato liberato dai lupi !","imm"   :"",},
	'L': {"titolo":"I Lupi hanno vinto !","imm"   :"",},
	'C': {"titolo":"Hanno vinto i ... Ops, no. Ha vinto Lui !","imm"   :"",},
}

class EndGameEventJournalObject(EventJournalObject):
	def __init__(self, fazione, date):
		self.titolo = MSG_VITTORIA[fazione]["titolo"]
		self.nice = -1
		self.date = date
		self.template = "lupus/events/END.html"
		self.imm = MSG_VITTORIA[fazione]["imm"]
		self.context = {"fazione":fazione}

