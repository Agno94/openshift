# -*- encoding: utf8 -*-
from django.shortcuts import get_object_or_404
from django.http import HttpResponse, HttpResponseRedirect, Http404
from django.core.urlresolvers import reverse, reverse_lazy
from django.views.decorators.csrf import *

import json
import random
import os

from lupus.time import *
from lupus.models import *
from lupus.views import LupusGenericView, LupusLoggedView

@ensure_csrf_cookie
def getcookie(request) :
	return HttpResponse("")

