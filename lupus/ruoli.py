# -*- encoding: utf8 -*-
from lupus.setting import RUOLI, RUOLI_CHOICES, Ruolo, GAMETIME_DAYSTART, GAMETIME_NIGHTSTART, EVENTI_FLAG
from lupus.time import localnow, isnowday, isnownight, giornodigioco
from lupus.models import Eventi, Nota, Utente, Azioni, Match
from lupus.engine import applica_calcolo
import datetime

def power_time_night(**kwargs):
	if not isnownight(kwargs.get("now", localnow())) :
		return "Non è ancora notte. Aspetta il calar del sole (%s) per agire"%(GAMETIME_NIGHTSTART.strftime("%H:%M"))
	return ""

def power_time_day(**kwargs):
	if not isnowday(kwargs.get("now", localnow())) :
		return "Puoi usare il tuo potere soltanto durante il giorno. Aspetta le %s per agire"%(GAMETIME_DAYSTART.strftime("%H:%M"))
	return ""

def power_msg_passive(**kwargs):
	return "Il tuo ruolo ha un potere passivo, ovvero non decidi tu quando usarlo ma ha effetto in automatico"

def power_hasdone_perday(**kwargs):
	giorno = kwargs.get("giorno")
	gID = kwargs.get("gid",0)
	if gID :
		return Azioni.objects.filter(agente__gruppo_id=gID, time=giorno).exists()
	uID = kwargs.get("uid",0)
	return Azioni.objects.filter(agente_id=uID, time=giorno).exists()

def power_hasdone_permatch(**kwargs):
	giorno = kwargs.get("giorno")
	gID = kwargs.get("gid",0)
	if gID :
		return Azioni.objects.filter(agente__gruppo_id=gID).exists()
	uID = kwargs.get("uid",0)
	return Azioni.objects.filter(agente_id=uID).exists()

def power_passate_perday(**kwargs):
	gID = kwargs.get("gid",0)
	if gID :
		return Azioni.objects.filter(agente__gruppo_id=gID).values("time","agente__nome", "bersaglio__nome").order_by("-time")
	uID = kwargs.get("uid",0)
	return Azioni.objects.filter(agente_id=uID).values("bersaglio__nome", "time").order_by("-time")

def power_passate_permatch(**kwargs):
	uID = kwargs.get("uid",0)
	return Azioni.objects.filter(agente_id=uID).values("time", "bersaglio__nome").first()

def power_passate_veggente(**kwargs):
	uID = kwargs.get("uid",0)
	# Lista di dizionari con "bersaglio", "date", "aura" solo per azioni precedenti alla notte in corso
	azioni_index = dict()
	azioni = Azioni.objects.filter(agente_id=uID).values("bersaglio__nome", "time").order_by("-time")
	for azione in azioni:
		azione["aura"] = "?"
		azioni_index[azione["time"]] = azione
	for nota in Nota.objects.filter(utente1_id=uID, flags="VEG").values("opt", "time"):
		azioni_index[nota["time"]]["aura"] = nota["opt"]
	return azioni

def power_bersagli_standard(**kwargs):
	match = kwargs.get("match",0)
	match_id = match.id if match else 0
	bersagli = Utente.objects.filter(match_id = match_id, stato=0)
	if not kwargs.get("gid",0):
		uID = kwargs.get("uid",0)
		bersagli = bersagli.exclude(id=uID).exclude(ruolo="GM")
	return bersagli

def power_bersagli_LUPI(**kwargs):
	giorno = kwargs.get("giorno", localnow().date())
	match = kwargs.get("match",0)
	if (giorno <= match.startdate+datetime.timedelta(days=1)) :
		return Utente.objects.filter(match_id = match.id, stato=0, ruolo="GM")
	else :
		return power_bersagli_standard(**kwargs)

def power_bersagli_BDG(**kwargs):
	uID = kwargs.get("uid",0)
	if not Azioni.objects.filter(agente_id=uID, bersaglio=uID).exists() :
		kwargs["gID"]=True
	return power_bersagli_standard(**kwargs)

def power_bersagli_morti(**kwargs):
	match = kwargs.get("match",0)
	match_id = match.id if match else 0
	return Utente.objects.filter(match_id = match_id, stato=1).exclude(ruolo="GM")

def power_bersagli_tutti(**kwargs):
	return Utente.objects.filter(match = kwargs.get("match",0))

def power_do_action_default(azione):
	azione.save()
	return

from lupus.status import *
def power_do_action_SAM(azione):
	esito = {"Morti": [azione.bersaglio.id], "Eventi":[{"flags": EVENTI_FLAG["Sam"], "utente":azione.agente.id}, {"flags": EVENTI_FLAG["MSam"], "utente":azione.bersaglio.id}]}
	for s in Status.objects.all().filter(utente_id=azione.bersaglio.id): #Per ogni status verifico se devo alterare l'esito
		data = STATUS_DATA[s.get_ID()]
		if data.get("altera_giorno", False):
			filter = data.get("filtro_giorno", False)
			if (filter): #Solo per sicurezza, forse non serve
				new = filter(s, esito)
				esito["Morti"] += new["Morti"]
				esito["Eventi"] += new["Eventi"]
	applica_calcolo(esito, giornodigioco(localnow()))
	return power_do_action_default(azione)

def power_other_notfirstnight(**kwargs):
	giorno = kwargs.get("giorno", localnow().date())
	match = kwargs.get("match",0)
	return match and (giorno > match.startdate+datetime.timedelta(days=1))

# has msg time hasdone other passate past_tpl context form_tpl bersagli do_action

RUOLI_NOTTURNI = ["VEG","BDG","MAG","PAP","BOMB","GUAR","MED","LUP","PUTT","STR","DEV","POL","ESP"]
RUOLI_DIURNI = ["SAM"]

def registra():
	#Il Sam agisce di giorno
	for ruolo in RUOLI_DIURNI :
		RUOLI[ruolo].set_power("time", power_time_day)
	#Ruoli che agisco di notte
	for ruolo in RUOLI_NOTTURNI :
		RUOLI[ruolo].set_power("time", power_time_night)
	
	RUOLI["MESS"].set_power("msg", power_msg_passive)
	RUOLI["CACC"].set_power("msg", power_msg_passive)
	
	# Ruoli che agiscono una volta per turno
	for ruolo in ["VEG","BDG","MAG","PAP","BOMB","MED","LUP","PUTT","STR","DEV","POL","ESP"] :
		RUOLI[ruolo].set_power("hasdone", power_hasdone_perday)
		RUOLI[ruolo].set_power("passate", power_passate_perday)
	# Ruoli che agiscono una volta nel match
	for ruolo in ["SAM","GUAR"] :
		RUOLI[ruolo].set_power("hasdone", power_hasdone_permatch)
		RUOLI[ruolo].set_power("passate", power_passate_permatch)
		RUOLI[ruolo].set_power("past_tpl", "onlyone.html")
	
	for ruolo in ["VEG", "MED", "DEV"] :
		RUOLI[ruolo].set_power("past_tpl", "veg.html")
		RUOLI[ruolo].set_power("passate", power_passate_veggente)
	
	# Ruoli che agiscono sui vivi
	for ruolo in ["VEG", "MAG", "PAP", "BDG", "BOMB", "SAM", "LUP", "PUTT", "DEV", "POL", "ESP"] :
		RUOLI[ruolo].set_power("bersagli", power_bersagli_standard)
	# Ruoli che agiscono sui morti
	for ruolo in ["GUAR", "MED"] :
		RUOLI[ruolo].set_power("bersagli", power_bersagli_morti)
	# Ruoli che agiscono su tutti
	for ruolo in ["STR"] :
		RUOLI[ruolo].set_power("bersagli", power_bersagli_tutti)
	# Lupi
	RUOLI["LUP"].set_power("bersagli", power_bersagli_LUPI)
	# Guardia ?
	# RUOLI["BDG"].set_power("bersagli", power_bersagli_BDG)
	
	# Ruoli che non agiscono la prima notte
	for ruolo in ["GUAR","BDG","MAG","BOMB","MED","PUTT","POL","ESP"] :
		RUOLI[ruolo].set_power("other", power_other_notfirstnight)
		RUOLI[ruolo].set_power("msg", (lambda **kwargs: "Il tuo ruolo non può agire la prima notte"))
	
	for ruolo in RUOLI.keys():
		RUOLI[ruolo].set_power("do_action", power_do_action_default)
	RUOLI["SAM"].set_power("do_action", power_do_action_SAM)
	
	return

registra()
