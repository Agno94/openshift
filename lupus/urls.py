#from django.conf.urls import patterns, url, include
from django.conf.urls import url, include

from lupus import views
from lupus import gmviews
#from lupus import appviews

app_name = "lupus"

gm_patterns = [
	#SEZIONE GM
	url(r"^$", gmviews.GMView.as_view(), name="gm_index"),
	url(r"^(?P<id>\d+)$", gmviews.GMView.as_view(), name="gm_index_single"),
	#GESTIONE RUOLI
	url(r"^ruoli/(?P<match_id>\d*)$", gmviews.GMRolesView.as_view(), name="gm_assegnazione_ruoli"),
	url(r"^ruoli/users$", gmviews.gm_user_match, name="gm_user_match"),
	url(r"^ruoli/set$", gmviews.gm_set_role, name="gm_set_user_role"),
	url(r"^ruoli/setusers$", gmviews.gm_set_users_role, name="gm_set_users_role"),
	url(r"^ruoli/set/random$", gmviews.gm_set_role_random, name="gm_set_user_role_random"),
	#GESTIONE EVENTI
	url(r"^eventi/(?P<match_id>\d*)$", gmviews.GMEventsView.as_view(), name="gm_gestione_eventi"),
	url(r"^eventi/modifica$", gmviews.gm_edit_event, name="gm_modifica_evento"),
	url(r"^eventi/elimina$", gmviews.gm_del_event, name="gm_elimina_eveto"),
	url(r"^eventi/add", gmviews.gm_add_event, name="gm_aggiungi_evento"),
	#GESTIONE NOTE
	url(r"^note/(?P<match_id>\d*)$", gmviews.GMNotaView.as_view(), name="gm_gestione_note"),
	url(r"^note/modifica$", gmviews.gm_edit_nota, name="gm_modifica_nota"),
	url(r"^note/elimina$", gmviews.gm_del_nota, name="gm_elimina_nota"),
	url(r"^note/add", gmviews.gm_add_nota, name="gm_aggiungi_nota"),
	#GESTIONE AZIONI
	url(r"^azioni/(?P<match_id>\d*)$", gmviews.GMActionView.as_view(), name="gm_gestione_azioni"),
	url(r"^azioni/modifica$", gmviews.gm_edit_action, name="gm_modifica_azione"),
	url(r"^azioni/elimina$", gmviews.gm_del_action, name="gm_elimina_azione"),
	url(r"^azioni/add", gmviews.gm_add_action, name="gm_aggiungi_azione"),
	#GESTIONE STATI
	url(r"^status/(?P<match_id>\d*)$", gmviews.GMStatusView.as_view(), name="gm_gestione_status"),
	url(r"^status/modifica$", gmviews.gm_edit_status, name="gm_modifica_status"),
	url(r"^status/clear$", gmviews.gm_clear_status, name="gm_pulizia_status"),
	url(r"^status/random$", gmviews.gm_status_random, name="gm_assegnamento_status_random"),
	#GESTIONE GRUPPI
	url(r"^gruppi$", gmviews.GMGroupsView.as_view(), name="gm_gestione_gruppi"),
	url(r"^gruppi/add$", gmviews.gm_add_group, name="gm_aggiungi_gruppo"),
	url(r"^gruppi/edit$", gmviews.gm_edit_group, name="gm_modifica_gruppo"),
	url(r"^gruppi/del$", gmviews.gm_del_group, name="gm_elimina_gruppo"),
	#GESTIONE MATCH
	url(r"^match$", gmviews.GMMatchView.as_view(), name="gm_gestione_match"),
	url(r"^match/add$", gmviews.gm_add_match, name="gm_aggiungi_match"),
	url(r"^match/edit$", gmviews.gm_edit_match, name="gm_modifica_match"),
	url(r"^match/del$", gmviews.gm_del_match, name="gm_elimina_match"),
	url(r"^match/deactivate$", gmviews.gm_deactivate_users_match, name="gm_disattiva_utenti_match"),
	#GESTIONE VOTAZIONI
	url(r"^votazioni/(?P<match_id>\d*)$", gmviews.GMVotiView.as_view(), name="gm_gestione_voti"),
	url(r"^votazioni/modifica$", gmviews.gm_edit_voto, name="gm_modifica_voto"),
	url(r"^votazioni/elimina$", gmviews.gm_del_voto, name="gm_elimina_voto"),
	url(r"^votazioni/add$", gmviews.gm_add_voto, name="gm_aggiungi_voto"),
	#GESTIONE NOTTE E LINCIAGGIO
	url(r"^notte/(?P<match_id>\d*)$", gmviews.GMValView.as_view(), name="gm_gestione_notte", kwargs={"flags": 2}),
	url(r"^notte/confirm$", gmviews.GMValConfirmView.as_view(), name="gm_conferma_notte"),
	url(r"^notte/do$", gmviews.GMValDoPostView.as_view(), name="gm_esegui_notte"),
	#url(r"^notte/do$", gmviews.gm_val_do, name="gm_esegui_notte"),
	url(r"^linciaggio/(?P<match_id>\d*)$", gmviews.GMValView.as_view(), name="gm_linciaggio", kwargs={"flags": 1}),
	url(r"^linciaggio/confirm$", gmviews.GMValConfirmView.as_view(), name="gm_conferma_linciaggio"),
	url(r"^linciaggio/do$", gmviews.GMValDoPostView.as_view(), name="gm_esegui_linciaggio"),
	#url(r"^linciaggio/do$", gmviews.gm_val_do, name="gm_esegui_linciaggio"),
	#CAMBIO PROFILO
	url(r"^profile$", gmviews.GMChangeProfileView.as_view(), name="gm_setprofile"),
	#GESTIONE FINE MATCH
	url(r"^end/(?P<match_id>\d*)$", gmviews.GMEndMatch.as_view(), name="gm_end_match"),
	#GESTIONE GM
	url(r"^staff$", gmviews.GMStaffView.as_view(), name="gm_staff"),
	url(r"^staff/add$", gmviews.gm_add_staff, name="gm_staff_add"),
	url(r"^su/add$", gmviews.gm_add_su, name="gm_su_add"),
	url(r"^su/del/(?P<id>\d+)$", gmviews.gm_del_su, name="gm_su_elimina"),
	#GESTIONE ISCRITTI
	url(r"^iscritti/(?P<match_id>\d+)$", gmviews.GMIscrittiView.as_view(), name="gm_gestione_iscritti"),
	url(r"^iscritti/edit$", gmviews.gm_edit_iscritto, name="gm_modifica_iscritto"),
	url(r"^iscritti/del$", gmviews.gm_del_iscritto, name="gm_elimina_iscritto")
]



urlpatterns = [
	# INDICE E GIORNALE
	url(r"^$", views.Index.as_view(), name ="index"),
	url(r"^index$", views.Index.as_view()),
	url(r"^giornale(?P<match_id>\d+)$", views.GiornaleView.as_view(), name="giornale"),
	url(r"^giornale/pdf(?P<match_id>\d+)$", views.notImplemented.as_view(), name ="giornale_pdf"),
	# REGOLAMENTO
	url(r"^regolamento$", views.RegView.as_view(), name ="regolamento"),
	url(r"^regolamento/online$", views.RegOnlineView.as_view(), name ="regolamento_online"),
	# GESTIONE MATCH
	url(r"^game$", views.GameView.as_view(), name ="gioco"),
	url(r"^game/leave$", views.GameLeaveView.as_view(), name ="gioco_leave"),
	url(r"^game/askleave$", views.GameAskLeaveView.as_view(), name ="gioco_askleave"),
	# GESTIONE PROFILO
	url(r"^profile$", views.ProfileView.as_view(), name ="profile"),
	url(r"^profile/make$", views.ProfileMakeView.as_view(), name ="profile_make"),
	url(r"^profile/(?P<user_id>\d+)$", views.ProfileOtherView.as_view(), name="profile_other"),
	# RIASSUNTO STATUS
	url(r"^status$", views.StatusView.as_view(), name ="status"),
	url(r"^status/postaction$", views.StatusActionView.as_view(), name ="status_action"),
	# VILLAGGIO E VOTAZIONI
	url(r"^villaggio/pastsindaco", views.VotazioniPassateSindacoView.as_view(), name ="villaggio_pastsindaco"),
	url(r"^villaggio/past", views.VotazioniPassateView.as_view(), name ="villaggio_past"),
	#url(r"^villaggio/postvoto(?P<flag_id>\d{1})_(?P<utente_id>\d+)$", views.VillaggioPostVotoView.as_view(), name ="villaggio_postvoto"),
	url(r"^villaggio/postvoto$", views.VillaggioPostFormVotoView.as_view(), name ="villaggio_postvoto"),
	url(r"^villaggio", views.VillaggioView.as_view(), name ="villaggio"),
	# PROPRIO GRUPPO E AZIONI NOTTURNE
	url(r"^gruppo$", views.GruppoView.as_view(), name ="gruppo"),
	url(r"^gruppo/azione$", views.GruppoAzioneView.as_view(), name ="gruppo_azione"),
	# CHAT DI GRUPPO
	url(r"^gruppo/postmsg$", views.gruppo_postmsg, name ="gruppo_postmsg"),
	url(r"^gruppo/getmsg(?P<isvillaggio>\d{1})_(?P<first_id>\d+)to(?P<last_id>\d+)$", views.gruppo_getmsg, name ="gruppo_getmsg"),
	# RIASSUNTO
	url(r"^summary$", views.SummaryCurrentView.as_view(), name ="summary_current"),
	url(r"^summary/match(?P<match_id>\d+)$", views.SummaryView.as_view(), name ="summary"),
	url(r"^summary/history(?P<match_id>\d+)$", views.SummaryFullView.as_view(), name ="summary_full"),
	url(r"^summary/pdf(?P<match_id>\d+)$", views.notImplemented.as_view(), name ="summary_pdf"),
	
	url(r"^IE.html$", views.IEwarningView.as_view(), name="IEwarning"),
	
	# GAME MASTER
	url(r'^gm/', include((gm_patterns,""))),
	
	# PROCEDURE AUTOMATICHE
	url(r'^auto/', include(
		([
			url(r"^giorno_all$", views.esegui_giorno_all_view, name ="giorno_all"),
			url(r"^notte_all$", views.esegui_notte_all_view, name ="notte_all"),
			url(r"^vittoria_all$", views.esegui_vittoria_all_view, name ="vittoria_all")
		],"auto")
	,namespace="auto")),
]
