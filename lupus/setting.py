# -*- encoding: utf8 -*-
from django.conf import settings
from datetime import time

LRT_VERSION = 1

CHAT_MSG_PER_REQUEST = 20
CHAT_INTERVALL_REQUEST = 4000

GAMETIME_DAYSTART = time(9, 00)
GAMETIME_DAYEND = time(21, 20)
GAMETIME_NIGHTSTART = time(21, 30)
GAMETIME_NIGHTEND = time(8, 00)
GAMETIME_WEEKENDISNIGTH = True

VOTO_FLAG = {
	"LINCIAGGIO":1,
	"SINDACO":2
}

STATUS_FLAG = {
	1:{
		"ID":"SIND",
		"Nome":"Sindaco",
		"DOC":"Il Sindaco è l'abitante del villaggio scelto, grazie alla sua autorevolezza o al suo portafoglio, dai compaesani per governare il villaggio.<br>" + \
			"Per ogni momento del gioco esiste un solo sindaco vivo. I voti del Sindaco nelle votazioni valgono 1+<i>epsilon</i> con 0<<i>epsilon</i><1, quindi in caso di pareggio in giocatore votato dal Sindaco sarà scelto.",
		"DOC_OPT":"Il campo OPT contiene il numero del giorno in cui viene eletto, considerando il primo giorno come giorno 0",
	},
	2:{
		"ID":"VECC",
		"Nome":"Vecchio",
		"DOC":"Il Vecchio è un abitante di età avanzata dalla debole costituzione e dalla salute traballante. Purtroppo non gli rimangono ancora molti giorni di vita. <br>"+\
			"Ogni notte il vecchio ha una probabilità del (5+2<i>x</i>) per cento di morire, con <i>x</i> il numero di giorni trascorsi dall'inizio del match. La sua morte non apparirà diversa da quelle avvenute per altre cause.",
		"DOC_OPT":"",
	},
	3:{
		"ID":"INN",
		"Nome":"Innamorato",
		"DOC":"Gli innamorati sono un gruppo di persone legati da una passione strugente. Provano sentimenti vicendevoli al punto da non poter credere di poter vivere senza il proprio/i propri amati. <br>"+ \
			"Qualora uno di loro dovesse morire il suo compagno/la sua compagna/i suoi compagni si suiciderebbero per seguirlo nell'aldilà.",
		"DOC_OPT":"Il campo OPT contiene un ID che associa tutte le persone mutualmente innamorate.\nGli status di due persone innamorate hanno lo stesso OPT; come anche due persona arroccate e il rocco hanno tutti e 3 lo stesso OPT.",
	},
	5:{
		"ID":"CUP",
		"Nome":"Cupido",
		"DOC":"Il cupido è un abile arciere che sa come colpire al cuore le persone. <br>"+\
			"Può scaglierà due frecce incantate: le due persone colpite non potranno far altro che giurarsi amore a vicenda, tanto che nel caso in cui uno dei due dovesse morire, l'altro sarà pronto a suicidarsi per amore.",
		"DOC_OPT":"Il campo OPT contiene 0 se il potere deve ancora essere usato, un numero positivo altrimenti",
	},
	6:{
		"ID":"ROC",
		"Nome":"Rocco",
		"DOC":"Il rocco è un geometra mancato, che vede soltanto triangoli nella sua vita, anche in amore. <br>"+\
			"La prima notte di gioco inviterà due persone a far parte del suo triangolo amoroso e queste non potranno rifiutarsi. "+\
			"I tre cominceranno ad amarsi l'un l'altro a tal punto che se anche solo uno dei tre dovesse morire, gli altri due si suiciderebbero pur di poterlo raggiungere nell'aldilà.",
		"DOC_OPT":"Il campo OPT contiene 0 se il potere deve ancora essere usato, un numero positivo altrimenti",
	},
	7:{
		"ID":"ADE",
		"Nome":"Amico dell'espansivo",
		"DOC":"Gli amici dell'espansivo sono le persone a cui l'espansivo ha fatto visita nottetempo rivelando loro il suo ruolo. Sono tanto impressionati da lui che gli rimarranno amici per sempre.",
		"DOC_OPT":"Il campo OPT contiene l'ID del giocatore Espansivo",
	},
}

NEXT_STATUS_FLAG = {
	"SIND":{
		"ID":"SIND",
		"Nome":"Sindaco",
		"DOC":"Il Sindaco è l'abitante del villaggio scelto, grazie alla sua autorevolezza o al suo portafoglio, dai compaesani per governare il villaggio.<br>" + \
			"Per ogni momento del gioco esiste un solo sindaco vivo. I voti del Sindaco nelle votazioni valgono 1+<i>epsilon</i> con 1<<i>epsilon</i><1, quindi in caso di pareggio in giocatore votato dal Sindaco sarà scelto.",
		"DOC_OPT":"Il campo OPT contiene il numero del giorno in cui viene eletto, considerando il primo giorno come giorno 0",
	},
	"VECC":{
		"ID":"VECC",
		"Nome":"Vecchio",
		"DOC":"Il Vecchio è un abitante di età avanzata dalla debole costituzione e dalla salute traballante. Purtroppo non gli rimangono ancora molti giorni di vita. <br>"+\
			"Ogni notte il vecchio ha una probabilità del (5+2<i>x</i>) per cento di morire, con <i>x</i> il numero di giorni trascorsi dall'inizio del match. La sua morte non apparirà diversa da quelle avvenute per altre cause.",
		"DOC_OPT":"",
	},
	"INN":{
		"ID":"INN",
		"Nome":"Innamorato",
		"DOC":"Gli innamorati sono un gruppo di persone legati da una passione strugente. Provano sentimenti vicendevoli al punto da non poter credere di poter vivere senza il proprio/i propri amati. <br>"+ \
			"Qualora uno di loro dovesse morire il suo compagno/la sua compagna/i suoi compagni si suiciderebbero per seguirlo nell'aldilà.",
		"DOC_OPT":"Il campo OPT contiene un ID che associa tutte le persone mutualmente innamorate.\nGli status di due persone innamorate hanno lo stesso OPT; come anche due persona arroccate e il rocco hanno tutti e 3 lo stesso OPT.",
	},
	"CUP":{
		"ID":"CUP",
		"Nome":"Cupido",
		"DOC":"Il cupido è un abile arciere che sa come colpire al cuore le persone. <br>"+\
			"Può scaglierà due frecce incantate: le due persone colpite non potranno far altro che giurarsi amore a vicenda, tanto che nel caso in cui uno dei due dovesse morire, l'altro sarà pronto a suicidarsi per amore.",
		"DOC_OPT":"Il campo OPT contiene 0 se il potere deve ancora essere usato, un numero positivo altrimenti",
	},
	"ROC":{
		"ID":"ROC",
		"Nome":"Rocco",
		"DOC":"Il rocco è un geometra mancato, che vede soltanto triangoli nella sua vita, anche in amore. <br>"+\
			"La prima notte di gioco inviterà due persone a far parte del suo triangolo amoroso e queste non potranno rifiutarsi. "+\
			"I tre cominceranno ad amarsi l'un l'altro a tal punto che se anche solo uno dei tre dovesse morire, gli altri due si suiciderebbero pur di poterlo raggiungere nell'aldilà.",
		"DOC_OPT":"Il campo OPT contiene 0 se il potere deve ancora essere usato, un numero positivo altrimenti",
	},
	"ADE":{
		"ID":"ADE",
		"Nome":"Amico dell'espansivo",
		"DOC":"Gli amici dell'espansivo sono le persone a cui l'espansivo ha fatto visita nottetempo rivelando loro il suo ruolo. Sono tanto impressionati da lui che gli rimarranno amici per sempre.",
		"DOC_OPT":"Il campo OPT contiene l'ID del giocatore Espansivo",
	},
}

STATUS_ID = {
	"SIND":1,
	"VECC":2,
	"INN":3,
	"CUP":5,
	"ROC":6,
	"ADE":7,
}

STATUS_CHOICES = tuple((x, y["Nome"]) for x,y in STATUS_FLAG.items())

STATUS_IMG_FORMAT = "lupus-images/status-images/%s48.png"

EVENTI = {
	0 : {
		"Help":"",
		"scenes":0,
	},
	1  : {
		"Help":"Morte notturna",
		"scenes":3,
	},
	2  : {
		"Help":"Esecuzione",
		"scenes":4,
		"":"",
	},
	3  : {
		"Help":"Suicidio Notturno",
		"scenes":3,
		"":"",
	},
	4  : {
		"Help":"Bersaglio Sam",
		"scenes":0,
		"":"",
	},
	5  : {
		"Help":"Suicidio Diurno",
		"scenes":2,
		"":"",
	},
	11 : {
		"Help":"Elezione a sindaco",
		"scenes":0,
		"":"",
	},
	14 : {
		"Help":"Azione Sam",
		"scenes":0,
		"":"",
	},
	21 : {
		"Help":"Paparazzato",
		"scenes":1,
	},
	31 : {
		"Help":"Resurezzione standard",
		"scenes":1,
	},
	32 : {
		"Help":"Linciaggio fallito",
		"scenes":1,
	},
	100: {
		"Help":"Inizio",
		"scenes":0,
	},
# ESPLOSIONE
}

EVENTI_FLAG = {
	"MNott":1,
	"MLinc":2,
	"Suic" :3,
	"MSam" :4,
	"SuicD" :5,
	"Sind" :11,
	"Sam"  :14,
	"Papar":21,
	"Res"  :31,
	"NoLin":32,
	"Start":100,
}

EVENTI_DIURNI = [EVENTI_FLAG[i] for i in ["MLinc","MSam","SuicD","Sind","Sam"]]

EVENTI_NOTTURNI = [EVENTI_FLAG[i] for i in ["MNott","Suic","Papar","Res"]]

EVENTI_CHOICES = tuple((x, y["Help"]) for x,y in EVENTI.items())

FAZIONI = {" ":"","":"","P":'Popolani','L':'Lupi','C':'Criceto'}

FAZIONI_MSG = {
	"":"",
	" ":"nessuno ha vinto",
	"P":"i Popolani hanno vinto",
	"L":"i Lupi hanno vinto",
	"C":"il Criceto ha vinto",
}

FAZIONI_CHOICES = tuple(FAZIONI.items())

class Ruolo(object):
	def __init__(self, id, fazione="", aura=1, esce=False, entra=False, nome="", doc="", speed=0, active_power=0) :
		self.id=id
		self.fazione=fazione
		self.aura=aura
		self.esce=esce
		self.entra=entra
		self.nome=nome
		self.doc=doc
		self.active_power=active_power
		self.speed=speed
		self.power_funcs = {
			"has"  : self.power_default_has,
			"msg"  : self.power_default_msg,
			"time" : (lambda **kwargs: ""),
			"hasdone"  : (lambda **kwargs: True),
			"other"    : (lambda **kwargs: True),
			"bersagli" : (lambda **kwargs: []),
			"context"  : (lambda **kwargs: dict()),
			"passate"  : (lambda **kwargs: []),
			"past_tpl" : "list.html",
			"form_tpl" : "default.html",
			"do_action": (lambda azione: None),
		}
	def __bool__(self):
		return bool(self.id)
	def __str__(self):
		return self.nome;
	def __repr__(self):
		return "< %s - %s >"%(self.id, self.nome)
	def fazione_human(self):
		return FAZIONI[self.fazione]
	def set_power(self, nome, func):
		self.power_funcs[nome] = func
		return
	def get_power(self, nome):
		return self.power_funcs[nome]
	def call(self, nome, *args, **kwargs):
		return (self.power_funcs[nome])(*args, **kwargs)
	def power_default_has(self, **kwargs) :
		return self.active_power
	def power_default_msg(self, **kwargs) :
		return "Il tuo ruolo non ha poteri particolari"

RUOLI = {
	"GM": Ruolo(
		"GM"," ",10,False,False,"Gamemaster",
		"Sei il Gamemaster, muori la prima notte"
	),
	"CON": Ruolo(
		"CON","P",1,False,False,"Contadino",
		"Il contadino è un abitante del villaggio che subisce l'invasione dei lupi mannari, non ha alcun potere particolare, quindi deve cercare di sopravvivere per votare durante il giorno."
	),
	"MAS": Ruolo(
		"MAS","P",2,False,False,"Massone",
		"La massoneria è un'organizzazione segreta i cui membri si conoscono ed aiutano a vicenda. Il massone è un popolano che non ha poteri particolari, ma conosce gli altri membri della sua organizzazione e sa di potersi fidare di loro per organizzare i suoi piani."
	),
	"VEG": Ruolo(
		"VEG","P",1,False,False,"Veggente",
		"Il veggente è un vecchio saggio che tramite l'arte imparata nel corso degli anni cerca di stanare i nemici del villaggio. Ogni notte, tramite la sua sfera di cristallo, può scegliere un abitante del villaggio e scrutarne l'animo. La mattina successiva verrà a conoscenza dell'aura di questa persona."
		,1,True
	),
	"MED": Ruolo(
		"MED","P",1,False,False,"Medium",
		"Il medium è un giovane veggente, che non sa ancora come sfruttare il suo potere al meglio. Per ora riesce a scrutare soltanto l'anima dei morti: ogni notte potrà scegliere una persona morta e la mattina seguente riceverà il responso sull'aura della persona scelta."
		,1,True
	),
	"BDG": Ruolo(
		"BDG","P",1,True,True,"Guardia del Corpo",
		"La guardia del corpo è un popolano che cerca di proteggere i buoni dai lupi. Così ogni notte sceglie una persona e la protegge dai lupi: se un lupo e la guardia agiscono sulla stessa persona, quella persona non verrà sbranata dai lupi."+\
		"Ogni notte la guardia può scegliere di proteggere chi vuole, anche la stessa persona più di una volta."
		,25,True
	),
	"MAG": Ruolo(
		"MAG","P",1,True,False,"Mago",
		"Il mago è un popolano che tenta di proteggere i suoi compaesani tramite la magia. Ogni notte sceglie una persona, si avvicina alla sua casa e urla: ''Speculum!''. In questo modo crea un enorme specchio davanti alla casa del giocatore prescelto: chiunque provi ad usare il suo potere contro questa persona, si ritrova ad usare il proprio potere contro se stesso."
		,100,True
	),
	"PAP": Ruolo(
		"PAP","P",1,True,False,"Paparazzi",
		"Il paparazzi è il tipico personaggio invasivo sempre alla ricerca di scoop. Ogni notte sceglie una persona e si apposta di fronte alla sua casa per vedere cosa succede: vede così se quella persona esce e quanti e quali persone entrano a casa del malcapitato."+\
		" La mattina dopo uscirà un articolo di giornale pubblico in cui tutti potranno leggere cosa è successo qualla notte nella casa del paparazzato."
		,4,True
	),
	"BOMB": Ruolo(
		"BOMB","P",2,True,True,"Bombarolo",
		"Il bombarolo è un popolano che cerca di eliminare i lupi ed i loro complici tramite l'utilizzo di esposivi. Purtroppo a fare le spese delle sue azioni sono spesso anche i suoi alleati."+\
		" Ogni notte egli entra in una casa e ci piazza una bomba: se un'altra persona entra nella casa questa esplode ed il proprietario della casa con tutte le persone entrate, eccetto il bombarolo, muoiono."
		,10,True
	),
	"MESS": Ruolo(
		"MESS","P",1,False,False,"Messia",
		"Il messia è quasi un semplice contadino, ma con un potere speciale: una volta morto, all'alba del terzo giorno resuscita e torna nel mondo dei vivi, a parteggiare ancora una volta per i popolani."
	),
	"GUAR": Ruolo(
		"GUAR","P",1,True,True,"Guaritore",
		"Il guaritore conosce tutte le erbe mediche e sa come utilizzarle al meglio. Tuttavia la sua scorta di erbe è limitata, quindi può agire soltanto una volta in tutta la partita: durante la notte può scegliere di resuscitare una persona morta. Questa ritornerà in vita il giorno seguente riacquistando i propri poteri speciali."
		,30,True
	),
	"SAM": Ruolo(
		"SAM","P",2,True,False,"Sam",
		"Il Sam è un vecchietto all'apparenza dolce e tranquillo ma dentro di lui nutre un odio represso verso gli altri componenti del villaggio. Il suo odio è così ben radicato che a un certo punto del gioco, durante il giorno, potrà alzarsi in piedi, puntare il dito contro una persona e gridare: «Muori stronzo!»."+\
		" La persona in questione morirà immediatamente di attacco cardiaco: chi se lo sarebbe mai aspettato che un vecchietto tanto docile potesse arrivare a tanto? Per fortuna il Sam è un popolano buono, quindi cercherà di uccidere i lupi; per sfortuna, può usare il suo potere soltanto una volta in tutta la partita."
		,200,True
	),
	"CACC": Ruolo(
		"CACC","P",1,False,False,"Cacciatore",
		"Il cacciatore è un popolano temerario sempre pronto a sparare ai lupi, tanto che gira sempre con il suo fucile in spalla."+\
		"Se di notte uno dei lupi proverà a sbranarlo, lui riuscirà a fare soltanto una cosa prima di morire: sparare al suo assassino. Così cacciatore e lupo moriranno insieme, l'uno sbranato e l'altro con una pallottola nella pelliccia."
	),
	"LUP": Ruolo(
		"LUP","L",2,True,True,"Lupo",
		"I lupi mannari sono creature che la notte si trasformano in pericolosi ed affamati predatori. Si conoscono vicendevolmente (il proprio odore è per loro inconfondibile) e comunicano ogni notte tramite ulutati. Ogni notte dopo essersi consultati, uno di loro esce di casa e si reca dalla vittima predestinata per sbranarla e portare a casa la cena."
		,20,True
	),
	"LVG": Ruolo(
		"LVG","L",2,False,False,"Lupo vegano",
		"I lupi mannari sono creature che (solitamente) la notte si trasformano in pericolosi ed affamati predatori. Si conoscono vicendevolmente (il proprio odore è per loro inconfondibile) e comunicano ogni notte tramite ulutati. Tuttavia qualcuno di loro, maledetto da uno sciamano, ha recuperato parte delle sua coscienza umana. Il lupo vegano a causa di questa maledizione non riesce a uccidere nessuna creatura animale, si dovrà affidare ai suoi compagni che gli procureranno la cena"
	),
	"PUTT": Ruolo(
		"PUTT","L",1,True,True,"Puttana",
		"La prostituta è una donna infatuata di un lupo mannaro (di cui però non conosce l'identità) che ogni notte si reca da un giocatore con il doppio scopo di impedire ai popolani con poteri di agire o di passare una notte con il suo segreto amore.<br>"+\
		" Ogni notte sceglie un giocatore e si reca a casa sua, questi sarà impegnato la notte e non porterà a compimento nessuna azione che aveva pianificato."
		,100,True
	),
	"STR": Ruolo(
		"STR","L",2,False,False,"Strega",
		"La strega è una vecchiaccia che parteggia per i lupi e vince insieme a loro. Durante la sua vita si è avvicinata alla magia nera e ha imparato come invertire l'aura di una persona."+\
		" Così ogni notte sceglie una persona, viva o morta; se tale persona viene scrutata durante la notte, il risultato è invertito (ovvero se ha aura bianca risulta nera, e viceversa; se indica il criceto il potere della strega non ha alcun effetto)."
		,2,True
	),
	"IND": Ruolo(
		"IND","L",1,False,False,"Indemoniato",
		"L'indemoniato è un falso contadino che parteggia per i lupi, e quindi vince insieme a loro. Durante il giorno sembra essere un semplice e comune contadino, ma il suo animo nasconde questo terribile segreto, e durante la notte spera con tutte le sue forze che un popolano possa essere sbranato dai lupi."
	),
	"DEV": Ruolo(
		"DEV","L",2,False,False,"Diavolo",
		"Il diavolo è un spirito malvagio che tempo addietro assunse forma umana per nascondersi nel villaggio. Ha interesse nella morte dei popolani ed aiuta i lupi. Ogni notte, sceglie un abitante del villaggio e, tramite i suoi oscuri poteri, ne scruta l'animo. La mattina successiva verra a conoscenza dell'aura di questa persona."+\
		" Dato che egli conosce bene la magia nera l'azione del diavolo non viene influenzata dal potere della strega."
		,1,True
	),
	"CRI": Ruolo(
		"CRI","C",0,False,False,"Criceto",
		"Il criceto mannaro sembra un normale contadino e cerca di apparire tale, ma dietro la facciata si nasconde un essere macchiavellico che approfitta della battaglia fra le altre due fazioni per raggiungere il proprio fine.<br> Il criceto non può essere sbranato dai lupi che fuggono solo percependo il suo odore."+\
		" Invece odia il veggente, infatti se questi dovesse tentare di sondare la sua aura la sua magia finirebbe per uccidere per uccidere il criceto."
	),
	"ESP": Ruolo(
		"ESP","P",1,True,True,"Espansivo",
		"L'espansivo è un popolano che non è capace di nascondere le proprie emozioni, tanto che ogni notte sceglie una persona e questa diventa \"amico dell'espansivo\" e scopre il suo ruolo."
		,40,True
	),
	"POL": Ruolo(
		"POL","P",1,True,True,"Poliziotto",
		"Il poliziotto è popolano che ogni notte sceglie una persona e  se quella non è un lupo e viene scelta dai lupi allora il poliziotto muore al suo posto, se quella persona è un lupo il poliziotto muore sbranato."
		,50,True
	),
	"": Ruolo(
		"","P",1,0,0,"",
		""
	),
}

RUOLI_CHOICES = tuple((x.id, str(x)) for x in RUOLI.values())

RUOLI_IMG_FORMAT = "lupus-images/role-images/%(ID)s32.png"

NOTE = {
	# help -> Spiegazione user friendly del timpo di nota
	# uses -> Dizionario per ha per chiavi i campi facoltativi usati e per valori una spiegazione user friendly del loro impiego
	# show -> "day" / "night" / "" a secondo se la nota va mostrata di giorno, mostata di notte o nascosta nel riassunto completo
	# show_format -> un string_format che dovra' fornite il testo da mostrare nel riepilogo
	# show_filter -> un dizionario che per chiavi a un campo (gli utenti sono utenti__nome) e per valore delle 
	#   funzioni che verranno applicate ai valori dei campi prima di essere usate in show_format
	"VEG" : {
		"help":"Registro risultati azione del veggente/diavolo/...",
		"uses":{"utente2":"Utente bersagli del veggente","opt":"Aura rivelata"},
		"show":"night",
		"show_filter":{"opt":(lambda opt: {0:"non e' riuscito a vedere",1:"ha visto Bianca",2:"ha visto Nera",10:"ha scrutato"}[opt])},
		"show_format":"%(utente1__nome)s %(opt)s l'aura di %(utente2__nome)s",
	},
	"EPAP": {
		"help":"Registro di chi il paparazzi vede entrare",
		"uses":{"utente2":"Utente che entra"},
		"show":"night",
		"show_filter":{},
		"show_format":"Il paparazzi ha visto %(utente2__nome)s entrare in casa di %(utente1__nome)s",
	},
	"UPAP": {
		"help":"Registro che dice se è uscito di casa",
		"uses":{"opt":"non 0 se l'utente esce"},
		"show":"night",
		"show_filter":{},
		"show_format":"Il paparazzi ha visto %(utente1__nome)s uscire di casa",
	},
}

NOTE_CHOICES = tuple((x, y["help"]) for x,y in NOTE.items())
