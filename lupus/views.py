# -*- encoding: utf8 -*-
from django.shortcuts import get_object_or_404
from django.http import HttpResponse, HttpResponseRedirect, Http404
from django.urls import reverse, reverse_lazy
from django.utils.http import urlquote
from django.contrib.auth.decorators import login_required, user_passes_test
from django.utils import timezone

from django.views.generic import TemplateView, RedirectView, View
#from django.views.generic.list import *
from django.utils.decorators import method_decorator

import json
import random
import glob
import os
from datetime import timedelta

from lupus.time import *
from django.contrib import messages
from lupus.models import *
from lupus.setting import *
from lupus.status import STATUS_DATA_DEFAULT, STATUS_DATA
from lupus.engine import *
from lupus.eventi import EndGameEventJournalObject
import lupus.ruoli

class IEwarningView(TemplateView):
	template_name = "IEwarning.html"

class LevelMixin(object):
	profile = None
	livello = ""
	allowed_level = {"pubblico", "loggato", "iscritto", "gioco", "finito"}
	denied_level = []
	gm_perms = ""
	gm_required = False
	def ottieni_livello(self, request, now=localnow()):
		if request.user.is_active :
			self.livello = "loggato"
			gm_perms = has_gm_permissions(request.user)
			if gm_perms :
				self.gm_perms = gm_perms
				id_profile = request.session.get("lupus_gm_asprofile",False)
				if id_profile:
					try:
						self.profile = Utente.objects.get(pk=id_profile)
					except (Utente.DoesNotExist) :
						request.session.pop("lupus_gm_asprofile")
			elif self.gm_required :
				raise Http404
			if not self.profile :
				self.profile = get_user_profile(request)
			if self.profile :
				self.livello = "iscritto"
				self.match = self.profile.match
				if self.match :
					if self.match.isStarted(now) :
						self.gruppo = self.profile.gruppo if self.profile.gruppo else Gruppo(name=RUOLI[self.profile.ruolo].nome, id=0, ruolo=self.profile.ruolo)
						self.livello = "gioco"
					elif self.match.winner != "" :
						self.livello = "finito"
		else :
			self.livello = "pubblico"
			if self.gm_required:
				raise Http404
		return
	def controlla_livello(self, **kwargs) :
		"""
		Controlla che il livello corrente sia consentito
		Ritorna False se e' cosi', True altrimenti
		"""
		livello = kwargs.get("livello", self.livello)
		return not (livello in self.allowed_level) or (livello in self.denied_level)

class LupusTemplateView(TemplateView, LevelMixin, GameTimeMixin):
	script = False
	template_subpath = "lupus/"
	extra_stylesheets = []
	extra_JSscript = []
	def get_template_names(self):
		return [ self.template_subpath + x for x in super(LupusTemplateView, self).get_template_names()]
	def dispatch(self, *args, **kwargs):
		self.init_time()
		self.ottieni_livello(self.request, self.now)
		if self.controlla_livello() :
			messages.warning(self.request,
				"La pagina %s non &egrave; accessibile<br>Alcune pagine possono essere visualizzate solo dopo il login,<br>"%(self.request.path)+
				"altre solo dagli iscritti ad un match,<br>altre ancora solo dai giocatori di un match iniziato ...")
			return HttpResponseRedirect(reverse("lupus:gioco"))
		if self.profile and (self.controlla_livello(livello="pubblico")) and (self.profile.user == self.request.user):
			try:
				LogAccesso.objects.get_or_create(timeflag = self.nightdayflag, utente = self.profile, date = self.giorno)
			except (LogAccesso.MultipleObjectsReturned):
				messages.error(self.request, "REGISTRO ACCESSO - ERRORE <i>MultiObjRet</i><br>&Egrave; avvenuto un grave errore<br>Informa presto l'amministratore")
		return super(LupusTemplateView, self).dispatch(*args, **kwargs)
	def get(self, request, *args, **kwargs):
		user_agent = self.request.META["HTTP_USER_AGENT"]
		isOldIE = (("MSIE" in user_agent) and not (("Opera" in user_agent) or ("Maxthon" in user_agent)))
		isIE = isOldIE # or (("Trident" in user_agent) and not ("Maxthon" in user_agent))
		if isIE and not (request.session.get("lupus_IEwarning", False)):
			request.session["lupus_IEwarning"] = True
			#messages.info(request, "Sembra che tu stia usando Internet Explorer<br>Cambia browser")
			return HttpResponseRedirect(reverse("lupus:IEwarning"))
		# TODO - AVVISO PER CHIEDERE L'AGGIORNAMENTO DI VECCHIO VERSIONI DI CHROME E FIREFOX
		if not request.session.get("lupus_cookies", False):
			messages.info(request, "Questo sito per funzionare correttamente richiede di salvare dei cookie<br>Non sarà altrimenti possibile effettuare l'accesso")
			request.session["lupus_cookies"] = True
		return super(LupusTemplateView, self).get(request, *args, **kwargs)
	def get_context_data(self, **kwargs):
		context = super(LupusTemplateView, self).get_context_data(**kwargs)
		if "print" in self.request.GET.keys() :
			context['layout'] = 'lupus/print_layout.html'
		context.update({"now":self.now, "time":self.time, "today":self.today}) ##DEBUG
		context['isnowday'] = self.isnowday
		context['isnownight'] = self.isnownight
		context['script'] = self.script
		context["page"] = self.request.path
		context["livello"] = self.livello
		if self.livello == "gioco" :
			context["nomegruppo"] = self.gruppo.name
			context["status_count"] = (not self.profile.stato) and (self.profile.status_set.all().count())
		context["gm_perms"] = self.gm_perms
		context["extra_stylesheet_file"] = []
		for css in self.extra_stylesheets :
			context["extra_stylesheet_file"].append("lupus/"+css+".css")
		context["extra_JSscript_file"] = []
		for jss in self.extra_JSscript :
			context["extra_JSscript_file"].append("lupus/"+jss+".js")
		return context

class LupusLoggedTemplateView(LupusTemplateView):
	@method_decorator(login_required)
	def dispatch(self, *args, **kwargs):
		return super(LupusLoggedTemplateView, self).dispatch(*args, **kwargs)

class LupusGenericView(View, LevelMixin, GameTimeMixin) :
	level_not_allowed_url = reverse_lazy("lupus:gioco")
	def dispatch(self, *args, **kwargs):
		self.init_time()
		self.ottieni_livello(self.request, self.now)
		if self.controlla_livello() :
			return HttpResponseRedirect(level_not_allowed_url)
		return super(LupusGenericView, self).dispatch(*args, **kwargs)

class LupusLoggedView(LupusGenericView) :
	@method_decorator(login_required)
	def dispatch(self, *args, **kwargs):
		return super(LupusLoggedView, self).dispatch(*args, **kwargs)

class Index(LupusTemplateView):
	template_name = "index.html"
	match_giornale = 0
	def get_giornale_context(self, **kwargs) :
		context = dict()
		max_date = (self.match_giornale.regenddate if self.match_giornale.regenddate else self.giorno) - timezone.timedelta(days=2)
		max_date = somma_giorni((self.match_giornale.regenddate if self.match_giornale.regenddate else self.giorno), -2) ## NEW
		notizie = events_parser(Eventi.objects.select_related("utente").filter(date__gte=max_date, utente__match_id=self.match_giornale.id))
		context["giorno"] = diff_giorni(self.today, self.match_giornale.startdate)
		if self.match_giornale.winner :
			notizie = [EndGameEventJournalObject(self.match_giornale.winner, self.match_giornale.enddate)] + notizie
			context["giorno"] = diff_giorni(self.match_giornale.enddate,self.match_giornale.startdate)
			context["match_winner"] = FAZIONI_MSG[self.match_giornale.winner]
		context["notizie"] = notizie
		context["ultimi_morti"] = Eventi.objects.select_related("utente").filter(flags__in = [1,3,4,5], utente__match_id=self.match_giornale.id).order_by("-date")[:4]
		context["ultimi_linciati"] = Eventi.objects.select_related("utente").filter(flags=2, utente__match_id=self.match_giornale.id).order_by("-date")[:3]
		utenti = self.match_giornale.utente_set.all()
		ruoli = {}
		for u in utenti :
			if (u.ruolo not in ["CON","GM"]) :
				ruolo = u.get_ruolo().nome
				ruoli[ruolo] = ruoli.get(ruolo, 0)+1
		context["ruoli"] = ruoli.items()
		context["totali"] = len(utenti)
		context["in_vita"] = utenti.filter(stato = 0).count()
		return context
	def get_public_context(self, **kwargs) :
		context = {"nuovi":Match.objects.filter(regenddate__gte=self.today).count}
		context["recenti"] = []
		for match in Match.objects.exclude(winner="").values("id", "nome","winner").order_by("-enddate")[:4] :
			context["recenti"].append((match["id"], match["nome"],FAZIONI_MSG[match["winner"]]))
		return context
	def get_context_data(self, **kwargs):
		if not self.match_giornale :
			if self.livello in ["gioco","finito"] :
				self.match_giornale = self.match
		if self.match_giornale :
			self.template_name = "giornale.html"
			self.extra_stylesheets = super(Index, self).extra_stylesheets + ["game-style"]
			pagecontext = self.get_giornale_context(**kwargs)
		else  :
			pagecontext = self.get_public_context(**kwargs)
		context = super(Index, self).get_context_data(**kwargs)
		context.update(pagecontext)
		context["email"] = 'lupusrealtime@gmx.com'
		return context

class GiornaleView(Index):
	template_name = "giornale.html"
	def get(self, request, *args, **kwargs):
		match_id = kwargs.get("match_id", 0)
		self.match_giornale = get_object_or_404(Match, pk=match_id)
		return super(GiornaleView, self).get(request, *args, **kwargs)

class RegView(LupusTemplateView):
	template_name = "regolamento.html"
class RegOnlineView(LupusTemplateView):
	template_name = "reg_online.html"

class WithProfileView(LupusLoggedTemplateView):
	allowed_level = ["iscritto", "gioco", "finito"]
	extra_stylesheets = ["game-style"]
	#with_profile = True

class ProfileView(WithProfileView):
	template_name = "profile.html"
	def get_context_data(self, **kwargs):
		context = super(ProfileView, self).get_context_data(**kwargs)
		context['profile'] = self.profile
		context['profile_ruolo'] = self.profile.get_ruolo()
		context['profile_ruolo_IMG'] = RUOLI_IMG_FORMAT%{"ID":self.profile.get_ruolo().id}
		context['starttime'] = GAMETIME_NIGHTSTART
		if not self.profile.stato :
			status = set(map(lambda s: s.flags, Status.objects.filter(utente_id=self.profile.id).exclude(flags=STATUS_ID["SIND"])))
			try:
				sindaco = Status.objects.filter(flags=STATUS_ID["SIND"], utente__match_id=self.match.id).order_by("-opt")[0].utente
				if sindaco.id == self.profile.id:
					status.add(STATUS_ID["SIND"])
			except (IndexError):
				pass
			stat = []
			for k, s in STATUS_FLAG.items():
				stat.append({
					"img":STATUS_IMG_FORMAT%(s["ID"]),
					"has":k in status,
					"desc":s["Nome"],
				})
			context["status"] = stat
		return context

class ProfileOtherView(WithProfileView):
	template_name = "profile_other.html"
	def get_context_data(self, **kwargs):
		context = super(ProfileOtherView, self).get_context_data(**kwargs)
		user_id = kwargs.get("user_id")
		context['profile'] = get_object_or_404(Utente, pk=user_id)
		if context['profile'].match_id != self.profile.match_id:
			raise Http404
		context['starttime'] = GAMETIME_NIGHTSTART
		return context

from django.forms import *

class ProfileForm(ModelForm):
	_given_now = None
	match = ModelChoiceField(queryset=Match.objects.filter(active=True, regenddate__gte=localnow().date()), empty_label="Scegli un match ....", required=True)#, widgets_attrs={"require":""})
	genere = TypedChoiceField(choices=((1, "Maschio"),(2, "Femmina")), widget=RadioSelect(attrs={"require":""}), empty_value=None, label="Sesso", required=True)
	class Meta:
		model = Utente
		fields = ['match', 'genere', 'nome']
		labels = {
			"nome": "Nome visualizzato",
		}
	def give_now(self, now):
		self._given_now = now
		#self.match.queryset = Match.objects.filter(active=True, regenddate__gte=now.date())
		self.fields["match"].queryset = Match.objects.filter(active=True, regenddate__gte=now.date())
	def clean(self):
		cleaned_data = super(ProfileForm, self).clean()
		if ("match" in cleaned_data) and (not cleaned_data["match"].isRegistrable(self._given_now if self._given_now else localnow())) :
			# Forse questo non serve
			del(cleaned_data["match"])
			raise ValidationError("Il match che hai scelto non ha le iscrizioni aperte")
		if ("nome" in cleaned_data) and ("match" in cleaned_data):
			samenameusers = Utente.objects.filter(nome=cleaned_data["nome"], match=cleaned_data["match"])
			samenameusers = samenameusers.exclude(id=self.instance.id if self.instance.id else 0)
			if samenameusers.exists() :
				del cleaned_data["nome"]
				raise ValidationError("Il nome scelto è già impiegato da un altro utente iscritto allo stesso match")
		return cleaned_data

class ProfileMakeView(LupusLoggedTemplateView):
	allowed_level = ["loggato", "iscritto"]
	template_name = "profile_make.html"
	post_tried = False
	def get_context_data(self, **kwargs):
		context = super(ProfileMakeView, self).get_context_data(**kwargs)
		if self.post_tried :
			form = self.form
		else :
			if self.livello == "iscritto" :
				if not self.profile.match.isRegistrable(self.now) :
					messages.error(self.request, "Non puoi più modificare questo profilo")
					return HttpResponseRedirect(reverse("lupus:profile"))
				form = ProfileForm(instance=self.profile)
				form.give_now(self.now)
				form.fields["match"].empty_label=None
				form.fields["match"].queryset=Match.objects.filter(id=self.profile.match_id)
			else :
				form = ProfileForm(instance=Utente(
					nome=self.request.user.get_full_name(),
					match_id=self.request.GET.get("match",None)
				))
		context["form"] = form
		return context
	def post(self, *args, **kwargs):
		f=ProfileForm(self.request.POST, instance=self.profile if self.profile else None)
		f.give_now(self.now)
		if f.is_valid():
			profile = f.save(commit = False)
			if not profile.id :
				profile.ruolo = ""
				profile.active = True
				profile.user = self.request.user
			if self.request.user.utente_set.filter(active=True).exclude(id=profile.id if profile.id else 0).exists() :
				messages.error(self.request, "Disinscriviti agli altri match prima di iscriverti in uno nuovo")
				return HttpResponseRedirect(reverse("lupus:gioco"))
			profile.save()
			return HttpResponseRedirect(reverse("lupus:profile"))
		else :
			self.post_tried = True
			self.form = f
			return self.get(*args, **kwargs)

class GameView(LupusTemplateView):
	template_name = "gioco.html"
	def get_context_data(self, **kwargs):
		context = super(GameView, self).get_context_data(**kwargs)
		matchs = Match.objects.filter(active = True)
		contextmatchs = {"reg":[],"waiting":[],"current":[],"end":[]}
		for match in matchs :
			if match.isRegistrable(self.now) :
				contextmatchs["reg"].append(match)
			elif match.isStarted() :
				contextmatchs["current"].append(match)
			elif match.winner == "" :
				contextmatchs["waiting"].append(match)
			else :
				contextmatchs["end"].append(match)
		context["matchs"] = contextmatchs
		if self.request.user.is_active :
			context['profile'] = self.profile
		return context

class GameAskLeaveView(LupusLoggedTemplateView):
	allowed_level = ["iscritto", "gioco"]
	template_name = "ask_leave.html"
	def get_context_data(self, **kwargs):
		context = super(LupusTemplateView, self).get_context_data(**kwargs)
		context["nome"] = self.match.nome
		return context

class GameLeaveView(LupusLoggedView):
	"""
	Gestisce le richieste provenienti da GameView.
	Permette di abbandonare una partita.
	"""
	allowed_level = ["iscritto", "gioco", "finito"]
	def get(self, *args, **kwargs) :
		confirm = (self.request.GET.get("confirm","") == "true")
		profile = self.profile
		if (not self.match.isRegistrable(self.now)) and (not confirm) and (not (self.livello == "finito")):
			return HttpResponseRedirect(reverse("lupus:gioco_askleave"))
		if self.match.isRegistrable(self.now) or (not self.match.isStarted(self.now) and (not self.profile.ruolo)):
			profile.delete()
		else :
			profile.active = False
			profile.save()
			if (self.livello != "finito") :
				status = Status(utente=profile, flags=STATUS_ID["VECC"])
				status.save()
		return HttpResponseRedirect(reverse("lupus:gioco"))

class StatusView(WithProfileView):
	allowed_level = ["gioco", "finito"]
	template_name = "status.html"
	def get_context_data(self, **kwargs):
		context = super(StatusView, self).get_context_data(**kwargs)
		context['profile'] = self.profile
		if self.profile.stato :
			return context
		status_db = self.profile.status_set.all()
		template_path = STATUS_DATA_DEFAULT["template_path"]
		context["status_template_path"] = template_path
		status_list=[]
		"""
		Per ogni status dell'utente corrente recupero da STATUS_DATA i dati per mostrare le informazioni:
		* il template
		* la funzione che fornisce il contesto da passare con cui includere il template
		* il fatto se può o meno usare un potere attivo
		"""
		for s in status_db:
			d = s.get_dict()
			data = STATUS_DATA[d["ID"]]
			context_data = (data.get("context",STATUS_DATA_DEFAULT["context"]))(s)
			template = template_path + (data.get("info_template",STATUS_DATA_DEFAULT["info_template"]))
			can_use_power = data.get("can_use_power",STATUS_DATA_DEFAULT["can_use_power"])
			active_power_form = can_use_power and can_use_power(s)
			if active_power_form:
				context["script"] = True
				context["giocatori_bersaglio"] = Utente.objects.filter(stato=0, match_id=self.match.id).exclude(id=s.utente_id, ruolo="GM")
				active_power_form = template_path + (data.get("form_template",STATUS_DATA_DEFAULT["form_template"]))
			status_list.append( (s.id, d["Nome"], s.flags, context_data, template, active_power_form ) )
		context["status_list"]=status_list
		return context

class StatusActionView(LupusLoggedView):
	"""
	Gestisce le richieste POST prevenienti da StatusView.
	Dopo aver verificato la correttezza della richiesta recupera la funzione che gestisce l'azione per lo status specifico
	e la esegue passandole la richiesta post
	"""
	allowed_level = ["gioco"]
	def do_action(self):
		if self.profile.stato :
			return "Sei morto !! Non hai status"
		post = self.request.POST
		try:
			status = Status.objects.get(id=post["sID"], flags=post["flags"], utente=self.profile)
		except (KeyError):
			return "Errore nella richiesta POST"
		except (Status.DoesNotExist):
			return "Errore nella richiesta POST"
		else:
			data = STATUS_DATA[status.get_ID()]
		can_use_power = data.get("can_use_power",STATUS_DATA_DEFAULT["can_use_power"]) # TODO - ELIMINARE PASSAGGIO USANDO LAMBDA
		status_do_post = data.get("POST", False)
		if not (can_use_power and can_use_power(status) and status_do_post):
			return "Richiesta priva di senso per questo Status"
		# Eseguo la funzione per la gestione delle richieste post dello status specifico
		error = status_do_post(status, post, now=self.now)
		if error :
			return "Errore nella elaborazione della richiesta:<br>"+error
		messages.info(self.request,"Azione confermata con successo")
		return
	def post(self, *args, **kwargs) :
		error = self.do_action()
		if error :
			messages.error(self.request,error)
		return HttpResponseRedirect(reverse("lupus:status"))

class VotazioniPassateView(WithProfileView):
	#with_game = True
	allowed_level = ["gioco", "finito"]
	template_name = "votazioni_passate.html"
	tipologia = "LINCIAGGIO"
	sottotitolo = "Le passate votazione per il linciaggio giornaliero"
	def get_context_data(self, **kwargs):
		context = super(VotazioniPassateView, self).get_context_data(**kwargs)
		context["sottotitolo"] = self.sottotitolo
		print(self)
		context["votazioni"] = Voto.objects.select_related("risultato__utente", "votante").filter(flags=VOTO_FLAG[self.tipologia], time__lt=self.giorno, votante__match_id=self.match.id).order_by('-time')
		return context

class VotazioniPassateSindacoView(VotazioniPassateView):
	tipologia = "SINDACO"
	sottotitolo = "Le passate votazioni per l'elezione del sindaco del villaggio"

class AbstractGruppoView(WithProfileView):
	allowed_level = ["gioco", "finito"]
	extra_JSscript = WithProfileView.extra_JSscript + ["group_script"]
	script = True
	template_name = "gruppo.html"	
	def get_gruppo(self,**kwargs):
		""" Ritorna il gruppo della vista corrente, di default è il gruppo calcolato da ottieni_livello, deve essere sofrascritta """
		return self.gruppo
	def get_utenti(self,**kwargs):
		""" Ritorna gli utenti, da sovrascrivere """
		return None
	def show_access(self,**kwargs):
		""" Dice se mostrare o meno gli accessi degli utenti del gruppo """
		return False
	def get_context_data(self, **kwargs):
		context = super(AbstractGruppoView, self).get_context_data(**kwargs)
		context['profile'] = self.profile
		gruppo = self.get_gruppo(**kwargs)
		context['gruppo'] = gruppo
		utenti = self.get_utenti(**kwargs)
		context['utenti'] = utenti
		context["user_access"] = {}
		if (len(utenti) > 1) and self.show_access(**kwargs):
			context["user_access"] = set(LogAccesso.objects.filter(date=self.giorno, timeflag=self.nightdayflag).values_list("utente", flat=True))
		context['msg_per_req'] = CHAT_MSG_PER_REQUEST
		context['req_intervall'] = CHAT_INTERVALL_REQUEST
		return context

class VillaggioView(AbstractGruppoView):
	def get_utenti(self,**kwargs):
		return Utente.objects.filter(match_id=self.match.id, ruolo__isnull=False).exclude(ruolo="").order_by('nome')
	def get_gruppo(self,**kwargs):
		if self.profile:
			return self.match.villaggio
	def show_access(self,**kwargs):
		return self.isnowday
	def get_context_data(self, **kwargs):
		context = super(VillaggioView, self).get_context_data(**kwargs)
		context['villaggio'] = 1
		try:
			sindaco = Status.objects.select_related("utente").filter(flags=STATUS_ID["SIND"], utente__match_id=self.match.id).order_by("-opt")[0].utente
		except (IndexError):
			sindaco = Utente(stato=1, nome="[nessuno]")
		context["sindaco"] = sindaco
		#if (today == self.match.startdate) or ((today-timezone.timedelta(days=1) == self.match.startdate) and (time<=GAMETIME_DAYSTART)) :
		if self.isnownight and (diff_giorni(self.giorno,self.match.startdate) <= 1):
			context["is_prima_notte"] = True
			return context
		context["chat_allow_send"] = self.isnowday #and ((not self.profile.stato) or (has_gm_permissions(self.request.user) == "superuser"))
		context["chat_allow_send"] |= (self.profile.user_id == self.match.gm_id)
		#Può chattare solo chi è vivo, a meno che sia il GM o un superuser
		# VOTAZIONI LINCIAGGIO
		context["fatto_linciaggio"] = Voto.objects.filter(flags=1, time=self.today, votante=self.profile).exists() if self.isnowday else True ## Magari self.giorno ??
		context["voto_linciaggio"] = Votazione.objects.select_related().filter(utente__match_id=self.match.id).filter(date=self.today, flags=1, count__gte=0) ## Magari self.giorno ??
		context["last_linciaggio"] = Eventi.objects.select_related("utente").filter(utente__match_id=self.match.id).filter(date__gte=somma_giorni(self.today, -1), flags=2).order_by("-date").first()## Magari self.giorno ??
		if context["fatto_linciaggio"] or self.profile.stato :
			context["esiti_linciaggio"] = Voto.objects.select_related().filter(votante__match_id=self.match.id).filter(flags=1, time=self.today) ## Magari self.giorno ??
		# VOTAZIONI SINDACO
		if sindaco.stato :
			context["fatto_sindaco"] = Voto.objects.filter(flags=2, time=self.today, votante=self.profile).exists() if self.isnowday else True ## Magari self.giorno ??
			context["voto_sindaco"] = Votazione.objects.select_related().filter(utente__match_id=self.match.id).filter(date=self.today, flags=2, count__gte=0) ## Magari self.giorno ??
			if context["fatto_sindaco"] or self.profile.stato :
				context["esiti_sindaco"] = Voto.objects.select_related("risultato__utente", "votante").filter(votante__match_id=self.match.id).filter(flags=2, time=self.today) ## Magari self.giorno ??
		return context

class VillaggioPostVotoView(LupusLoggedView):
	"""
	Gestite la richiesta POST per le votazioni
	Aggiunge un voto al giocatore un ID utente_id per la votazione flag_id.
	"""
	allowed_level = ["gioco"]
	def get(self, *args, **kwargs) :
		if self.profile.stato :
			messages.warning(self.request,"Sei morto, quindi non puoi votare")
			return HttpResponseRedirect(reverse("lupus:villaggio"))
		today = self.today ## Magari self.giorno ??
		if not self.isnowday :
			messages.warning(self.request,"Le votazioni sono già terminate")
			return HttpResponseRedirect(reverse("lupus:villaggio"))
		had_already = Voto.objects.filter(flags=kwargs["flag_id"], votante=self.profile, time=today).exists()
		if had_already :
			messages.warning(self.request,"Oggi hai già votato")
			return HttpResponseRedirect(reverse("lupus:villaggio"))
		try:
			utente = Utente.objects.get(id=kwargs.get("utente_id",0))
		except (Utente.DoesNotExist):
			messages.error(self.request, "Errore durante la votazione. Utente non trovato")
			return HttpResponseRedirect(reverse("lupus:villaggio"))
		if utente.stato :
			messages.warning(self.request,"%s &egrave; morto<br>Non puoi votarlo"%(utente.nome))
			return HttpResponseRedirect(reverse("lupus:villaggio"))
		try:
			voti = Votazione.objects.get(date=today, flags=kwargs.get("flag_id",0), utente_id=utente.id)
		except (Votazione.DoesNotExist) :
			voti = Votazione(date=today, flags=kwargs.get("flag_id",0), utente=utente)
		voti.count += 1
		voti.save()
		voto = Voto(time=today, votante=self.profile, flags=kwargs.get("flag_id",0), risultato=voti)
		voto.save()
		messages.info(self.request,"Hai appena votato %s"%(utente.nome))
		return HttpResponseRedirect(reverse("lupus:villaggio"))

class VillaggioPostFormVotoView(LupusLoggedView):
	"""
	Gestite la richiesta POST per le votazioni
	Aggiunge un voto al giocatore un ID utente_id per la votazione flag_id.
	"""
	allowed_level = ["gioco"]
	def do_action(self):
		if self.profile.stato :
			return "Sei morto, quindi non puoi votare"
		post = self.request.POST
		today = self.today ## Magari self.giorno ??
		if not self.isnowday :
			return "Le votazioni sono già terminate"
		had_already = Voto.objects.filter(flags=post.get("flag_id"), votante=self.profile, time=today).exists()
		if had_already :
			return "Oggi hai già votato"
		try:
			utente = Utente.objects.get(id=post.get("bersaglio",0))
		except (Utente.DoesNotExist):
			return "Errore durante la votazione. Utente non trovato";
		if utente.stato :
			return "%s &egrave; morto<br>Non puoi votarlo"%(utente.nome)
		try:
			voti = Votazione.objects.get(date=today, flags=post.get("flag_id",0), utente_id=utente.id)
		except (Votazione.DoesNotExist) :
			voti = Votazione(date=today, flags=post.get("flag_id",0), utente=utente)
		voti.count += 1
		voti.save()
		voto = Voto(time=today, votante=self.profile, flags=post.get("flag_id",0), risultato=voti)
		voto.save()
		messages.info(self.request,"Hai appena votato %s"%(utente.nome))
		return
	
	def post(self, *args, **kwargs) :
		error = self.do_action()
		if error :
			messages.warning(self.request,error)
		return HttpResponseRedirect(reverse("lupus:villaggio"))

class GruppoView(AbstractGruppoView):
	allowed_level = ["gioco"]
	def show_access(self,**kwargs):
		return True
	def get_utenti(self,**kwargs):
		if self.profile.gruppo :
			return Utente.objects.filter(ruolo__isnull=False, gruppo=self.profile.gruppo)
		else :
			return [self.profile]
	def get_context_data(self, **kwargs):
		context = super(GruppoView, self).get_context_data(**kwargs)
		context["chat_allow_send"] = True #not self.isnowday #and ((not self.profile.stato) or has_gm_permissions(self.request.user) == "superuser")
		context["chat_allow_send"] |= (self.profile.user_id == self.profile.match.gm_id)
		#Può chattare solo chi è vivo, a meno che sia il GM o un superuser
		ruolo = RUOLI[self.profile.ruolo]
		if not ruolo.call("has"):
			context['potere_msg'] = ruolo.call("msg")
			return context
		context["potere_passate"] = ruolo.call("passate", gid=self.gruppo.id, uid=self.profile.id)
		context["potere_passate_template"] = "lupus/roles/past_tpl__"+ruolo.get_power("past_tpl")
		if self.profile.stato :
			context['potere_msg'] = "Sei morto, non puoi agire."
			return context			
		time_msg = ruolo.call("time", now=self.now)
		if time_msg :
			context['potere_msg'] = time_msg
			return context
		hasdone = ruolo.call("hasdone",gid=self.gruppo.id, uid=self.profile.id, giorno=self.giorno)
		if hasdone :
			context["potere_msg"] = "Hai già usato il potere speciale."
			return context
		other = ruolo.call("other",gid=self.gruppo.id, uid=self.profile.id, giorno=self.giorno, match=self.match)
		if not other :
			context['potere_msg'] = ruolo.call("msg")
			return context
		# Posso agire
		context["potere_bersagli"] = ruolo.call("bersagli", gid=self.gruppo.id, uid=self.profile.id, match=self.match, giorno=self.giorno)
		if not context["potere_bersagli"] :
			context['potere_msg'] = "In questo momento non c'è nessun utente su cui tu possa usare il tuo potere"
		context["potere_data"] = ruolo.call("context", gid=self.gruppo.id, uid=self.profile.id, match=self.match.id)
		context["potere_form_template"] = "lupus/roles/form_tpl__"+ruolo.get_power("form_tpl")
		return context

class GruppoAzioneView(LupusLoggedView):
	allowed_level = ["gioco"]
	def do_action (self, *args, **kwargs) :
		if self.profile.stato :
			return "Sei morto, non puoi agire"
		post = self.request.POST
		ruolo = self.profile.get_ruolo()
		if int(post.get("agente",0)):
			try:
				agente = Utente.objects.get(id=int(post["agente"]))
			except (Utente.DoesNotExist):
				return "Errore nella richiesta POST"
			if (agente.gruppo_id != self.profile.gruppo_id) :
				return "Errore, azione non permessa"
			if agente.stato :
				return "%s &egrave; morto, non pu&ograve; agire"%(agente.nome)
		else :
			agente = self.profile
		try :
			bersaglio = Utente.objects.get(id=int(post["bersaglio"]))
		except (Utente.DoesNotExist, IndexError):
			return "Errore nella richiesta POST"
		giorno = self.giorno#self.now.date() if (self.now.time()<GAMETIME_NIGHTSTART) else (self.now+timezone.timedelta(days=1)).date()
		if ruolo.call("time", now=self.now) :
			return "Errore<br>"+ruolo.call("time")
		if ruolo.call("hasdone",gid=self.gruppo.id, uid=self.profile.id, giorno=giorno) :
			return "Hai già usato il potere speciale."
		if not ruolo.call("other",gid=self.gruppo.id, uid=self.profile.id, giorno=giorno, match=self.match) :
			return "Errore:"+ruolo.call("msg")
		if not (bersaglio in ruolo.call("bersagli", uid=self.profile.id, match=self.match, giorno=giorno)) :
			return "Errore, azione su %s non permessa"%(self.profile.nome)
		azione=Azioni(time=giorno,agente=agente, bersaglio=bersaglio)
		ruolo.call("do_action", azione=azione)
		#azione.save()
		return
	def post(self, *args, **kwargs) :
		error = self.do_action()
		if error :
			messages.error(self.request, error)
		else :
			messages.success(self.request, "Azione registrata con successo.")
		return HttpResponseRedirect(reverse("lupus:gruppo"))
	def get(self, *args, **kwargs) :
		messages.error(self.request, "Errore: nessuna richiesta POST")
		return HttpResponseRedirect(reverse("lupus:gruppo"))

@login_required
def gruppo_postmsg(request):
	profile = get_user_profile(request)
	if not profile :
		return HttpResponseRedirect(reverse("lupus:profile_make"))
	try :
		gruppo = Gruppo.objects.all().filter(pk=int(request.POST["gruppo"])).first()
		if (not(request.is_ajax())) or (not gruppo):
			messages.error(request, "Errore durante l'elaborazione della richiesta")
			return HttpResponse("Fail")
		if (gruppo not in [profile.gruppo, profile.match.villaggio]) :
			messages.error(request, "Non puoi postare in questa chat")
			return HttpResponse("Fail")
		msg = request.POST["msg"]
		if msg == "": #Verifico che il messaggio contenga testo
			return HttpResponse("Fail")
		datetime = localnow()
	except (KeyError):
		messages.error(request, "Errore durante l'elaborazione della richiesta")
		return HttpResponse("Fail")
	M = Messaggio(utente=profile, text=msg, gruppo=gruppo, time=localnow())
	M.save()
	return HttpResponse("Ok")

@login_required
def gruppo_getmsg(request, isvillaggio, first_id, last_id):
	"""
	Ritorna un insieme di al più CHAT_MSG_PER_REQUEST messaggi di chat per il gruppo del utente corrente.
	Se first_id != 0 allora ritorna solo i messagi con ID maggiore di first_id
	Se last_id != 0 allora ritorna solo i messagi con ID minore di last_id
	"""
	profile = None
	if has_gm_permissions(request.user) :
		id_profile = request.session.get("lupus_gm_asprofile",False)
		if id_profile:
			try:
				profile = Utente.objects.get(pk=id_profile)
			except (Utente.DoesNotExist) :
				request.session.pop("lupus_gm_asprofile")
	if not profile :
		profile = get_user_profile(request)
	if not profile and not profile.match :
		HttpResponse("Fail")
	gruppo = profile.match.villaggio if int(isvillaggio) else profile.gruppo
	messages = Messaggio.objects.all().filter(gruppo=gruppo)
	if int(first_id):
		messages = messages.filter(id__gt=first_id)
	if int(last_id):
		messages = messages.filter(id__lt=last_id)
	messages = messages.order_by("-time")[:CHAT_MSG_PER_REQUEST] #Ordino e limito i messaggi ritornati
	res = []
	for m in messages: #Modifico l'oggetto
		res.append({
		"id":m.id,
		"time":m.time.isoformat(),
		"text":m.text,
		"utente":m.utente.nome
		})
	data = json.dumps(res)
	return HttpResponse(data, content_type = "application/json")

class SummaryView(LupusTemplateView):
	template_name = "summary.html"
	summary_match = None
	def get_context_data(self, **kwargs):
		context = super(SummaryView, self).get_context_data(**kwargs)
		context['ruolo_img_format'] = RUOLI_IMG_FORMAT
		if not self.summary_match :
			self.summary_match = get_object_or_404(Match, pk=self.kwargs.get("match_id",0))
		winner = self.summary_match.winner
		if not winner :
			raise Http404
		giocatori = Utente.objects.filter(match=self.summary_match).values("nome", "ruolo", "stato")
		vivi = 0
		vincitori = {}
		altri = {}
		for g in giocatori :
			ruolo = RUOLI[g["ruolo"]]
			if ruolo.fazione == winner :
				vincitori[ruolo.id] = [g] + vincitori.get(ruolo.id,[])
			else :
				altri[ruolo.id] = [g] + altri.get(ruolo.id,[])
			if not g["stato"] : vivi += 1
		status = {}
		for s in Status.objects.select_related("utente").filter(utente__match_id = self.summary_match.id).values("utente__nome", "flags", "opt") :
			status[s["flags"]] = [s] + status.get(s["flags"],[])
		for s in status :
			data = STATUS_DATA[STATUS_FLAG[s]["ID"]]
			status[s] = {"list":status[s], "grouper": STATUS_FLAG[s]["Nome"], "template": STATUS_DATA_DEFAULT["template_path"] + data.get("summ_template",STATUS_DATA_DEFAULT["summ_template"])}
		context.update({"vincitori":vincitori, "altri":altri, "vivi":vivi, "totale":len(giocatori), "match":self.summary_match, "status":status})
		context["RUOLO_NOME"] = { r.id:r.nome for r in RUOLI.values()}
		context["winner"] = {"nome":FAZIONI[winner], "msg":FAZIONI_MSG[winner]}
		return context
	
class SummaryCurrentView(SummaryView):
	allowed_level = ["finito"]
	def get(self, *args, **kwargs):
		self.summary_match = self.match
		return super(SummaryCurrentView, self).get(*args, **kwargs)

class SummaryFullView(SummaryView):
	template_name = "summary_full.html"
	def get_context_data(self, **kwargs):
		context = super(SummaryFullView, self).get_context_data(**kwargs)
		match = self.summary_match
		context["start_date"] = match.startdate
		context["end_date"] = match.enddate
		eventi = events_parser(Eventi.objects.all().filter(utente__match_id=match.id), True)
		for e in eventi:
			if e.nice != 20:
				e.momento = "notte" if e.nice >= 21 else "giorno"
		context["eventi"] = eventi
		context["voti_linciaggio"] = Votazione.objects.all().prefetch_related().filter(flags=VOTO_FLAG["LINCIAGGIO"], count__gt=0, utente__match=match).order_by("-count")
		context["voti_sindaco"] = Votazione.objects.all().filter(flags=VOTO_FLAG["SINDACO"], count__gt=0, utente__match=match).order_by("-count")
		azioni = Azioni.objects.all().filter(agente__match=match).filter(agente__ruolo__in=lupus.ruoli.RUOLI_NOTTURNI)
		for a in azioni:
			a.agente.ruolo = a.agente.get_ruolo()
			a.bersaglio.ruolo = a.bersaglio.get_ruolo()
		context["azioni"] = azioni
		#print azioni
		note = Nota.objects.filter(utente1__match_id=match.id).values("utente1__nome", "utente2__nome", "opt", "time", "text", "flags").order_by("-time","-flags")
		context["note"] = {}
		for nota in note:
			data = NOTE[nota["flags"]]
			if data["show"]:
				for x in nota.keys():
					nota[x] = data["show_filter"].get(x,(lambda x: x))(nota[x])
				testo = data["show_format"]%(nota)
				#context["note"].append( {"testo":testo, "show":data["show"], "date", nota["time"]} )
				context["note"] [nota["time"]] = context["note"].get(nota["time"],{})
				context["note"] [nota["time"]] [data["show"]] = context["note"] [nota["time"]].get(data["show"],[]) + [testo] 
		#context["giorni"] = [match.startdate+timedelta(days=i+1) for i in xrange((match.enddate-match.startdate).days)]
		context["giorni"] = [somma_giorni(match.startdate, i+1) for i in xrange(diff_giorni(match.enddate, match.startdate))]
		return context

class notImplemented(LupusTemplateView):
	template_name = "notImplemented.html"

def esegui_giorno_all_view(request, *args, **kwargs):
	r = esegui_giorno_all()
	response = "<br>\n".join(["<< Elaborazione automatica giorno >>"]+["%(id)d: %(nome)s  => %(text)s "%x for x in r]) + "\n"
	return HttpResponse(response)

def esegui_notte_all_view(request, *args, **kwargs):
	raise Http404
	return HttpResponse("")

def esegui_vittoria_all_view(request, *args, **kwargs):
	r = esegui_vittoria_all()
	for x in r :
		if x["winner"] == " ":
			x["winner"] = "X"
	response = "<br>\n".join(["<< Controllo automatico vittoria partite >>"]+["%(id)d: %(nome)s  => %(winner)s"%x for x in r]) + "\n"
	return HttpResponse(response)
