# -*- encoding: utf8 -*-
from lupus.setting import STATUS_FLAG, STATUS_ID, EVENTI_FLAG
from lupus.models import Status, Utente
from lupus.time import localnow, isnowday

from random import randint

def statusContext_default(status):
	doc = status.get_dict()["DOC"]
	return {"doc":doc}

def statusContext_CUP_ROC(status):
	context = statusContext_default(status)
	context["opt"]=status.opt
	if (status.opt):
		context["users"]=[x.utente for x in Status.objects.filter(flags=STATUS_ID["INN"], opt=status.utente_id).exclude(utente_id=status.utente_id)]
	return context

def statusContext_INN(status):
	context = statusContext_default(status)
	innamoramenti = Status.objects.filter(opt=status.opt, flags=status.flags)
	context["innamorati"] = [ x.utente for x in innamoramenti]
	return context

def statusContext_ADE(status):
	context = statusContext_default(status)
	print(status.opt)
	context["espansivo"] = Utente.objects.filter(id=status.opt).first()
	return context

def statusPOST_CUP(status, post, **kwargs) :
	if (isnowday(kwargs.get("now", localnow()))) :
		return "Non ti &egrave; consentivo agire durante il giorno"
	if (status.opt) :
		return "Hai gia' agito"
	try :
		u1ID = int(post["user1_ID"])
		u2ID = int(post["user2_ID"])
		if (u1ID<=0) or (u2ID<=0) :
			raise(ValueError)
		cupID = status.utente_id
		if (cupID in (u1ID, u2ID)) :
			return "Utente bersaglio non permesso"
		user1 = Utente.objects.get(id=u1ID)
		user2 = Utente.objects.get(id=u2ID)
	except (ValueError) :
		return "Informazioni utenti bersaglio malformate"
	except (KeyError) :
		return "Informazioni malformate"
	except (Utente.DoesNotExist) :
		return "Informazioni utenti bersaglio errate"
	if (user1.stato or user2.stato) or (user1.match_id != status.utente.match_id) or (user2.match_id != status.utente.match_id):
		return "Utente bersaglio non permesso"
	if (user1.ruolo == "GM") or (user2.ruolo == "GM"):
		return "Il gamemaster non &egrave; permesso"
	s1=Status(utente=user1, flags=STATUS_ID["INN"], opt=cupID)
	s2=Status(utente=user2, flags=STATUS_ID["INN"], opt=cupID)
	s1.save();
	s2.save();
	status.opt=1;
	status.save();
	return ""

def statusPOST_ROC(status, post, **kwargs) :
	error = statusPOST_CUP(status,post, **kwargs)
	if not error :
		s=Status(utente=status.utente, flags=STATUS_ID["INN"], opt=status.utente_id)
		s.save()
		# Status.objects.create(utente=status.utente, flags=STATUS_ID["INN"], opt=status.utente_id)
	return error
	
def statusFilter_INN(status, esito) :
	uid = status.utente_id
	if uid in esito["Morti"] :
		innamorati = Status.objects.filter(flags=status.flags, opt=status.opt).exclude(utente_id=uid)
		new_esito={"Morti" : [inn.utente_id for inn in innamorati if not (inn.utente_id in esito["Morti"])]}
		new_esito["Eventi"] = [{"flags": EVENTI_FLAG["Suic"],"utente": uid} for uid in new_esito["Morti"]]
		return new_esito
	#...
	return {}

def statusFilter_Day_INN(status, esito) :
	uid = status.utente_id
	if uid in esito["Morti"] :
		innamorati = Status.objects.filter(flags=status.flags, opt=status.opt).exclude(utente_id=uid)
		new_esito={"Morti" : [inn.utente_id for inn in innamorati if not (inn.utente_id in esito["Morti"])]}
		new_esito["Eventi"] = [{"flags": EVENTI_FLAG["SuicD"],"utente": uid} for uid in new_esito["Morti"]]
		return new_esito
	#...
	return {}

def statusFilter_VECC(status, esito) :
	night = esito["nightID"]-1
	new_esito = {}
	if (randint(1,100)<=(5+2*night)) :
		new_esito["Morti"] = [status.utente_id]
		new_esito["Eventi"] = [{"flags": EVENTI_FLAG["MNott"],"utente": status.utente_id}]
	#...
	return new_esito

def statusCanUsePower_OPT(status) :
	return (status.opt == 0)

STATUS_DATA_DEFAULT = {
	"post":False,
	"can_use_power" : False,
	"context" : statusContext_default,
	"template_path" : "lupus/status/",
	"info_template" : "default.html",
	"form_template" : "",
	"summ_template" : "default_summary.html",
	"altera_notte"  : False,
	"filtro_notte"  : False,#Cambiare con lambda
	"altera_giorno" : False,
	"filtro_giorno" : False,#Cambiare con lambda
}

STATUS_DATA = {
	"SIND":{
		#"context":statusContext_default,
		"summ_template" : "SIND_summary.html",
	},
	"VECC":{
		"altera_notte"  : True,
		"filtro_notte"  : statusFilter_VECC
	},
	"INN" :{
		"context"      : statusContext_INN,
		"info_template":"INN.html",
		"altera_notte" : True,
		"filtro_notte" : statusFilter_INN,
		"altera_giorno" : True,
		"filtro_giorno" : statusFilter_Day_INN,
		"summ_template" : "INN_summary.html",
	},
	"CUP" :{
		"POST"         : statusPOST_CUP,
		"can_use_power": statusCanUsePower_OPT,
		"context"      : statusContext_CUP_ROC,
		"form_template": "CUP_form.html",
		"info_template": "CUP_ROC.html",
	},
	"ROC" :{
		"POST"         : statusPOST_ROC,
		"can_use_power": statusCanUsePower_OPT,
		"context"      : statusContext_CUP_ROC,
		"form_template": "ROC_form.html",
		"info_template": "CUP_ROC.html",
	},
	"ADE" :{
		"context"      : statusContext_ADE,
		"info_template":"ADE.html",
	},
}
