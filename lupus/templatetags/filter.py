from django import template
register = template.Library()

@register.filter
def dictget(dictionary, key):
	""" Filtro che nei template permette di recuperare il valore data una variabile contenente la chiave """
	return dictionary.get(key)

@register.filter
def dictgetint(dictionary, key):
	""" Filtro che nei template permette di recuperare il valore data una variabile contenente la chiave """
	return dictionary.get(int(key))

@register.filter
def string_format(string, dictionary):
	""" Filtro che nei template permette di formattare una stringa """
	return string%dictionary

@register.filter
def gruopedget(list, key):
	""" filtro che nei template permette di recuperare il valore data il grouper da una lista esito di un regroup """
	for i in list:
		if i.grouper == key:
			return i.list
