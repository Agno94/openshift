# -*- encoding: utf8 -*-
from django.shortcuts import get_object_or_404
from django.utils.decorators import method_decorator

from lupus.time import localnow, giornodigioco, diff_giorni, somma_giorni
from datetime import datetime, date, timedelta, time

import json
import random
from django.contrib import messages
from django.contrib.auth.models import User

from lupus.models import *
from lupus.setting import *

from lupus.eventi import EVENTI_PARSER
from lupus.status import STATUS_DATA


def get_user_profile(request):
	"""
	Ritorna il profilo utente attivo.
	"""
	# In futuro magari la modificheremo. Penso a quando sarà da gestire più match
	if request.user.is_active :
		try :
			return request.user.utente_set.select_related().get(active=True)
		except (Utente.MultipleObjectsReturned) :
			print("Error : Utente.MultipleObjectsReturned per %s controlla !"%(str(request.user)))
			return request.user.utente_set.select_related().filter(active=True).order_by("-id").first()
		except (Utente.DoesNotExist) :
			pass
	return Utente(nome="", ruolo="")

def has_gm_permissions(user) :
	if user.is_superuser:
		return "superuser"
	elif user.is_staff:
		return "staff"
	return False

def get_user_per_match(id):
	"""
	Funzione che recupera gli user per il match con ID "id"
	"""
	u = Utente.objects.all().select_related("user").filter(match__id=id)
	u_res = []
	for us in u:
		u_res.append({"nome": us.nome, "id": us.id, "ruolo": us.ruolo, "gruppo": us.gruppo.id if us.gruppo else "", "nome_full": us.user.get_full_name()})
	return u_res

def events_parser(event_list, compatto=False) :
	parsers = dict()
	parser_list = []
	for event in event_list :
		if not (event.flags in parsers) :
			parser = EVENTI_PARSER[event.flags](compatto)
			for flag in parser.flags :
				parsers[flag] = parser
			parser_list.append(parser)
		parsers[event.flags].append(event)
	results = []
	for parser in parser_list :
		results.extend(parser())
	results.sort(reverse=True)
	return results

def empty_calcolo():
	return {"Morti":[], "Risorti":[], "Eventi":[], "Note":[], "Status":[]}

def applica_calcolo(in_azioni, giorno):
	"""
	Aggiorna il database con i risultati di calcola_giorno e calcola_notte
	"""
	azioni = empty_calcolo()
	azioni.update(in_azioni)
	error = ""
	Utente.objects.filter(id__in = azioni["Morti"]  ) .update(stato = 1)
	Utente.objects.filter(id__in = azioni["Risorti"] ).update(stato = 0)
	for x in azioni["Eventi"]:
		try:
			x["utente_id"] = x["utente"]
			del x["utente"]
			x["date"] = giorno
			x["random_scene"] = True
			ev = Eventi(**x)
			ev.save()
		except:
			error += "Errore salvataggio eventi ! "
	for x in azioni["Note"]:
		try:
			x["utente1_id"] = x["utente1"]
			del x["utente1"]
			if ("utente2" in x) and type(x["utente2"])==type(0) :
				x["utente2_id"] = x["utente2"]
				del x["utente2"]
			x["time"] = giorno
			nota = Nota(**x)
			nota.save()
		except:
			error += "Errore salvataggio note ! "
	for x in azioni["Status"]:
		try:
			x["utente_id"] = x["utente"]
			del x["utente"]
			st = Status(**x)
			st.save()
		except:
			error += "Errore salvataggio status ! "
	return error


def get_azioni(match, data):
	"""
	Ritorna quello che è accaduto nella notte percedente alla data fornita
	"""

	return Azioni.objects.all().filter(agente__match=match, time=data)

def calcola_notte(match, data):
	"""
	Funzione che elabora quello che è avvenuto nella notte e propone delle liste di:
	* morti/risorti/suicidati
	* eventi
	* nuovi status
	* note
	* ecc
	da sottoporre per l'approvazione/modifica del GM
	"""
	#azioni = get_azioni(match, data)
	night_id = diff_giorni(data, match.startdate)
	ret = empty_calcolo()
	ret["nightID"] = night_id
	#Calcolo degli status
	for s in Status.objects.all().filter(utente__match_id=match.id, utente__stato=0).order_by("flags"):
			data = STATUS_DATA[s.get_ID()]
			if data.get("altera_notte", False):
				filter = data.get("filtro_notte", False)
				if (filter): #Solo per sicurezza, forse non serve
					new = filter(s, ret)
					if (new):
						ret["Morti"] += new["Morti"]
						ret["Eventi"] += new["Eventi"]
	return ret #{"Eventi":[], "Note":[], "Morti":[], "Risorti":[], "Status":[], "nightID": night_id} 	# -Morti e risortli liste di id
																										#-Eventi: {"utente":xxx, "flags":xxx}
																										#-Note: {"utente1":xxx, ...} omettendo quelli non necessari
																										#-Status: {"utente":xxx, "flags":xxx, "opt":xxx}

def somebody_won(match):
	"""
	Funzione che controlla se una fazione ha vinto e ritorna:
	"" se nessuno ha vinto
	" " se sono morti tutti (gioco finito senza vincitori)
	"L", "C", "P" se hanno vinto rispettivamente i lupi, il criceto e i popolani
	"""
	if match.winner: #Se la partita è già conclusa
		return match.winner
	# if not match.utente_set.filter(stato=0).exists():
	if not Utente.objects.all().filter(match=match, stato=0).exists(): #Se nessuno è vivo
		return " "
	winner = ""
	n_lupi = Utente.objects.all().filter(match=match, stato=0, ruolo="LUP").count()
	if not n_lupi: #Se tutti i lupi sono morti
		winner ="P"
	else:
		ruoli_pop = [k for k in RUOLI if RUOLI[k].fazione == "P"]
		if n_lupi > Utente.objects.all().filter(match=match, stato=0, ruolo__in=ruoli_pop).count(): #Se lupi almeno uno in più dei popolani
			winner = "L"
	if winner and Utente.objects.all().filter(match=match, stato=0, ruolo="CRI").exists(): #Se una fazione ha vinto e il criceto è vivo
		winner = "C"
	return winner

def get_voti(match, data, flags):
	"""
	Funzione che ritorna i voti ricevuti dagli utenti per la votazione indicata da flags ordinati
	"""
	votes = []
	count = 0
	for v in Votazione.objects.all().filter(date=data, flags=flags, count__gt=0, utente__match=match).order_by("-count"):
		if not v.utente.stato:
			vot = {"utente": v.utente, "count": v.count}
			votes.append(vot)
		count += v.count
	return {"voti":votes, "count":count}

def calcola_giorno(match, data):
	"""
	Funzione che elabora le votazione avvenute il giorno e restituisce le lista di morti, eventi e eventuale nuovo sindaco
	"""
	day_id = diff_giorni(data, match.startdate)
	ret = empty_calcolo()
	ret["dayID"] = day_id
	vivi = Utente.objects.all().filter(match_id=match.id, stato=0).count()
	if vivi == 0: #Errore
		return ret
	n_accessi = LogAccesso.objects.all().filter(timeflag=2, date=data, utente__match_id=match.id, utente__stato=0).count() #Numero dei giocatori vivi che hanno effettuato l'accesso al sito
	if  not n_accessi >= vivi//2: #Se almeno metà dei vivi non sono attivi la votazione non è valida
		return ret
	###SINDACO###
	sindaco_vivo=False
	sindaco_eletto = None
	try:
		sindaco = Status.objects.filter(flags=STATUS_ID["SIND"], utente__match=match).order_by("-opt")[0].utente
		sindaco_vivo = not bool(sindaco.stato)
	except (IndexError):
		pass
	if not sindaco_vivo: #Se il sindaco è morto eseguo elezione
		voti = get_voti(match, data, VOTO_FLAG["SINDACO"])
		votes = voti["voti"]
		if len(votes) != 0:
			max_count = votes[0]["count"]
			candidates = [votes[0]["utente"]]
			i = 1
			while i < len(votes) and votes[i]["count"] == max_count:
				candidates.append(votes[i]["utente"])
				i += 1
			if len(candidates) > 1:
				random.shuffle(candidates)
			ret["Status"].append({"utente":candidates[0].id, "flags":STATUS_ID["SIND"], "opt":diff_giorni(data, match.startdate)})
			ret["Eventi"].append({"utente":candidates[0].id, "flags":EVENTI_FLAG["Sind"]})
			sindaco_eletto = candidates[0]
	###LINCIAGGIO###
	voti = get_voti(match, data, VOTO_FLAG["LINCIAGGIO"])
	votes = voti["voti"]
	if len(votes) != 0:
		max_count = votes[0]["count"]
		candidates = [votes[0]["utente"]]
		i = 1
		while i < len(votes) and votes[i]["count"] == max_count:
			candidates.append(votes[i]["utente"])
			i += 1
		#Se tra i candidati al linciaggio c'è quello votato dal sindaco
		try:
			sindaco = Status.objects.filter(flags=STATUS_ID["SIND"]).order_by("-opt")[0].utente
			if bool(sindaco.stato) and sindaco_eletto: #Se il sindaco è morto e ne è stato eletto un'altro
				sindaco = sindaco_eletto
			if (not bool(sindaco.stato)) and Voto.objects.all().filter(votante=sindaco, flags=VOTO_FLAG["LINCIAGGIO"], time=data).exists():
				votato = Voto.objects.all().filter(votante=sindaco, flags=VOTO_FLAG["LINCIAGGIO"], time=data).first().risultato.utente
				if votato in candidates:
					candidates = [votato]
		except (IndexError):
			pass
		if len(candidates) > 1:
			random.shuffle(candidates)
		ret["Morti"].append(candidates[0].id)
		ret["Eventi"].append({"utente":candidates[0].id, "flags":EVENTI_FLAG["MLinc"]})
		#Per ogni status verifico se devo alterare l'esito
		for s in Status.objects.all().filter(utente__id=candidates[0].id):
			data = STATUS_DATA[s.get_ID()]
			if data.get("altera_giorno", False):
				filter = data.get("filtro_giorno", False)
				if (filter): #Solo per sicurezza, forse non serve
					new = filter(s, ret)
					if (new):
						ret["Morti"] += new["Morti"]
						ret["Eventi"] += new["Eventi"]
	return ret

def esegui_giorno(match):
	now = localnow()
	giorno = giornodigioco(now)
	# print now, giorno
	if (now.time() > GAMETIME_NIGHTSTART) :
		giorno = somma_giorni(giorno, -1)
	if (now.time() < GAMETIME_DAYEND) or (now.date() < giorno):
		return "! (Non Terminato)"
	if LogGestione.objects.filter(date=giorno, flag=1, match_id=match.id).exists():
		return "! (Gia' elaborato)"
	res = calcola_giorno(match, giorno)
	#print str(res)
	applica_calcolo(res, giorno)
	log = LogGestione(azioni=repr(res), match=match, date=giorno, flag=1, automatico=True)
	log.save()
	return "OK"

def esegui_giorno_all():
	risultato = []
	now = localnow()
	giorno = giornodigioco(now)
	risultato = [{"id":0,"nome":"DEBUG","text":"%s %s"%(str(now),str(giorno))}]
	for m in Match.objects.all().filter(winner="", active=True, startdate__lt=localnow().date()):
		risultato.append({"id":m.id, "nome":m.nome, "text": esegui_giorno(m)})
	return risultato

def esegui_vittoria_all():
	risultato = []
	today = localnow().date()
	for m in Match.objects.filter(winner="", active=True, startdate__lt=today):
		winner = somebody_won(m)
		if winner :
			m.winner = winner
			m.enddate = today
			m.save()
		risultato.append({"id":m.id, "nome":m.nome, "winner": m.winner})
	return risultato
