# -*- encoding: utf8 -*-
from django.utils import timezone
from lupus.setting import GAMETIME_DAYSTART, GAMETIME_DAYEND, GAMETIME_NIGHTEND, GAMETIME_NIGHTSTART, GAMETIME_WEEKENDISNIGTH

def localnow() :
	return timezone.localtime(timezone.now(), timezone.get_default_timezone())

def isgameday(date = localnow().date(), isWEnight = GAMETIME_WEEKENDISNIGTH):
	return (date.isoweekday() <= 5) or (not isWEnight)

def isnowday(now = localnow(), isWEnight = GAMETIME_WEEKENDISNIGTH):
	return (now.time() >= GAMETIME_DAYSTART) and (now.time() <= GAMETIME_DAYEND) and not (isWEnight and (now.isoweekday() > 5))

def isnownight(now = localnow(), isWEnight = GAMETIME_WEEKENDISNIGTH):
	return (now.time() >= GAMETIME_NIGHTSTART) or (now.time() <= GAMETIME_NIGHTEND) or (isWEnight and (now.isoweekday() > 5))

def giornodigioco(now = localnow(), NS = GAMETIME_NIGHTSTART, isWEnight=GAMETIME_WEEKENDISNIGTH) :
	today = now.date()
	giorno = today if (now.time()<NS) else today+timezone.timedelta(days=1)
	if (giorno.isoweekday() > 5) and (isWEnight) :
		return giorno + timezone.timedelta(days=(8-giorno.isoweekday()))
	return giorno

def diff_giorni(today, start, isWEnight = GAMETIME_WEEKENDISNIGTH):
	real = (today-start).days
	if not isWEnight:
		return real
	gap = real - (real // 7)*2
	if today.isoweekday() < start.isoweekday():
		gap += -2 + (start.isoweekday() // 5)*(start.isoweekday() % 5) - (today.isoweekday() // 5)*(today.isoweekday() % 5)
	return gap

def somma_giorni(giorno, intervallo, isWEnight = GAMETIME_WEEKENDISNIGTH):
	if isWEnight :
		intervallo += (intervallo // 5)*2
		if giorno.isoweekday() > 5:
			giorno = giorno - timezone.timedelta(days=(giorno.isoweekday() % 5))
		intervallo += 2 if (giorno.isoweekday() + intervallo % 7) > 5 else 0
	return giorno + timezone.timedelta(days=intervallo)


class GameTimeMixin(object):
	def init_time(self, gametime=(GAMETIME_DAYSTART, GAMETIME_DAYEND, GAMETIME_NIGHTSTART, GAMETIME_NIGHTEND), isWEnight=GAMETIME_WEEKENDISNIGTH) :
		self.gametime = {"DS":gametime[0], "DE":gametime[1], "NS":gametime[2], "NE":gametime[3]}
		self.now = localnow()
		self.time = self.now.time()
		self.today = self.now.date()
		self.isnowday = (self.time >= self.gametime["DS"]) and (self.time <= self.gametime["DE"])
		self.isnowday &= (not isWEnight or (self.today.isoweekday() <= 5)) # Controllo WE-aware
		self.isnownight = (self.time >= self.gametime["NS"]) or (self.time <= self.gametime["NE"])
		self.isnownight |= (isWEnight and (self.today.isoweekday() > 5)) # Controllo WE-aware
		self.nightdayflag = (1 if self.isnownight else (2 if self.isnowday else 0))
		self.giorno = self.today if (self.time<self.gametime["NS"]) else self.today+timezone.timedelta(days=1)
		if (self.giorno.isoweekday() > 5) and (isWEnight):
			self.giorno = self.giorno + timezone.timedelta(days=(8-self.giorno.isoweekday()))
		return
