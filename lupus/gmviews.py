# -*- encoding: utf8 -*-
from django.shortcuts import get_object_or_404
from django.http import HttpResponse, HttpResponseRedirect, Http404
from django.urls import reverse, reverse_lazy
from django.utils import timezone
from django import forms

from django.contrib.auth.decorators import login_required, user_passes_test
from django.utils.decorators import method_decorator

from django.contrib import messages

from django.db.models import Q

import json
import random
import glob
import os
from datetime import datetime, date, timedelta, time

from lupus.time import localnow as now, isnowday, isnownight, giornodigioco

from lupus.views import LupusTemplateView, LupusLoggedTemplateView, LupusLoggedView
from lupus.models import *
from lupus.setting import *
from lupus.status import STATUS_DATA_DEFAULT, STATUS_DATA
from lupus.engine import *
from django.template.context_processors import request
#from django.core.context_processors import request

class LupusGMTemplateView(LupusLoggedTemplateView):
	template_subpath = LupusLoggedTemplateView.template_subpath+"gm_"
	gm_required = True

class WithMatchView(LupusGMTemplateView):
	script = True
	def get_context_data(self, **kwargs):
		context = super(WithMatchView, self).get_context_data(**kwargs)
		matchs = Match.objects.filter(active = True, regenddate__lt = self.today, winner = "")
		if self.gm_perms == "staff":
			matchs = matchs.filter(gm=self.request.user)
		if (self.livello == "loggato") and (matchs.filter(winner="").exists()) :
			messages.info(request,"Il GM deve iscriversi al match che amministra")
		context['matchs'] = matchs
		if "match_id" in kwargs and kwargs["match_id"] and kwargs["match_id"] != "0": #Se fornisco l'id e non è nullo seleziono quel match, altrimenti il primo se c'è.
			m = Match.objects.filter(id = kwargs["match_id"], active = True, regenddate__lt = self.today)
			if self.gm_perms == "staff":
				m = m.filter(gm=self.request.user)
			context["match"] = m.first()
		else:
			try:
				context["match"] = context['matchs'][0]
			except IndexError:
				messages.error(self.request, "Nessun match valido.")
				context["match"] = None
				return context
		if not context["match"]:
			messages.error(self.request, "Match non valido.")
			return context
		context["users"] = get_user_per_match(context["match"].id)
		return context

	@method_decorator(login_required)
	def get(self, request, *args, **kwargs):
		return super(WithMatchView, self).get(request, *args, **kwargs)

	@method_decorator(login_required)
	def post(self, request, *args, **kwargs):
		if request.user.is_superuser:
			if "smatch" in request.POST:
				return HttpResponseRedirect(reverse(kwargs["redirect_url"], kwargs={"match_id" : request.POST.get("smatch")}))
			else:
				raise Http404
		raise Http404

class GMRolesView(WithMatchView):
	template_name = "ruoli.html"

	def get_context_data(self, **kwargs):
		context = super(GMRolesView, self).get_context_data(**kwargs)
		context["ruoli"] = RUOLI.values()
		context["ruoli2"] = [x for x in RUOLI.values() if x.id!="CON" and x.id!="" and x.id!="GM"]
		g = Gruppo.objects.all()
		if self.gm_perms == "staff":
			g = g.filter(match__gm=self.request.user)
		context["gruppi"] = g
		return context

	def post(self, request, *args, **kwargs):
		kwargs["redirect_url"] = "lupus:gm_assegnazione_ruoli"
		return super(GMRolesView, self).post(request, *args, **kwargs)

@login_required
@user_passes_test(lambda u: has_gm_permissions(u))
def gm_user_match(request):
	if request.is_ajax():
		m_id = request.POST.get("match")
		if has_gm_permissions(request.user) == "staff" and not Match.objects.all().filter(id=m_id, gm=request.user).exists():
			raise Http404
		u_res = get_user_per_match(m_id)
		data = json.dumps(u_res)
		return HttpResponse(data, content_type = "application/json")
	raise Http404()

@login_required
@user_passes_test(lambda u: has_gm_permissions(u))
def gm_set_role(request):
	if request.is_ajax():
		u_id = request.POST.get("user")
		u = get_object_or_404(Utente, id=u_id)
		if has_gm_permissions(request.user) == "staff" and not Match.objects.all().filter(id=u.match_id, gm=request.user).exists():
			raise Http404
		r_id = request.POST.get("role")
		is_role = request.POST.get("is-role").lower() == "true"
		if is_role:
			if r_id in RUOLI :
				r = RUOLI[r_id]
			else :
				raise Http404
			#u.ruolo = get_object_or_404(Ruolo, id=r_id)
			u.ruolo = r_id
		else:
			u.gruppo = get_object_or_404(Gruppo, id=r_id) if r_id != "" else None
		u.save()
		return HttpResponse("Ok")
	raise Http404()

@login_required
@user_passes_test(lambda u: has_gm_permissions(u))
def gm_set_users_role(request):
	m_id = request.POST.get("matchid")
	if has_gm_permissions(request.user) == "staff" and not Match.objects.all().filter(id=m_id, gm=request.user).exists():
		raise Http404
	for k in request.POST:
		if k.startswith("ruolo_"):
			u_id = k[6:]
			u = get_object_or_404(Utente, id=u_id)
			if u.match_id != m_id:
				raise Http404
			r_id = request.POST.get(k)
			if r in RUOLI :
				r = RUOLI[r_id]
			else :
				raise Http404
			u.ruolo = r_id
			u.save()
		elif k.startswith("gruppo_"):
			u_id = k[7:]
			u = get_object_or_404(Utente, id=u_id)
			if u.match_id != m_id:
				raise Http404
			r_id = request.POST.get(k)
			u.gruppo = get_object_or_404(Gruppo, id=r_id) if r_id != "" else None
			u.save()
	messages.success(request, "Salvataggio ruoli eseguito correttamente")
	return HttpResponseRedirect(reverse("lupus:gm_assegnazione_ruoli", kwargs={"match_id": m_id}))

@login_required
@user_passes_test(lambda u: has_gm_permissions(u))
def gm_set_role_random(request):
	m_id = request.POST.get("matchid")
	if has_gm_permissions(request.user) == "staff" and not Match.objects.all().filter(id=m_id, gm=request.user).exists():
		raise Http404
	m = get_object_or_404(Match, pk=m_id)
	#Cerco il GM
	if m.gm:
		gm = Utente.objects.all().filter(user=m.gm, active=True, match=m).first()
	else: #Se m.gm è None il match è gestibile solo da un superuser
		gm = Utente.objects.all().filter(user=request.user, active=True, match=m).first()
	#Se il GM c'è gli assegno il ruolo
	u = Utente.objects.all().filter(match__id=m_id)
	if gm:
		gm.ruolo = "GM"
		gm.save()
		u = u.exclude(user=gm.user) #Tolgo il GM dall'assegnazione dei ruoli
	else:
		messages.warning(request, "Impossibile assegnare automaticamente il GM. Assegnarlo manualmente.")
	lista_ruoli = []
	conteggio = 0
	for k in request.POST:
		if k.startswith("ruolo_"):
			r_id = k[6:]
			if r_id in RUOLI :
				ruolo = RUOLI[r_id]
			else :
				raise Http404
			n = request.POST.get(k)
			lista_ruoli.append((r_id, n))
			conteggio += int(n)
	if conteggio > u.count():
		messages.error(request, "Errore: asseganti piu' ruoli degli utenti registrati")
		return HttpResponseRedirect(reverse("lupus:gm_assegnazione_ruoli", kwargs={"match_id": request.POST.get("matchid")}))
	u2 = list(u)
	for r, n in lista_ruoli:
		for i in range(int(n)):
			us = random.choice(u2)
			us.ruolo = r
			us.save()
			u2.remove(us)
	#r = Ruolo.objects.filter(name="Contadino").first()
	for us in u2:
		us.ruolo = "CON"
		us.save()
	messages.success(request, "Assegazione ruoli casuali eseguita correttamente")
	return HttpResponseRedirect(reverse("lupus:gm_assegnazione_ruoli", kwargs={"match_id": request.POST.get("matchid")}))


class GMEventsView(WithMatchView):
	template_name = "eventi.html"

	def get_context_data(self, **kwargs):
		context = super(GMEventsView, self).get_context_data(**kwargs)
		flags = {}
		for k in EVENTI:
			flags[k] = EVENTI[k]["Help"]
		context["flags"] = flags
		if context["match"]:
			events = Eventi.objects.all().filter(utente__match=context["match"], date__gte=context["match"].startdate).order_by("-date")
			context["events"] = events
		return context

	def post(self, request, *args, **kwargs):
		kwargs["redirect_url"] = "lupus:gm_gestione_eventi"
		return super(GMEventsView, self).post(request, *args, **kwargs)

@login_required
@user_passes_test(lambda u: has_gm_permissions(u))
def gm_edit_event(request):
	if request.method == "POST" or request.is_ajax():
		id = request.POST.get("id_evento")
		e = get_object_or_404(Eventi, pk=id)
		if has_gm_permissions(request.user) == "staff" and e.utente.match.gm != request.user:
			raise Http404
		try:
			e.date = datetime.strptime(request.POST.get("date"), "%Y-%m-%d")
		except:
			messages.error(request, "Data non valida (aaaa-mm-dd).")
			return HttpResponseRedirect(reverse("lupus:gm_gestione_eventi", kwargs={"match_id": request.POST.get("matchid")}))
		e.flags = int(request.POST.get("flag"))
		e.scena = int(request.POST.get("scena"))
		e.utente = Utente.objects.all().filter(pk=request.POST.get("utente")).first()
		e.save()
		if request.is_ajax():
			return HttpResponse("Ok")
		else:
			messages.success(request, "Modifica effettuata con successo.")
			return HttpResponseRedirect(reverse("lupus:gm_gestione_eventi", kwargs={"match_id": request.POST.get("matchid")}))
	else:
		raise Http404

@login_required
@user_passes_test(lambda u: has_gm_permissions(u))
def gm_del_event(request):
	if request.method == "POST" or request.is_ajax():
		id = request.POST.get("id_evento")
		e = Eventi.objects.all().filter(pk=id).first()
		if has_gm_permissions(request.user) == "staff" and e.utente.match.gm != request.user:
			raise Http404
		e.delete()
		if request.is_ajax():
			return HttpResponse("Ok")
		else:
			messages.success(request, "Eliminazione effettuata con successo.")
			return HttpResponseRedirect(reverse("lupus:gm_gestione_eventi", kwargs={"match_id": request.POST.get("matchid")}))
	else:
		raise Http404

@login_required
@user_passes_test(lambda u: has_gm_permissions(u))
def gm_add_event(request):
	if request.method == "POST":
		e = Eventi()
		try:
			e.date = datetime.strptime(request.POST.get("date"), "%Y-%m-%d")
		except:
			messages.error(request, "Data non valida (aaaa-mm-dd).")
			return HttpResponseRedirect(reverse("lupus:gm_gestione_eventi", kwargs={"match_id": request.POST.get("matchid")}))
		e.flags = int(request.POST.get("flag"))
		scena = int(request.POST.get("scena"))
		if scena != -1:
			e.scena = scena
		u = Utente.objects.all().filter(pk=request.POST.get("utente")).first()
		if has_gm_permissions(request.user) == "staff" and u.match.gm != request.user:
			raise Http404
		e.utente = u
		e.save()
		messages.success(request, "Evento aggiunto con successo.")
		return HttpResponseRedirect(reverse("lupus:gm_gestione_eventi", kwargs={"match_id": request.POST.get("matchid")}))
	else:
		raise Http404

class GMActionView(WithMatchView):
	template_name = "azioni.html"

	def get_context_data(self, **kwargs):
		context = super(GMActionView, self).get_context_data(**kwargs)
		if context["match"]:
			actions = Azioni.objects.all().filter(agente__match=context["match"], time__gte=context["match"].startdate).order_by("-time")
			context["actions"] = actions
		return context

	def post(self, request, *args, **kwargs):
		kwargs["redirect_url"] = "lupus:gm_gestione_azioni"
		return super(GMActionView, self).post(request, *args, **kwargs)

@login_required
@user_passes_test(lambda u: has_gm_permissions(u))
def gm_edit_action(request):
	if request.method == "POST" or request.is_ajax():
		id = request.POST.get("id_azione")
		a = get_object_or_404(Azioni, pk=id)
		if has_gm_permissions(request.user) == "staff" and a.agente.match.gm != request.user:
			raise Http404
		a.bersaglio = Utente.objects.all().filter(pk=request.POST.get("bersaglio")).first()
		a.save()
		if request.is_ajax():
			return HttpResponse("Ok")
		else:
			messages.success(request, "Modifica effettuata con successo.")
			return HttpResponseRedirect(reverse("lupus:gm_gestione_azioni", kwargs={"match_id": request.POST.get("matchid")}))
	else:
		raise Http404

@login_required
@user_passes_test(lambda u: has_gm_permissions(u))
def gm_del_action(request):
	if request.method == "POST" or request.is_ajax():
		id = request.POST.get("id_azione")
		a = Azioni.objects.all().filter(pk=id).first()
		if has_gm_permissions(request.user) == "staff" and a.agente.match.gm != request.user:
			raise Http404
		a.delete()
		if request.is_ajax():
			return HttpResponse("Ok")
		else:
			messages.success(request, "Eliminazione effettuata con successo.")
			return HttpResponseRedirect(reverse("lupus:gm_gestione_azioni", kwargs={"match_id": request.POST.get("matchid")}))
	else:
		raise Http404

@login_required
@user_passes_test(lambda u: has_gm_permissions(u))
def gm_add_action(request):
	if request.method == "POST":
		a = Azioni()
		try:
			a.time = datetime.strptime(request.POST.get("date"), "%Y-%m-%d")
		except:
			messages.error(request, "Data non valida (aaaa-mm-dd).")
			return HttpResponseRedirect(reverse("lupus:gm_gestione_azioni", kwargs={"match_id": request.POST.get("matchid")}))
		agente = Utente.objects.all().filter(pk=request.POST.get("agente")).first()
		bersaglio = Utente.objects.all().filter(pk=request.POST.get("bersaglio")).first()
		if has_gm_permissions(request.user) == "staff" and (agente.match.gm != request.user or bersaglio.match.gm != request.user):
			raise Http404
		a.agente = agente
		a.bersaglio = bersaglio
		a.save()
		messages.success(request, "Azione aggiunta con successo.")
		return HttpResponseRedirect(reverse("lupus:gm_gestione_azioni", kwargs={"match_id": request.POST.get("matchid")}))
	else:
		raise Http404

class GMView(LupusLoggedTemplateView):
	template_name = "gm_index.html"
	gm_required = True

	def get_context_data(self, **kwargs):
		context = super(GMView, self).get_context_data(**kwargs)
		if "id" in kwargs:
			context["single"] = True
			if self.gm_perms == "staff":
				m = Match.objects.all().filter(active = True, pk=kwargs["id"], gm=self.request.user).first()
				if not m:
					messages.error(self.request, "Non puoi gestire questo match")
					return HttpResponseRedirect(reverse("lupus:gm_index"))
				context["match"] = m
			else:
				context["match"] = Match.objects.all().filter(active = True, pk=kwargs["id"]).first()
		else:
			 m = Match.objects.all().filter(active = True, winner="")
			 if self.gm_perms == "staff":
				 m = m.filter(gm=self.request.user)
			 context['matchs'] = []
			 context['reg_matchs'] = []
			 for match in m:
				 if match.isRegistrable(self.now):
					 context['reg_matchs'].append(match)
				 else:
					 context['matchs'].append(match)
		return context

	@method_decorator(login_required)
	def get(self, request, *args, **kwargs):
		if has_gm_permissions(request.user):
			return super(GMView, self).get(request, *args, **kwargs)
		raise Http404

class GMStatusView(WithMatchView):
	template_name = "status.html"

	def get_context_data(self, **kwargs):
		context = super(GMStatusView, self).get_context_data(**kwargs)
		if not context["match"]:
			return context
		stati = {}
		flags = STATUS_FLAG.values()
		context["flags"] = flags
		for u in Utente.objects.all().filter(match=context["match"]):
			stati[u.id] = {"nome":u.nome}
			if not bool(u.stato):
				stati[u.id]["VIVO"] = True
			for f in flags:
				if Status.objects.all().filter(utente=u, flags=STATUS_ID[f["ID"]]).exists():
					if f["DOC_OPT"]:
						stati[u.id][f["ID"]] = Status.objects.all().filter(utente=u, flags=STATUS_ID[f["ID"]]).first().opt
					else:
						stati[u.id][f["ID"]] = True
		context["stati"] = stati
		return context

	def post(self, request, *args, **kwargs):
		kwargs["redirect_url"] = "lupus:gm_gestione_status"
		return super(GMStatusView, self).post(request, *args, **kwargs)

@login_required
@user_passes_test(lambda u: has_gm_permissions(u))
def gm_edit_status(request):
	if request.method == "POST" or request.is_ajax():
		id = request.POST.get("id_utente")
		u = get_object_or_404(Utente, pk=id)
		if has_gm_permissions(request.user) == "staff" and u.match.gm != request.user:
			raise Http404
		#VIVO
		if "vivo" in request.POST:
			u.stato = 0
		else:
			u.stato = 1
		u.save()
		#ALTRI STATI
		for k in STATUS_ID:
			if k.lower() in request.POST: #Se k è prenete nella POST
				if not Status.objects.all().filter(flags=STATUS_ID[k], utente=u).exists():
					s = Status()
					s.utente = u
					s.flags = STATUS_ID[k]
					if STATUS_FLAG[STATUS_ID[k]]["DOC_OPT"]:
						opt = request.POST.get(k.lower()+"opt")
						if not opt:
							opt = "0"
						s.opt = int(opt)
					s.save()
				else:  #Se lo stato esiste già e usa opt si effettua la sua modifica
					if STATUS_FLAG[STATUS_ID[k]]["DOC_OPT"]:
						s = Status.objects.all().filter(flags=STATUS_ID[k], utente=u).first()
						opt = request.POST.get(k.lower()+"opt")
						if not opt:
							opt = "0"
						s.opt = int(opt)
						s.save()
			else: #Se k non è nella POST lo status se esiste va eliminato
				if  Status.objects.all().filter(flags=STATUS_ID[k], utente=u).exists():
					Status.objects.all().filter(flags=STATUS_ID[k], utente=u).first().delete()
		#RISPOSTA
		if request.is_ajax():
			return HttpResponse("Ok")
		else:
			messages.success(request, "Modifica effettuata con successo.")
			return HttpResponseRedirect(reverse("lupus:gm_gestione_status", kwargs={"match_id": request.POST.get("matchid")}))
	else:
		raise Http404()

@login_required
@user_passes_test(lambda u: has_gm_permissions(u))
def gm_clear_status(request):
	if request.method == "POST":
		id = request.POST.get("matchid")
		m = get_object_or_404(Match, pk=id)
		if has_gm_permissions(request.user) == "staff" and m.gm != request.user:
			raise Http404
		for u in Utente.objects.all().filter(match=m):
			u.stato = 0
			u.save()
		for s in Status.objects.all().filter(utente__match=m):
			s.delete()
		messages.success(request, "Pulizia stati effettuata con successo.")
		return HttpResponseRedirect(reverse("lupus:gm_gestione_status", kwargs={"match_id": request.POST.get("matchid")}))
	else:
		raise Http404()

@login_required
@user_passes_test(lambda u: has_gm_permissions(u))
def gm_status_random(request):
	if request.method == "POST":
		id = request.POST.get("matchid")
		if has_gm_permissions(request.user) == "staff" and not Match.objects.all().filter(id=id, gm=request.user).exists():
			raise Http404
		u = Utente.objects.all().filter(match__id=id, stato=0)
		if "sind" in request.POST:
			s = Status()
			utente = random.choice(u)
			while Status.objects.all().filter(utente=utente, flags = STATUS_ID["SIND"]).exists() and Status.objects.all().filter(flags = STATUS_ID["SIND"]).order_by("-opt")[0].utente == utente: #Se lo status è già assegnato all'utente, si cambia utente
				utente = random.choice(u)
			s.utente = utente
			s.flags = STATUS_ID["SIND"]
			match = get_object_or_404(Match, pk=request.POST.get("matchid"))
			s.opt = diff_giorni(now().date(), match.startdate) ## self.giorno ???
			s.save()
		if "vecc" in request.POST:
			s = Status()
			utente = random.choice(u)
			while Status.objects.all().filter(utente=utente, flags = STATUS_ID["VECC"]).exists(): #Se lo status è già assegnato all'utente, si cambia utente
				utente = random.choice(u)
			s.utente = utente
			s.flags = STATUS_ID["VECC"]
			s.save()
		if "cup" in request.POST:
			s = Status()
			utente = random.choice(u)
			while Status.objects.all().filter(utente=utente, flags = STATUS_ID["CUP"]).exists(): #Se lo status è già assegnato all'utente, si cambia utente
				utente = random.choice(u)
			s.utente = utente
			s.flags = STATUS_ID["CUP"]
			s.save()
		if "roc" in request.POST:
			s = Status()
			utente = random.choice(u)
			while Status.objects.all().filter(utente=utente, flags = STATUS_ID["ROC"]).exists(): #Se lo status è già assegnato all'utente, si cambia utente
				utente = random.choice(u)
			s.utente = utente
			s.flags = STATUS_ID["ROC"]
			s.save()
		messages.success(request, "Assegnazione casuale degli stati effettuata con successo.")
		return HttpResponseRedirect(reverse("lupus:gm_gestione_status", kwargs={"match_id": request.POST.get("matchid")}))
	else:
		raise Http404()

class GMGroupsView(LupusGMTemplateView):
	template_name = "gruppi.html"
	script = True

	def get_context_data(self, **kwargs):
		context = super(GMGroupsView, self).get_context_data(**kwargs)
		context["roles"] = RUOLI
		g = Gruppo.objects.all()
		if self.gm_perms == "staff":
			g = g.filter(match__gm=self.request.user)
		context["gruppi"] = g
		m = Match.objects.all() #Eventuali filtri
		if self.gm_perms == "staff":
			m = m.filter(gm=self.request.user)
		context["matches"] = m
		return context

	@method_decorator(login_required)
	def get(self, request, *args, **kwargs):
		if has_gm_permissions(request.user):
			return super(GMGroupsView, self).get(request, *args, **kwargs)
		raise Http404

@login_required
@user_passes_test(lambda u: has_gm_permissions(u))
def gm_add_group(request):
	if request.method == "POST" or request.is_ajax():
		g = Gruppo()
		g.name = request.POST.get("nome")
		if g.name == "":
			messages.error(request, "Nome del gruppo richiesto.")
			return HttpResponseRedirect(reverse("lupus:gm_gestione_gruppi"))
		#g.ruolo = Ruolo.objects.all().filter(pk=request.POST.get("ruolo")).first()
		g.ruolo = request.POST.get("ruolo")
		m = get_object_or_404(Match, pk=request.POST.get("match"))
		if has_gm_permissions(request.user) == "staff" and m.gm != request.user:
			raise Http404
		g.match = m
		g.save()
		if request.is_ajax():
			return HttpResponse("Ok")
		else:
			messages.success(request, "Gruppo aggiunto con successo.")
			return HttpResponseRedirect(reverse("lupus:gm_gestione_gruppi"))
	else:
		raise Http404()

@login_required
@user_passes_test(lambda u: has_gm_permissions(u))
def gm_edit_group(request):
	if request.method == "POST" or request.is_ajax():
		id = request.POST.get("id_gruppo")
		g = get_object_or_404(Gruppo, pk=id)
		g.name = request.POST.get("nome")
		if g.name == "":
			if request.is_ajax():
				raise Http404
			else:
				messages.error(request, "Nome del gruppo richiesto.")
				return HttpResponseRedirect(reverse("lupus:gm_gestione_gruppi"))
		#g.ruolo = Ruolo.objects.all().filter(pk=request.POST.get("ruolo")).first()
		g.ruolo = request.POST.get("ruolo")
		m = get_object_or_404(Match, pk=request.POST.get("match"))
		if has_gm_permissions(request.user) == "staff" and m.gm != request.user:
			raise Http404
		g.match = m
		g.save()
		if request.is_ajax():
			return HttpResponse("Ok")
		else:
			messages.success(request, "Modifica effettuata con successo.")
			return HttpResponseRedirect(reverse("lupus:gm_gestione_gruppi"))
	else:
		raise Http404()

@login_required
@user_passes_test(lambda u: has_gm_permissions(u))
def gm_del_group(request):
	if request.method == "POST" or request.is_ajax():
		id = request.POST.get("id_gruppo")
		if not Match.objects.all().filter(villaggio__id=id).exists():
			g = Gruppo.objects.all().filter(pk=id).first()
			if has_gm_permissions(request.user) == "staff" and g.match.gm != request.user:
				raise Http404
			g.delete()
			if request.is_ajax():
				return HttpResponse("Ok")
			else:
				messages.success(request, "Eliminazione effettuata con successo.")
				return HttpResponseRedirect(reverse("lupus:gm_gestione_gruppi"))
		else:
			if request.is_ajax():
				return HttpResponse("Fail")
			else:
				messages.error(request, "Impossibile eliminare un gruppo usato come villaggio in un match.")
				return HttpResponseRedirect(reverse("lupus:gm_gestione_gruppi"))
	else:
		raise Http404()

class GMMatchView(LupusGMTemplateView):
	template_name = "match.html"
	script = True

	def get_context_data(self, **kwargs):
		context = super(GMMatchView, self).get_context_data(**kwargs)
		g = Gruppo.objects.all()
		if self.gm_perms == "staff":
			g = g.filter(match__gm=self.request.user)
		context["gruppi"] = g
		m = Match.objects.all()
		if self.gm_perms == "staff":
			m = m.filter(gm=self.request.user)
		context["matches"] = m
		context["factions"] = FAZIONI_CHOICES
		if self.gm_perms != "staff":
			context["users"] = [{"id":u.id, "nome":u.get_full_name()} for u in User.objects.filter(is_staff=True)]
		context["su_perms"] = self.gm_perms == "superuser"
		context["today"] = self.today
		context["tomorrow"] = self.today + timedelta(days=1)
		return context

	@method_decorator(login_required)
	def get(self, request, *args, **kwargs):
		if has_gm_permissions(request.user):
			return super(GMMatchView, self).get(request, *args, **kwargs)
		raise Http404

@login_required
@user_passes_test(lambda u: has_gm_permissions(u))
def gm_add_match(request):
	if request.method == "POST" or request.is_ajax():
		m = Match()
		m.nome = request.POST.get("nome")
		if m.nome == "":
			messages.error(request, "Nome del match richiesto.")
			return HttpResponseRedirect(reverse("lupus:gm_gestione_match"))
		try:
			m.regenddate = datetime.strptime(request.POST.get("regenddate"), "%Y-%m-%d").date()
			m.startdate = datetime.strptime(request.POST.get("startdate"), "%Y-%m-%d").date()
		except:
			messages.error(request, "Date non valide (aaaa-mm-dd).")
			return HttpResponseRedirect(reverse("lupus:gm_gestione_match"))
		if m.regenddate < now().date():
			messages.error(request, "Data di fine registrazioni non valida.")
			return HttpResponseRedirect(reverse("lupus:gm_gestione_match"))
		if m.startdate < m.regenddate:
			messages.error(request, "Data di inizio match non valida.")
			return HttpResponseRedirect(reverse("lupus:gm_gestione_match"))
		g = Gruppo()
		g.name = request.POST.get("villaggio")
		if g.name == "":
			messages.error(request, "Nome del villaggio richiesto.")
			return HttpResponseRedirect(reverse("lupus:gm_gestione_match"))
		if "attivo" in request.POST:
			m.active = True
		else:
			m.active = False
		if has_gm_permissions(request.user) == "superuser":
			gm_id = request.POST.get("gm")
			m.gm = get_object_or_404(User, pk=gm_id) if gm_id != "" else None
		else:
			m.gm = request.user
		m.save()
		g.match = m
		g.save()
		m.villaggio = g
		m.save()
		#Match.objects.filter(id=m.id).update(villaggio=g)
		if request.is_ajax():
			return HttpResponse("Ok")
		else:
			messages.success(request, "Match aggiunto con successo.")
			return HttpResponseRedirect(reverse("lupus:gm_gestione_match"))
	else:
		raise Http404()

@login_required
@user_passes_test(lambda u: has_gm_permissions(u))
def gm_edit_match(request):
	if request.method == "POST" or request.is_ajax():
		id = request.POST.get("id_match")
		m = get_object_or_404(Match, pk=id)
		if has_gm_permissions(request.user) == "staff" and m.gm != request.user: #Se tenatativo di modificare un match che non gli appartiene
			raise Http404
		try:
			m.enddate = datetime.strptime(request.POST.get("enddate"), "%Y-%m-%d").date()
		except:
			m.enddate = None
		if not (m.isStarted() or m.winner): #Se sono modificabili
			try:
				m.regenddate = datetime.strptime(request.POST.get("regenddate"), "%Y-%m-%d").date()
				m.startdate = datetime.strptime(request.POST.get("startdate"), "%Y-%m-%d").date()
			except:
				if request.is_ajax():
					return HttpResponse("Fail")
				else:
					messages.success(request, "Errore nella modifica.")
					return HttpResponseRedirect(reverse("lupus:gm_gestione_match"))
			error = False
			if m.regenddate < now().date():
				error = "Data di fine registrazioni non valida."
			if m.startdate < m.regenddate:
				error = "Data di inizio match non valida."
			if error:
				if request.is_ajax():
					return HttpResponse("Fail")
				else:
					messages.success(request, error)
					return HttpResponseRedirect(reverse("lupus:gm_gestione_match"))
		if "attivo" in request.POST:
			m.active = True
		else:
			m.active = False
		m.winner = request.POST.get("winner")
		if has_gm_permissions(request.user) == "superuser":
			gm_id = request.POST.get("gm")
			m.gm = get_object_or_404(User, pk=gm_id) if gm_id != "" else None
		m.save()
		if request.is_ajax():
			return HttpResponse("Ok")
		else:
			messages.success(request, "Modifica effettuata con successo.")
			return HttpResponseRedirect(reverse("lupus:gm_gestione_match"))
	else:
		raise Http404()

@login_required
@user_passes_test(lambda u: has_gm_permissions(u))
def gm_del_match(request):
	if request.method == "POST" or request.is_ajax():
		id = request.POST.get("id_match")
		if has_gm_permissions(request.user) == "staff" and Match.objects.all().filter(pk=id).first().gm != request.user: #Se tenatativo di eliminare un match che non gli appartiene
			raise Http404
		# Elimino dati relativi dal database
		Eventi.objects.all().filter(utente__match__id=id).delete()
		Nota.objects.all().filter(utente1__match__id=id).delete()
		Status.objects.all().filter(utente__match__id=id).delete()
		Messaggio.objects.all().filter(utente__match__id=id).delete()
		Azioni.objects.all().filter(agente__match__id=id).delete()
		Votazione.objects.all().filter(utente__match__id=id).delete()
		Voto.objects.all().filter(votante__match__id=id).delete()
		LogGestione.objects.all().filter(match__id=id).delete()
		Utente.objects.all().filter(match__id=id).delete()
		m = Match.objects.all().filter(pk=id).first().delete()
		if request.is_ajax():
			return HttpResponse("Ok")
		else:
			messages.success(request, "Eliminazione effettuata con successo.")
			return HttpResponseRedirect(reverse("lupus:gm_gestione_match"))
	else:
		raise Http404()

@login_required
@user_passes_test(lambda u: has_gm_permissions(u))
def gm_deactivate_users_match(request):
	if request.method != "POST":
		raise Http404
	id = request.POST.get("id_match")
	if get_object_or_404(Match, pk=id).gm != request.user and has_gm_permissions(request.user) != "superuser":
		messages.error(request, "Non puoi gestire questo match.")
		return HttpResponseRedirect(reverse("lupus:gm_gestione_match"))
	#for u in Utente.objects.all().filter(match_id=id):
	#	u.active = False
	#	u.save()
	Utente.objects.filter(match_id=id).update(active=False)
	messages.success(request, "Disattivazione utenti effettuata con successo.")
	return HttpResponseRedirect(reverse("lupus:gm_gestione_match"))

class GMVotiView(WithMatchView):
	template_name = "voti.html"

	def get_context_data(self, **kwargs):
		context = super(GMVotiView, self).get_context_data(**kwargs)
		if context["match"]:
			context["voti"] = Voto.objects.all().filter(votante__match = context["match"], time__gte=context["match"].startdate).order_by("-time")
		return context

	def post(self, request, *args, **kwargs):
		kwargs["redirect_url"] = "lupus:gm_gestione_voti"
		return super(GMVotiView, self).post(request, *args, **kwargs)

@login_required
@user_passes_test(lambda u: has_gm_permissions(u))
def gm_add_voto(request):
	if request.method == "POST" or request.is_ajax():
		v = Voto()
		try:
			v.time = datetime.strptime(request.POST.get("date"), "%Y-%m-%d")
		except:
			messages.error(request, "Data non valida (aaaa-mm-dd).")
			return HttpResponseRedirect(reverse("lupus:gm_gestione_voti", kwargs={"match_id": request.POST.get("matchid")}))
		votante = get_object_or_404(Utente, pk=request.POST.get("votante"))
		if has_gm_permissions(request.user) == "staff" and votante.match.gm != request.user:
			raise Http404
		v.votante = votante
		v.flags = request.POST.get("motivo")
		id = request.POST.get("votato")
		votato = get_object_or_404(Utente, pk=id)
		if has_gm_permissions(request.user) == "staff" and votato.match.gm != request.user:
			raise Http404
		try:
			voti = Votazione.objects.get(date=v.time, flags=v.flags, utente=votato)
		except (Votazione.DoesNotExist) :
			voti = Votazione(date=v.time, flags=v.flags, utente=votato)
		voti.count += 1
		voti.save()
		v.risultato = voti
		v.save()
		if request.is_ajax():
			return HttpResponse("Ok")
		else:
			messages.success(request, "Voto aggiunto con successo.")
			return HttpResponseRedirect(reverse("lupus:gm_gestione_voti", kwargs={"match_id": request.POST.get("matchid")}))
	else:
		raise Http404()

@login_required
@user_passes_test(lambda u: has_gm_permissions(u))
def gm_edit_voto(request):
	if request.method == "POST" or request.is_ajax():
		id = request.POST.get("id_voto")
		flag = request.POST.get("motivo")
		id_utente = request.POST.get("votato")
		v = get_object_or_404(Voto, pk=id)
		if has_gm_permissions(request.user) == "staff" and (v.votante.match.gm != request.user or v.risultato.utente.match.gm != request.user):
			raise Http404
		if v.risultato.flags != flag or v.risultato.utente.id != id_utente: #Procede solo se il voto è stato effettivamente modificato
			v.flags = flag
			v.risultato.count -= 1
			v.risultato.save()
			try:
				voti = Votazione.objects.get(date=v.time, flags=flag, utente__id=id_utente)
			except (Votazione.DoesNotExist) :
				voti = Votazione(date=v.time, flags=v.flags, utente=Utente.objects.all().filter(pk=id_utente).first())
			voti.count += 1
			voti.save()
			v.risultato = voti
			v.save()
		if request.is_ajax():
			return HttpResponse("Ok")
		else:
			messages.success(request, "Modifica effettuata con successo.")
			return HttpResponseRedirect(reverse("lupus:gm_gestione_voti", kwargs={"match_id": request.POST.get("matchid")}))
	else:
		raise Http404()

@login_required
@user_passes_test(lambda u: has_gm_permissions(u))
def gm_del_voto(request):
	if request.method == "POST" or request.is_ajax():
		id = request.POST.get("id_voto")
		v = get_object_or_404(Voto, pk=id)
		if has_gm_permissions(request.user) == "staff" and v.votante.match.gm != request.user:
			raise Http404
		v.risultato.count -= 1
		if v.risultato.count <= 0:
			v.risultato.delete() #Elimina anche il voto
		else:
			v.risultato.save()
			v.delete()
		if request.is_ajax():
			return HttpResponse("Ok")
		else:
			messages.success(request, "Voto eliminato con successo.")
			return HttpResponseRedirect(reverse("lupus:gm_gestione_voti", kwargs={"match_id": request.POST.get("matchid")}))
	else:
		raise Http404()

class GMNotaView(WithMatchView):
	template_name = "nota.html"

	def get_context_data(self, **kwargs):
		context = super(GMNotaView, self).get_context_data(**kwargs)
		if context["match"]:
			notes = Nota.objects.all().filter(utente1__match=context["match"], time__gte=context["match"].startdate).order_by("-time")
			context["notes"] = notes
			context["choices"] = NOTE_CHOICES
		return context

	def post(self, request, *args, **kwargs):
		kwargs["redirect_url"] = "lupus:gm_gestione_note"
		return super(GMNotaView, self).post(request, *args, **kwargs)

@login_required
@user_passes_test(lambda u: has_gm_permissions(u))
def gm_edit_nota(request):
	if request.method == "POST" or request.is_ajax():
		id = request.POST.get("id_nota")
		n = get_object_or_404(Nota, pk=id)
		if has_gm_permissions(request.user) == "staff" and n.utente1.match.gm != request.user:
			raise Http404
		id2 = request.POST.get("utente2")
		n.utente2 = Utente.objects.all().filter(pk=id2).first() if id2 != "" else None
		n.flags = str(request.POST.get("flags"))
		n.opt = int(request.POST.get("opt"))
		n.text = request.POST.get("text")
		n.save()
		if request.is_ajax():
			return HttpResponse("Ok")
		else:
			messages.success(request, "Modifica effettuata con successo.")
			return HttpResponseRedirect(reverse("lupus:gm_gestione_note", kwargs={"match_id": request.POST.get("matchid")}))
	else:
		raise Http404

@login_required
@user_passes_test(lambda u: has_gm_permissions(u))
def gm_del_nota(request):
	if request.method == "POST" or request.is_ajax():
		id = request.POST.get("id_nota")
		n = Nota.objects.all().filter(pk=id).first()
		if has_gm_permissions(request.user) == "staff" and n.utente1.match.gm != request.user:
			raise Http404
		n.delete()
		if request.is_ajax():
			return HttpResponse("Ok")
		else:
			messages.success(request, "Eliminazione effettuata con successo.")
			return HttpResponseRedirect(reverse("lupus:gm_gestione_note", kwargs={"match_id": request.POST.get("matchid")}))
	else:
		raise Http404

@login_required
@user_passes_test(lambda u: has_gm_permissions(u))
def gm_add_nota(request):
	if request.method == "POST":
		n = Nota()
		try:
			n.time = datetime.strptime(request.POST.get("date"), "%Y-%m-%d")
		except:
			messages.error(request, "Data non valida (aaaa-mm-dd).")
			return HttpResponseRedirect(reverse("lupus:gm_gestione_note", kwargs={"match_id": request.POST.get("matchid")}))
		utente1 = Utente.objects.all().filter(pk=request.POST.get("utente1")).first()
		if has_gm_permissions(request.user) == "staff" and utente1.match.gm != request.user:
			raise Http404
		n.utente1 = utente1
		id2 = request.POST.get("utente2")
		utente2 = Utente.objects.all().filter(pk=id2).first() if id2 != "" else None
		if utente2 and has_gm_permissions(request.user) == "staff" and utente2.match.gm != request.user:
			raise Http404
		n.utente2 = utente2
		n.flags = str(request.POST.get("flags"))
		n.opt = int(request.POST.get("opt"))
		n.text = request.POST.get("text")
		n.save()
		messages.success(request, "Azione aggiunta con successo.")
		return HttpResponseRedirect(reverse("lupus:gm_gestione_note", kwargs={"match_id": request.POST.get("matchid")}))
	else:
		raise Http404

class GMChangeProfileView(LupusGMTemplateView):
	template_name = "profile.html"

	def get_context_data(self, **kwargs):
		self.request.session["lupus_gm_setprofile_next"] = self.request.GET.get("next",self.request.META.get("HTTP_REFERER",reverse("lupus:gm_setprofile")))
		context = super(GMChangeProfileView, self).get_context_data(**kwargs)
		context["users"] = Utente.objects.all().filter(active=True)
		if self.gm_perms == "staff" :
			context["users"] = context["users"].filter(match__gm = self.request.user, match__winner = "", match__active = True)
		return context

	@method_decorator(login_required)
	def post(self, request, *args, **kwargs):
		if not self.gm_perms :
			raise Http404
		next_url = self.request.session.pop("lupus_gm_setprofile_next", reverse_lazy("lupus:gm_setprofile"))
		id = request.POST.get("profile")
		if id == "-1": #Se -1 indica di impostare il profilo proprio
			request.session["lupus_gm_asprofile"] = None
			return HttpResponseRedirect(next_url)
		u = get_object_or_404(Utente, pk=id)
		if not (self.gm_perms == "superuser") :
			if not ((u.match.gm == self.request.user) and (u.active) and (u.match.winner == "")) :
				request.session["lupus_gm_asprofile"] = None
				return HttpResponseRedirect(next_url)
		request.session["lupus_gm_asprofile"] = u.id
		return HttpResponseRedirect(next_url)

def create_StatiForm(utenti_vivi, utenti_morti):
	class StatiForm(forms.Form):
		morti = forms.MultipleChoiceField(widget=forms.CheckboxSelectMultiple, choices=[(int(u.id), u.nome) for u in utenti_vivi], required=False)
		risorti = forms.MultipleChoiceField(widget=forms.CheckboxSelectMultiple, choices=[(int(u.id), u.nome) for u in utenti_morti], required=False)
	return StatiForm

def create_EventoForm(users):
	class EventoForm(forms.Form):
		flags = forms.IntegerField(widget=forms.Select(choices=EVENTI_CHOICES))
		#scena = forms.IntegerField(min_value=0, required=False)
		utente = forms.IntegerField(widget=forms.Select(choices=[(int(u.id), u.nome) for u in users]))
	return EventoForm

def create_StatusForm(users):
	class StatusForm(forms.Form):
		flags = forms.IntegerField(widget=forms.Select(choices=STATUS_CHOICES))
		opt = forms.IntegerField(min_value=0, required=False)
		utente = forms.IntegerField(widget=forms.Select(choices=[(int(u.id), u.nome) for u in users]))
	return StatusForm

def create_NotaForm(users):
	class NotaForm(forms.Form):
		flags = forms.CharField(widget=forms.Select(choices=NOTE_CHOICES))
		utente1 = forms.IntegerField(widget=forms.Select(choices=[(u.id, u.nome) for u in users]))
		utente2 = forms.IntegerField(widget=forms.Select(choices=[(u.id, u.nome) for u in users]+[(None, "")]), required=False)
		opt = forms.IntegerField(required=False)
		text = forms.CharField(required=False)
	return NotaForm

class GMValView(WithMatchView):
	template_name = "valutazione.html"

	def get_context_night(self, match):
		context = {}
		today = self.giorno
		if self.isnownight or not match or not match.isStarted(): #Se fuori orario o se il match non è valido o se non è ancora iniziato
			context["valid"] = False
			context["start"] = GAMETIME_NIGHTSTART
			context["end"] = GAMETIME_NIGHTEND
			context["isWEnight"] = GAMETIME_WEEKENDISNIGTH
			return context
		if LogGestione.objects.filter(date=today, flag=2, match_id=match.id).exists():
			context["valid"] = False
			context["messag"] = "Valutazione già effettuata."
			return context
		context["valid"] = True
		azioni = get_azioni(match, today)
		for a in azioni: #Necessario per la corretta visualizzazione dei ruoli, altrimenti si vede "CON", ...
				a.agente.ruolo = a.agente.get_ruolo()
				a.bersaglio.ruolo = a.bersaglio.get_ruolo()
		context["azioni"] = azioni #Azioni della notte
		vecchi = []
		for s in Status.objects.all().filter(utente__match=match, utente__stato=0, flags=STATUS_ID["VECC"]):
			vecchi.append(s.utente)
		context["vecchi"] = vecchi
		innamorati = {}
		for s in Status.objects.all().filter(utente__match=match, utente__stato=0, flags=STATUS_ID["INN"]):
			if s.opt in innamorati:
				innamorati[s.opt].append(s.utente)
			else:
				innamorati[s.opt] = [s.utente]
		context["innamorati"] = innamorati.values()
		return context

	def get_context_day(self, match):
		context = {}
		today = self.giorno
		if self.now.date() != today:
			today = somma_giorni(today, -1)
		if not match:
			context["valid"] = False
			context["messag"] = "Il match selezionato non è valido."
			context["svalid"] = context["valid"]
			context["smessag"] = context["messag"]
			return context
		if self.now.time() < GAMETIME_DAYEND:
			context["valid"] = False
			context["messag"] = "Le votazioni sono ancora in corso, chiuderanno alle ore %s"%(GAMETIME_DAYEND.strftime("%H:%M"))
			context["svalid"] = context["valid"]
			context["smessag"] = context["messag"]
			return context
		#Utenti che potevano votare
		context["numero"] = Utente.objects.all().filter(stato=0, match=match).count()
		#Per linciaggio
		if LogGestione.objects.filter(date=today, flag=1, match_id=match.id).exists():
			context["valid"] = False
			context["messag"] = "Valutazione già effettuata."
			return context
		else:
			voti = get_voti(match, today, VOTO_FLAG["LINCIAGGIO"])
			votes = voti["voti"]
			if len(votes) == 0:
				context["valid"] = False
				context["messag"] = "Non ci sono voti."
			else:
				context["valid"] = True
				context["votes"] = votes
				context["conteggio_linciaggio"] = voti["count"]
		#Per elezione sindaco
		if Eventi.objects.all().filter(date=today, flags=11, utente__match=match).count() > 0:
			context["svalid"] = False
			context["smessag"] = "Oggi è già stato eletto il sindaco."
		else:
			svoti = get_voti(match, today, VOTO_FLAG["SINDACO"])
			svotes = svoti["voti"]
			if len(svotes) == 0:
				context["svalid"] = False
				context["smessag"] = "Non ci sono voti."
			else:
				context["svalid"] = True
				context["svotes"] = svotes
				context["conteggio_sindaco"] = svoti["count"]
		context["vivi"] = Utente.objects.all().filter(match=match, stato=0)
		return context

	def get_context_data(self, **kwargs):
		context = super(GMValView, self).get_context_data(**kwargs)
		if not "flags" in kwargs:
			raise Http404
		self.request.session["val_flags"] = kwargs["flags"]
		val = None
		today = self.giorno
		if kwargs["flags"] == 2: #Notte
			context.update(self.get_context_night(context["match"]))
			context["flags"] = "notte"
			if not context["valid"]:
				return context
			val = calcola_notte(context["match"], today)
			context["night_id"] = val["nightID"]
		else:
			context.update(self.get_context_day(context["match"]))
			context["flags"] = "giorno"
			if not context["valid"]:
				return context
			val = calcola_giorno(context["match"], today if self.now.date() == today else somma_giorni(today, -1))
			context["day_id"] = val["dayID"]
		users = Utente.objects.all().filter(match=context["match"])
		StatiForm = create_StatiForm(users.filter(stato=0), users.filter(stato=1))
		stati_form = StatiForm(None, initial={"morti":val["Morti"], "risorti":val["Risorti"]}, prefix="stati")
		EventiFormSet = forms.formset_factory(create_EventoForm(users), extra=0, can_delete=True)
		eventi_formset = EventiFormSet(initial=val["Eventi"], prefix="eventi")
		NoteFormSet = forms.formset_factory(create_NotaForm(users), extra=0, can_delete=True)
		note_formset = NoteFormSet(initial=val["Note"], prefix="note")
		StatusFormSet = forms.formset_factory(create_StatusForm(users), extra=0, can_delete=True)
		status_formset = StatusFormSet(initial=val["Status"], prefix="status")
		context["stati_form"] = stati_form
		context["eventi_formset"] = eventi_formset
		context["note_formset"] = note_formset
		context["status_formset"] = status_formset
		return context

	def post(self, request, *args, **kwargs):
		flags = request.session.get("val_flags", None)
		if not flags:
			raise Http404
		if flags == 2:
			kwargs["redirect_url"] = "lupus:gm_gestione_notte"
		else:
			kwargs["redirect_url"] = "lupus:gm_linciaggio"
		return super(GMValView, self).post(request, *args, **kwargs)

class GMValConfirmView(LupusGMTemplateView):
	template_name = "ask_valutazione.html"
	script = True

	def get_context_data(self, **kwargs):
		context = super(GMValConfirmView, self).get_context_data(**kwargs)
		mid = kwargs.get("mid", 0)
		match = get_object_or_404(Match, pk=mid)
		if self.gm_perms == "staff" and match.gm != self.request.user:
			raise Http404
		flags = self.request.session.get("val_flags", None)
		if not flags:
			raise Http404
		context["flags"] = flags
		context["eventi_flags"] = EVENTI
		context["note_flags"] = NOTE
		context["status_flags"] = STATUS_FLAG
		val = self.request.session.get("valutazione", None)
		if not val:
			raise Http404
		morti = set(val.get("Morti", []))
		context["morti"] = Utente.objects.all().filter(id__in=morti)
		risorti = set(val.get("Risorti", []))
		context["risorti"] = Utente.objects.all().filter(id__in=risorti)
		context["eventi"] = val.get("Eventi", [])
		context["note"] = val.get("Note", [])
		context["status"] = val.get("Status", [])
		context["mid"] = mid
		users_id = set([e["utente"] for e in context["eventi"]] + [n["utente1"] for n in context["note"]] + [n["utente2"] for n in context["note"]] + [n["utente"] for n in context["status"]])
		users = Utente.objects.all().filter(id__in=users_id)
		context["users"] = {u.id:u.nome for u in users}
		return context

	def post(self, request, *args, **kwargs):
		users = Utente.objects.all()
		StatiForm = create_StatiForm(users, users)
		stati = StatiForm(request.POST, prefix="stati")
		EventiFormSet = forms.formset_factory(create_EventoForm(users), extra=0, can_delete=True)
		eventi = EventiFormSet(request.POST, prefix="eventi")
		NoteFormSet = forms.formset_factory(create_NotaForm(users), extra=0, can_delete=True)
		note = NoteFormSet(request.POST, prefix="note")
		StatusFormSet = forms.formset_factory(create_StatusForm(users), extra=0, can_delete=True)
		status = StatusFormSet(request.POST, prefix="status")
		if not (stati.is_valid() and eventi.is_valid() and note.is_valid() and status.is_valid()):
			messages.error(request, "I dati inseriti non sono validi.")
			flags = request.session.get("val_flags", None)
			if not flags:
				raise Http404
			if flags == 2:
				url = "lupus:gm_gestione_notte"
			else:
				url = "lupus:gm_linciaggio"
				return HttpResponseRedirect(reverse(url, request.POST.get("mid")))

		def clean(input):
			del input["DELETE"]
			if "opt" in input and input["opt"] in [None, ""]:
				del input["opt"]
			return input

		request.session["valutazione"] = {
											"Morti": stati.cleaned_data["morti"],
											"Risorti":stati.cleaned_data["risorti"],
											"Eventi": [clean(f.cleaned_data) for f in eventi if not f.cleaned_data["DELETE"]],
											"Note": [clean(f.cleaned_data) for f in note if not f.cleaned_data["DELETE"]],
											"Status": [clean(f.cleaned_data) for f in status if not f.cleaned_data["DELETE"]]
										}
		kwargs["mid"] = request.POST.get("mid", 0)
		return self.get(request, *args, **kwargs)

class GMValDoPostView(LupusLoggedView):
	gm_required = True
	def post(self, request, *args, **kwargs):
		flags = request.session.get("val_flags", None)
		if not flags:
			raise Http404
		if flags == 2:
			url = "lupus:gm_gestione_notte"
		else:
			url = "lupus:gm_linciaggio"
		match = get_object_or_404(Match, pk=request.POST.get("mid"))
		if request.POST.get("confirm") != "true":
			messages.error(request, "Errore, la valutazione non è stata confermata.")
			return HttpResponseRedirect(reverse(url, kwargs={"match_id": request.POST.get("mid")}))
		val = request.session.pop("valutazione", None)
		if not val:
			return HttpResponseRedirect("lupus:gm_index")
		if LogGestione.objects.filter(match=match, date=self.giorno, flag=flags).exists() :
			messages.error(request, "Errore, valutazione già effettuata.")
			return HttpResponseRedirect(reverse(url, kwargs={"match_id": request.POST.get("mid")}))
		error = applica_calcolo(val, self.giorno)
		(LogGestione(azioni=repr(val), match=match, date=self.giorno, flag=flags, automatico=False)).save()
		if error:
			messages.error(request, error)
		else:
			messages.success(request, "Valutazione eseguita con successo.")
		winner = somebody_won(match)
		if winner:
			request.session["winner"] = winner
			return HttpResponseRedirect(reverse("lupus:gm_end_match", kwargs={"match_id": request.POST.get("mid")}))
		return HttpResponseRedirect(reverse("lupus:gm_index"))

@login_required
@user_passes_test(lambda u: has_gm_permissions(u))
def gm_val_do(request):
	if request.method != "POST":
		raise Http404
	flags = request.session.get("val_flags", None)
	if not flags:
		raise Http404
	if flags == 2:
		url = "lupus:gm_gestione_notte"
	else:
		url = "lupus:gm_linciaggio"
	if request.POST.get("confirm") != "true":
		messages.error(request, "Errore, la valutazione non è stata confermata.")
		return HttpResponseRedirect(reverse(url, kwargs={"match_id": request.POST.get("mid")}))
	val = request.session.pop("valutazione", None)
	if not val:
		raise Http404
	localnow = now()
	match = get_object_or_404(Match, pk=request.POST.get("mid"))
	if LogGestione.objects.filter(match=match, date=giornodigioco(localnow), flag=flags).exists() :
		messages.error(request, "Errore, valutazione già effettuata.")
		return HttpResponseRedirect(reverse(url, kwargs={"match_id": request.POST.get("mid")}))
	error = applica_calcolo(val, giornodigioco(localnow))
	messages.info(request,"DEBUG :<br>:%s<br>:%s"%(str(localnow),str(giornodigioco(localnow))))
	log = LogGestione(azioni=repr(val), match=match, date=giornodigioco(localnow), flag=flags, automatico=False)
	log.save()
	if error:
		messages.error(request, error)
	else:
		messages.success(request, "Valutazione eseguita con successo.")
	winner = somebody_won(match)
	if winner:
		request.session["winner"] = winner
		return HttpResponseRedirect(reverse("lupus:gm_end_match", kwargs={"match_id": request.POST.get("mid")}))
	return HttpResponseRedirect(reverse("lupus:gm_index"))

class GMEndMatch(WithMatchView):
	template_name = "end.html"

	def get_context_data(self, **kwargs):
		context = super(GMEndMatch, self).get_context_data(**kwargs)
		if not context["match"]:
			messages.error(self.request, "Nessun match valido")
			context["msg"] = "mai iniziato"
			return context
		winner = self.request.session.pop("winner", False)
		if not winner:
			winner = somebody_won(context["match"])
		context["winner"] = winner
		context["msg"] = FAZIONI_MSG[winner]
		return context

	def post(self, request, *args, **kwargs):
		winner = request.POST.get("winner", False)
		if winner:
			"""
			mid = request.POST.get("mid")
			m = get_object_or_404(Match, pk=mid)
			m.winner = winner
			m.enddate = self.today
			m.save()
			"""
			if Match.objects.filter(pk=mid).update(winner=winner, enddate=self.today):
				messages.success(request, "Match concluso con successo.")
			else :
				message.error(request, "Match non valido")
			return HttpResponseRedirect(reverse("lupus:gm_index"))
		else:
			kwargs["redirect_url"] = "lupus:gm_end_match"
			return super(GMEndMatch, self).post(request, *args, **kwargs)

class GMStaffView(LupusGMTemplateView):
	template_name = "staff.html"
	script = True

	def get_context_data(self, **kwargs):
		context = super(GMStaffView, self).get_context_data(**kwargs)
		context["staff"] = User.objects.all().filter(is_staff=True, is_superuser=False)
		context["superusers"] = User.objects.all().filter(is_superuser=True)
		context["candidates"] = set(User.objects.all()) - set(context["staff"]) - set(context["superusers"])
		return context

	@method_decorator(login_required)
	def get(self, request, *args, **kwargs):
		if has_gm_permissions(request.user) == "superuser":
			return super(GMStaffView, self).get(request, *args, **kwargs)
		raise Http404

@login_required
@user_passes_test(lambda u: has_gm_permissions(u))
def gm_add_staff(request):
	if request.method != "POST" or has_gm_permissions(request.user) != "superuser":
		raise Http404
	u = get_object_or_404(User, pk=request.POST.get("staff"))
	u.is_staff = True
	u.save()
	return HttpResponseRedirect(reverse("lupus:gm_staff"))

@login_required
@user_passes_test(lambda u: has_gm_permissions(u))
def gm_add_su(request):
	if request.method != "POST" or has_gm_permissions(request.user) != "superuser":
		raise Http404
	u = get_object_or_404(User, pk=request.POST.get("superuser"))
	u.is_staff = True
	u.is_superuser = True
	u.save()
	return HttpResponseRedirect(reverse("lupus:gm_staff"))

@login_required
@user_passes_test(lambda u: has_gm_permissions(u))
def gm_del_su(request, id):
	if has_gm_permissions(request.user) != "superuser":
		raise Http404
	u = get_object_or_404(User, pk=id)
	u.is_staff = False
	u.is_superuser = False
	u.save()
	return HttpResponseRedirect(reverse("lupus:gm_staff"))

class GMIscrittiView(LupusGMTemplateView): #Non si può usare WithMatchView perchè questa pagina deve essere accessibile anche prima della scadenza del periodo di registrazione
	template_name = "iscritti.html"

	def get_context_data(self, **kwargs):
		context = super(GMIscrittiView, self).get_context_data(**kwargs)
		if "match_id" in kwargs and kwargs["match_id"]:
			m = Match.objects.filter(id = kwargs["match_id"], active = True)
			if self.gm_perms == "staff":
				m = m.filter(gm=self.request.user)
			context["match"] = m.first()
		else:
			messages.error(self.request, "Match non specificato.")
			context["match"] = None
			return context
		if not context["match"]:
			messages.error(self.request, "Match non valido.")
			return context
		context["is_started"] = context["match"].isStarted()
		if context["match"]:
			iscritti = Utente.objects.all().filter(match_id=context["match"].id).order_by("-nome")
			context["iscritti"] = iscritti
		return context

@login_required
@user_passes_test(lambda u: has_gm_permissions(u))
def gm_edit_iscritto(request):
	if request.method == "POST" or request.is_ajax():
		id = request.POST.get("id_iscritto")
		i = get_object_or_404(Utente, pk=id)
		if has_gm_permissions(request.user) == "staff" and i.match.gm != request.user:
			raise Http404
		if i.match.isStarted():
			messages.error(request, "Non si può modificare un profilo dopo l'inizio del match.")
			return HttpResponseRedirect(reverse("lupus:gm_gestione_iscritti", kwargs={"match_id": request.POST.get("matchid")}))
		nome = request.POST.get("nickname", "")
		if not nome:
			messages.error(request, "Il nickname non può essere vuoto.")
			return HttpResponseRedirect(reverse("lupus:gm_gestione_iscritti", kwargs={"match_id": request.POST.get("matchid")}))
		i.nome = nome
		i.save()
		if request.is_ajax():
			return HttpResponse("Ok")
		else:
			messages.success(request, "Modifica effettuata con successo.")
			return HttpResponseRedirect(reverse("lupus:gm_gestione_iscritti", kwargs={"match_id": request.POST.get("matchid")}))
	else:
		raise Http404

@login_required
@user_passes_test(lambda u: has_gm_permissions(u))
def gm_del_iscritto(request):
	if request.method == "POST" or request.is_ajax():
		id = request.POST.get("id_iscritto")
		i = get_object_or_404(Utente, pk=id)
		if has_gm_permissions(request.user) == "staff" and i.match.gm != request.user:
			raise Http404
		if i.match.isStarted():
			messages.error(request, "Non si può rimuovere una iscrizione dopo l'inizio del match.")
			return HttpResponseRedirect(reverse("lupus:gm_gestione_iscritti", kwargs={"match_id": request.POST.get("matchid")}))
		i.delete()
		if request.is_ajax():
			return HttpResponse("Ok")
		else:
			messages.success(request, "Iscrizione rimossa con successo.")
			return HttpResponseRedirect(reverse("lupus:gm_gestione_iscritti", kwargs={"match_id": request.POST.get("matchid")}))
	else:
		raise Http404
