from django.contrib import admin
from lupus.models import *

admin.site.register(Utente)
admin.site.register(Match)
admin.site.register(Gruppo)
admin.site.register(Status)
admin.site.register(Eventi)
admin.site.register(Azioni)
admin.site.register(Voto)
admin.site.register(Votazione)
admin.site.register(Messaggio)
admin.site.register(Nota)
admin.site.register(LogGestione)
admin.site.register(LogAccesso)
