# -*- encoding: utf8 -*-
from django.db import models
from django.contrib.auth.models import User

from lupusrealtime.settings import *
#from lupus.setting import GAMETIME_NIGHTSTART, *
from lupus.setting import *

from lupus.time import localnow
from django.utils.timezone import timedelta

from random import randint

class Match(models.Model) :
	class Meta:
		verbose_name_plural = "Matchs"
	def __init__(self, *args, **kwargs) :
		if not ("startdate" in kwargs):
			kwargs["startdate"]=localnow().date()+timedelta(days=21)
		if not ("regenddate" in kwargs) :
			kwargs["regenddate"]=localnow().date()+timedelta(days=20)
		super(Match, self).__init__(*args, **kwargs)
	def __bool__(self):
		return bool(self.id)
	def __int__(self) :
		return self.id
	def __str__(self):
		return self.nome
	nome = models.CharField(max_length=30)
	startdate = models.DateField("Start Date")
	regenddate = models.DateField("Registration End Date", blank=True)
	enddate = models.DateField("End Date", null=True, blank=True)
	villaggio = models.OneToOneField("Gruppo", related_name="villaggio", related_query_name="villaggio", null=True, on_delete=models.CASCADE)
	active = models.BooleanField("active", default = False)
	winner = models.CharField(max_length=1, choices=FAZIONI_CHOICES, default="", blank=True)
	gm = models.ForeignKey(User, null=True, related_name="gm_set", related_query_name="gm", on_delete=models.CASCADE)
	def isStarted(self, now=localnow()):
		return (self.active) and (self.winner == "") and ((self.startdate < now.date())or((now.date() == self.startdate)and(now.time()>=GAMETIME_NIGHTSTART)))
	def isRegistrable(self, now=localnow()):
		return (self.active) and (self.regenddate >= now.date())

class Gruppo(models.Model) :
	class Meta:
		verbose_name_plural = "Gruppi"
	def __str__(self):
		return str(self.id)+":"+self.name
	def __bool__(self):
		return bool(self.id)
	def __bool__(self):
		return bool(self.id)
	name = models.CharField(max_length=30)
	ruolo = models.CharField(max_length=4, choices=RUOLI_CHOICES, default="CON")
	match = models.ForeignKey(Match, on_delete=models.CASCADE)

class Utente(models.Model) :
	class Meta:
		verbose_name = "Profilo"
		verbose_name_plural = "Profili"
	def __bool__(self):
		return bool(self.id)
	def __str__(self):
		try :
			nome = self.user.get_full_name()
		except (User.DoesNotExist):
			nome = "##NESSUNO##"
		return str(self.id)+"(%s):"%self.nome+nome
	nome = models.CharField(max_length=30)
	user = models.ForeignKey(User, on_delete=models.CASCADE)
	ruolo = models.CharField(max_length=4, choices=RUOLI_CHOICES, default="CON", null=True)
	stato = models.SmallIntegerField(default=0)
	gruppo = models.ForeignKey(Gruppo, null=True, blank=True, on_delete=models.CASCADE)
	genere = models.SmallIntegerField(choices=((1, "Maschio"),(2, "Femmina")))
	match = models.ForeignKey(Match, null=True, on_delete=models.CASCADE)
	active = models.BooleanField("active", default=False)
	def get_ruolo(self):
		return RUOLI[self.ruolo]

class Votazione(models.Model) :
	class Meta:
		verbose_name_plural = "Votazioni"
	def __str__(self):
		return "%d#%d,%s,U%d"%(self.id,self.flags,str(self.date),self.utente.id)
	utente = models.ForeignKey(Utente, on_delete=models.CASCADE)
	flags = models.SmallIntegerField(default=1)
	count = models.SmallIntegerField(default=0)
	date = models.DateField("date")

class Voto(models.Model) :
	class Meta:
		verbose_name_plural = "Voti"
	def __str__(self):
		return "%d#%d,%s,V%d"%(self.id,self.flags,str(self.time),self.votante.id)
	time = models.DateField("time")
	votante = models.ForeignKey(Utente, on_delete=models.CASCADE)
	flags = models.SmallIntegerField(default=1)
	risultato = models.ForeignKey(Votazione, on_delete=models.CASCADE)

class Azioni(models.Model) :
	class Meta:
		verbose_name_plural = "Azioni"
	def __str__(self):
		return "%s#%s,A%d"%(str(self.id) if self.id else "NONE",str(self.time),self.agente.id)
	agente = models.ForeignKey(Utente, related_name="azioni_set", related_query_name="azioni", on_delete=models.CASCADE)
	time = models.DateField("time")
	bersaglio = models.ForeignKey(Utente, related_name="subite_set", related_query_name="subite", on_delete=models.CASCADE)

class Messaggio(models.Model) :
	class Meta:
		verbose_name = "Messaggio chat"
		verbose_name_plural = "Messaggi chat"
	def __str__(self):
		return "%d:U%d,G%d"%(self.id,self.utente.id,self.gruppo.id)
	time = models.DateTimeField("time")
	text = models.CharField(max_length=200)
	utente = models.ForeignKey(Utente, on_delete=models.CASCADE)
	gruppo = models.ForeignKey(Gruppo, on_delete=models.CASCADE)

class Eventi(models.Model) :
	class Meta:
		verbose_name_plural = "Eventi"
	#def __str__(self):
	#	return "%d:U%d,G%d"%(self.id,self.utente.id,self.gruppo.id)
	def __init__(self, *args, **kwargs) :
		if (not "scena" in kwargs) and (kwargs.get("random_scene",False)) :
			maxs = EVENTI[kwargs.get("flags",0)]["scenes"]
			if maxs : 
				kwargs["scena"]=randint(1,maxs)
			del kwargs["random_scene"]
		super(Eventi, self).__init__(*args, **kwargs)
	def __str__(self):
		return "<Evento %s:%s>"%(str(self.id), self.get_flags_display())
	date = models.DateField("date")
	flags = models.SmallIntegerField("flags", default=1, choices=EVENTI_CHOICES)
	scena = models.SmallIntegerField("scena", default=0)
	utente = models.ForeignKey(Utente, on_delete=models.CASCADE)
	def get_human_name(self):
		return EVENTI[self.flags]["Help"]
	def get_dict(self):
		return EVENTI[self.flags]
	def get_data(self):
		pass

class Status(models.Model) :
	class Meta:
		verbose_name_plural = "Status"
	def __str__(self):
		return "<Status U%s, %d>"%(str(self.utente_id),self.flags)
	utente = models.ForeignKey(Utente, on_delete=models.CASCADE)
	flags = models.SmallIntegerField("flags", choices=STATUS_CHOICES)
	opt = models.IntegerField("opt", default=0)
	def get_dict(self):
		return STATUS_FLAG[self.flags]
	#def get_data(self):
	#	return STATUS_DATA[self.get_dict()["ID"]]
	def get_ID(self):
		return self.get_dict()["ID"]

class Nota(models.Model) :
	class Meta:
		verbose_name_plural = "Note"
	def __str__(self):
		return "<Nota #%id %s %s>"%(self.id, self.flags, str(self.time))
	utente1 = models.ForeignKey(Utente, related_name="nota1_set", related_query_name="nota1", on_delete=models.CASCADE)
	utente2 = models.ForeignKey(Utente, related_name="nota2_set", related_query_name="nota2", null=True, blank=True, on_delete=models.CASCADE)
	time = models.DateField("time")
	flags = models.CharField("flags", max_length=4, choices=NOTE_CHOICES)
	opt =  models.IntegerField("opt", default=0, blank=True)
	text =  models.CharField("text", default="", max_length=50, blank=True)
	def get_dict(self):
		return NOTE[self.flags]

class LogGestione(models.Model) :
	class Meta:
		verbose_name_plural = "LogGestioni"
	def __init__(self, *args, **kwargs):
		kwargs.setdefault("time", localnow())
		super(LogGestione, self).__init__(*args, **kwargs)
	def __str__(self):
		return "Log%d for %s in %s"%(self.id,self.get_flag_display(),str(self.date))
	flag = models.SmallIntegerField("flag", choices=((1,"tramonto"),(2,"notte")))
	time = models.DateTimeField("time")
	date = models.DateField("date")
	automatico = models.BooleanField("auto", default=False)
	match  = models.ForeignKey(Match, on_delete=models.CASCADE)
	azioni = models.CharField("azioni", max_length=1000)

class LogAccesso(models.Model) :
	class Meta:
		verbose_name_plural = "LogAccessi"
	def __str__(self):
		return "Logaccesso utente %d il %s"%(self.utente_id,str(self.date))
	timeflag = models.SmallIntegerField("timeflag", choices=((1,"notte"),(2,"giorno")))
	date = models.DateField("date") #il giorno di gioco
	utente = models.ForeignKey(Utente, on_delete=models.SET_NULL, null=True)
